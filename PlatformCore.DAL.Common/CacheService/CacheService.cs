﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Options;
using StackExchange.Redis;


namespace PlatformCore.DAL.Common.CacheService
{
    public class СacheService : IСacheService
    {
        private readonly ConnectionMultiplexer _connection;
        private readonly IDatabase _db;

        public СacheService(string connectionString, int dataBaseId)
        {
            _connection = ConnectionMultiplexer.Connect(connectionString);
            _db = _connection.GetDatabase(dataBaseId);
        }

        public bool StringSet(string key, string value, TimeSpan time)
        {
            return _db.StringSet(key,value, time);
        }
        public bool StringSet(string key, string value)
        {
            return _db.StringSet(key, value);
        }

        public string StringGet(string key)
        {
            return _db.StringGet(key);
        }

        public void RemoveKey(string key)
        {
            _db.KeyDelete(key);
        }

        public void Dispose()
        {
            _connection?.Close();
        }

        
    }
}
