
GO

UPDATE [dbo].[Categories]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8888', '');   
UPDATE [dbo].[Categories]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8889', '');   
   
UPDATE [dbo].[Competitions]
   SET BackgroundImage = REPLACE(BackgroundImage, 'http://94.249.192.145:8888', ''),   
   BackgroundImageMiddle = REPLACE(BackgroundImageMiddle, 'http://94.249.192.145:8888', ''),
   BackgroundImageSmall = REPLACE(BackgroundImageSmall, 'http://94.249.192.145:8888', '');
UPDATE [dbo].[Competitions]
   SET BackgroundImage = REPLACE(BackgroundImage, 'http://94.249.192.145:8889', ''),
   BackgroundImageMiddle = REPLACE(BackgroundImageMiddle, 'http://94.249.192.145:8889', ''),
   BackgroundImageSmall = REPLACE(BackgroundImageSmall, 'http://94.249.192.145:8889', '');
   
UPDATE [dbo].[MapGoodsPictures]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8888', '');      
UPDATE [dbo].[MapGoodsPictures]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8889', '');  
   
UPDATE [dbo].[News]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8888', '');
UPDATE [dbo].[News]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8889', '');  

UPDATE [dbo].[Sports]
   SET PictureWebUrl = REPLACE(PictureWebUrl, 'http://94.249.192.145:8888', ''),   
   PictureMobileUrl = REPLACE(PictureMobileUrl, 'http://94.249.192.145:8888', '');
UPDATE [dbo].[Sports]
   SET PictureWebUrl = REPLACE(PictureWebUrl, 'http://94.249.192.145:8889', ''),
   PictureMobileUrl = REPLACE(PictureMobileUrl, 'http://94.249.192.145:8889', '');
   
UPDATE [dbo].[Users]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8888', '');
UPDATE [dbo].[Users]
   SET PictureUrl = REPLACE(PictureUrl, 'http://94.249.192.145:8889', '');  

GO


