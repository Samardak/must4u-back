﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(20170726)]
    public class M_20170726_AlterUser_RegionStation : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AlterColumn("StationRegionId").AsInt64().Nullable().ForeignKey("StationRegions", "Id");
        }

        public override void Down()
        {
            
        }
    }
}
