﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FluentMigrator;

//namespace Db.Project
//{
//    [Migration(1)]
//    public class M0001_CreateShop : Migration
//    {
//        public override void Up()
//        {
//            //Add Quantity available in goods
//            Alter.Table("Goods").AddColumn("QuantityAvailable").AsInt32().NotNullable().WithDefaultValue(0);


//            Create.Table("Orders")
//                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable()
//                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id");

//            Create.Table("MapOrderGoods")
//                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
//                .WithColumn("OrderId").AsInt64().NotNullable().ForeignKey("Orders", "Id")
//                .WithColumn("GoodsId").AsInt64().NotNullable().ForeignKey("Goods", "Id")
//                .WithColumn("Quantity").AsInt32().NotNullable()
//                .WithColumn("Price").AsDecimal(18, 2).NotNullable()
//                .WithColumn("TotalPrice").AsDecimal(18, 2).NotNullable()
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable()
//                .WithColumn("IsDeleted").AsBoolean().NotNullable();


//        }

//        public override void Down()
//        {
//            Delete.Table("Orders");
//            Delete.Table("MapOrderGoods");
//        }
//    }

//    [Migration(310520171338)]
//    public class M0001_AddStatusOrderPaymentShipment : Migration
//    {
//        public override void Up()
//        {
//            #region table status order
//            Create.Table("StatusOrders")
//                    .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
//                    .WithColumn("Name").AsString().NotNullable()
//                    .WithColumn("Description").AsString().Nullable();

//            Insert.IntoTable("StatusOrders").Row(new { Id = 1, Name = "New", Description = "just after creation an order" });
//            Insert.IntoTable("StatusOrders").Row(new { Id = 2, Name = "In progress", Description = "when there are any action on admin side" });
//            Insert.IntoTable("StatusOrders").Row(new { Id = 3, Name = "Completed", Description = "when the goods are delivered" });
//            Insert.IntoTable("StatusOrders").Row(new { Id = 4, Name = "Issue", Description = "any unrecognized problems with order" });
//            Insert.IntoTable("StatusOrders").Row(new { Id = 5, Name = "Canceled", Description = "might be canceled by admin if there are no such a goods or not enough gg. Might be canceled by user" });
//            #endregion table status order

//            #region table status payment
//            Create.Table("StatusPayments")
//                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
//                .WithColumn("Name").AsString().NotNullable()
//                .WithColumn("Description").AsString().Nullable();

//            Insert.IntoTable("StatusPayments").Row(new { Id = 1, Name = "Paid", Description = "Paid" });
//            Insert.IntoTable("StatusPayments").Row(new { Id = 2, Name = "Not payed", Description = "Not payed" });
//            #endregion table status payment


//            #region table Shipment payment
//            Create.Table("StatusShipments")
//                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable()
//                .WithColumn("Name").AsString().NotNullable()
//                .WithColumn("Description").AsString().Nullable();

//            Insert.IntoTable("StatusShipments").Row(new { Id = 1, Name = "New", Description = "Didn’t start" });
//            Insert.IntoTable("StatusShipments").Row(new { Id = 2, Name = "Preparing", Description = "Preparing" });
//            Insert.IntoTable("StatusShipments").Row(new { Id = 3, Name = "In progress", Description = "In progress" });
//            Insert.IntoTable("StatusShipments").Row(new { Id = 4, Name = "Shipped", Description = "Shipped" });
//            #endregion table Shipment payment
//        }

//        public override void Down()
//        {
//            Delete.Table("StatusOrders");
//            Delete.Table("StatusPayments");
//            Delete.Table("StatusShipments");
//        }
//    }

//    [Migration(310520171352)]
//    public class M0001_AddStatusOrderEvent : Migration
//    {
//        public override void Up()
//        {
            
//            Create.Table("OrderEvents")
//                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable().Identity()
//                .WithColumn("OrderId").AsInt64().NotNullable().ForeignKey("Orders","Id")
//                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users","Id")
//                .WithColumn("StatusOrderId").AsInt32().NotNullable().ForeignKey("StatusOrders","Id")
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable();

//            Create.Table("OrderPaymentEvents")
//                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable().Identity()
//                .WithColumn("OrderId").AsInt64().NotNullable().ForeignKey("Orders", "Id")
//                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id")
//                .WithColumn("StatusPaymentId").AsInt32().NotNullable().ForeignKey("StatusPayments", "Id")
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable();

//            Create.Table("OrderShipmentEvents")
//                .WithColumn("Id").AsInt32().PrimaryKey().NotNullable().Identity()
//                .WithColumn("OrderId").AsInt64().NotNullable().ForeignKey("Orders", "Id")
//                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id")
//                .WithColumn("StatusShipmentId").AsInt32().NotNullable().ForeignKey("StatusShipments", "Id")
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable();
//        }

//        public override void Down()
//        {
//            Delete.Table("OrderEvents");
//            Delete.Table("OrderPaymentEvents");
//            Delete.Table("OrderShipmentEvents");
//        }
//    }

//    [Migration(310520171539)]
//    public class M0001_AddOrderEventColumn : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Orders").AddColumn("IsDeleted").AsBoolean().NotNullable().WithDefaultValue(false);
//        }

//        public override void Down()
//        {
            
//        }
//    }


//}
