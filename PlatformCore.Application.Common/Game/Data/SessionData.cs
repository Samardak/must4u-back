﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Common.Game.Enum;

namespace PlatformCore.Common.Game.Data
{
    public class SessionData
    {
        public string Token { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }

        public EnumUserTypes UserType { get; set; }
    }
}
