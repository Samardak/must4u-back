﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.GameTokenService
{
    public class TokenService : ITokenService
    {
        private static readonly long k = 123;

        public string GetNewToken(long customerId)
        {
            long gameId = 1;
            long digit = Convert.ToInt64(string.Format($"{customerId}{gameId}{gameId.ToString().Length}"));
            string ret = Guid.NewGuid().ToString();
            long tmp = 0;
            foreach (char c in ret)
            {
                tmp += c;
            }
            ret += "|" + ((tmp + digit) * k).ToString();
            return ret;
        }

        public void GetUserIddbyToken(string token, out long customeId)
        {
            customeId = 0;
            long gameId = -1;
            long ret = 0;
            var tkn = token.Split('|');
            long tmp = 0;
            foreach (char c in tkn[0])
            {
                tmp += c;
            }
            try
            {
                ret = (Convert.ToInt64(tkn[1]) / k - tmp);
                string strRet = ret.ToString();
                int strRetLength = strRet.Length;
                if (strRetLength < 3)
                {
                    return;
                }
                byte countDigit = Convert.ToByte(strRet.Substring(strRetLength - 1));
                gameId = Convert.ToInt64(strRet.Substring(strRetLength - countDigit - 1, countDigit));
                customeId = Convert.ToInt64(strRet.Substring(0, strRetLength - (1 + countDigit)));
            }
            catch
            {
                customeId = 0;
                gameId = -1;
            }
        }
    }
}
