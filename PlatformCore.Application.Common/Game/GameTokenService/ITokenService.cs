﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.GameTokenService
{
    public interface ITokenService
    {
        string GetNewToken(long customerId);
        void GetUserIddbyToken(string token, out long customeId);
    }
}
