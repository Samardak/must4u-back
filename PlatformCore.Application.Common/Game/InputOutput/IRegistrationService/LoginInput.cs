﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Common.Game.Enum;

namespace PlatformCore.Common.Game.InputOutput.IRegistrationService
{
    public class LoginInput
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Range(1, 2, ErrorMessage = "UserType suports Client:1 ; Master2; ")]
        public EnumUserTypes UserType { get; set; }
    }
}
