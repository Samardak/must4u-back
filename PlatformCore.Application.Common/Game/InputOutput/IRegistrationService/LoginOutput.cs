﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.InputOutput.IRegistrationService
{
    public class LoginOutput
    {
        public string Token { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }
        public CountryData CountryData { get; set; }
        public CityData CityData { get; set; }
        public StationRegionData StationRegionData { get; set; }

    }

    public class StationRegionData
    {
        public long StationRegionId { get; set; }
        public string StationRegionName { get; set; }
    }

    public class CityData
    {
        public long CityId { get; set; }
        public string CityName { get; set; }
    }

    public class CountryData
    {
        public long CountryId { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }
}
