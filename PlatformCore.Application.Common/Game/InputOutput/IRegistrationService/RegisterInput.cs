﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PlatformCore.Common.Game.Enum;

namespace PlatformCore.Common.Game.InputOutput.IRegistrationService
{
    public class RegisterInput
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [Range(1,2,ErrorMessage = "UserType suports Client:1 ; Master2; ")]
        public EnumUserTypes UserType { get; set; }
        [Required]
        public IpData IpData { get; set; }
    }

    public class IpData
    {
        public string City { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Isp { get; set; }
        public decimal Lat { get; set; }
        public decimal Lon { get; set; }
        public string Org { get; set; }
        public string Query { get; set; }
        public string Region { get; set; }
        public string RegionName { get; set; }
        public string Status { get; set; }
        public string Timezone { get; set; }
        public string Zip { get; set; }
    }
}
