﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Enum
{
    public enum EnumPostTypes
    {
        SimplePost = 1,
        AddPost = 2,
    }
}
