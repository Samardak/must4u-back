﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.Enum
{
    public enum EnumAuthTypes
    {
        Username = 1,
        Vk = 2,
        Facebook = 3
    }
}
