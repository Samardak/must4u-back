﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.Enum
{
    public enum EnumUserTypes : byte
    {
        Client = 1,
        Master = 2
    }
}
