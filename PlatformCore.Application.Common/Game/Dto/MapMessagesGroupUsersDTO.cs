﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MapMessagesGroupUsersDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long UserId { get; set; }
        public DateTimeOffset LastTimeView { get; set; }
    }
}
