﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MessagesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long UserIdFrom { get; set; }
        public long UserIdTo { get; set; }
        public string Text { get; set; }
        public bool IsRead { get; set; }
        public DateTimeOffset DateCreation { get; set; }
    }
}
