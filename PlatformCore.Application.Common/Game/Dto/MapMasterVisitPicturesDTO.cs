﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MapMasterVisitPicturesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long MasterVisitId { get; set; }
        public long PicutureId { get; set; }
    }
}
