﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class StationRegionsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string StationName { get; set; }
    }
}
