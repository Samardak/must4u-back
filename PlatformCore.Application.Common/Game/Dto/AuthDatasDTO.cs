﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class AuthDatasDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public int AuthTypeId { get; set; }
        public long UserId { get; set; }
        public int UserTypeId { get; set; }
        public string ExternalId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
