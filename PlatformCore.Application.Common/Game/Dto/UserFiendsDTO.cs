﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class UserFiendsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long User1Id { get; set; }
        public long User2Id { get; set; }
        public long IniciatorId { get; set; }
        public bool IsConfirm { get; set; }
        public bool IsIgnore { get; set; }
        public bool IdDeleted { get; set; }
        public DateTimeOffset DateCreation { get; set; }
    }
}
