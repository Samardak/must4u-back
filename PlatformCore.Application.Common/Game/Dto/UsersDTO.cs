﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class UsersDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public int UserTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }
        public string Email { get; set; }
        public string Currency { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public long? StationRegionId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int WorkingExperience { get; set; }
        public bool IsOnlyHome { get; set; }
        public string AboutText { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsManicure { get; set; }
        public bool IsPedicure { get; set; }
    }
}
