﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class PostsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public int PostTypeId { get; set; }
        public long UserId { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public int CountLikes { get; set; }
        public int CountDislike { get; set; }
        public int CountComments { get; set; }
        public bool IsDeleted { get; set; }
    }
}
