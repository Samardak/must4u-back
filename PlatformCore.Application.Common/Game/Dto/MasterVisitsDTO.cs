﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MasterVisitsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string Nickname { get; set; }
        public long MasterId { get; set; }
        public long MasterScheduleId { get; set; }
        public bool IsMasterConfirm { get; set; }
        public bool IsUserConfirm { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IdDeleted { get; set; }
    }
}
