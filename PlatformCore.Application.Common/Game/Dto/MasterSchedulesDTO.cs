﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MasterSchedulesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long UserId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public bool IsWeekly { get; set; }
        public long DayOfWeek { get; set; }
        public int Nyear { get; set; }
        public int Nmonth { get; set; }
        public int Nday { get; set; }
        public bool IsFreeDay { get; set; }
        public bool IsDeleted { get; set; }
    }
}
