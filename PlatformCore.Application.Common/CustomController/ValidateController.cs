﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.CustomController
{
    public class ValidateController : Controller
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            
            var listKeys = context.ActionArguments.Keys.ToList();
            var listNullObject = new List<string>();
            for (int i = 0; i < listKeys.Count; i++)
            {
                var currentKey = listKeys[i];
                var currInput = context.ActionArguments[currentKey];
                if(currInput == null)
                    listNullObject.Add(currentKey);
            }
            if (listNullObject.Count > 0)
            {
                var jsonNullKey = JsonConvert.SerializeObject(listNullObject);
                throw new BaseException(CodeError.CodeError.BadRequeast, "Invalid value of : " + jsonNullKey + " (See the documentation)", "(See the documentation)");
            }
            
            if (!ModelState.IsValid)
            {
                var allKeys = ModelState.Keys.ToList();
                var xx = ModelState.Values.ToList();
                var errors = xx.FirstOrDefault().Errors;
                var errorMEssage = errors.Select(i=> i.ErrorMessage).ToList();
                var allListError = JsonConvert.SerializeObject(errorMEssage);
                var jsonObject = JsonConvert.SerializeObject(allKeys);
                throw new BaseException(CodeError.CodeError.BadRequeast, "Invalid value of : " + jsonObject,  allListError);
            }
            
            //var arg = aa as IValidatableObject;
            //IEnumerable<ValidationResult> valResults = null;

            //if (arg != null)
            //{
            //    valResults = arg.Validate(null);
            //}

            //if (valResults != null && valResults.Any())
            //{
            //    foreach (var valRes in valResults)
            //    {
            //        ModelState.AddModelError(valRes.MemberNames.FirstOrDefault(), valRes.ErrorMessage);
            //    }
            //}

            //if (ModelState.IsValid == false)
            //    throw new ValidationException { Validation = ModelState };

            base.OnActionExecuting(context);
        }
    }
}
