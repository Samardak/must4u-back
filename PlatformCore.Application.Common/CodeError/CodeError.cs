﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.CodeError
{
    public enum CodeError
    {
        InternalServerIncorrectData = 500,
        TokenInvalid = 401,
        BadRequeast = 400,
        UsernameAlreadyTaken = 1,
        InvalidCredentials = 2,
        YouShoulLoginAsMaster = 3,
        YouShoulLoginAsClient = 4,
        InvalidCountryId = 5,
        InvalidCityId = 6,
        InvalidStationId = 7,
        InvalidMasterServiceId = 8,
        InvalidServiceTypeId = 9,

        You_Pass_Dublicate_MasterService_WithSrviceTypeId = 10,

        You_Pass_Dublicate_MasterService_With_SameId = 11,

        YouTypedTooMuchTextMaxLengthIs = 12,

        YouPassDubliacteDayOffWeek = 13,
        YouPassDubliacteScheduleId = 14,
        DoesNotExistUser = 15,
    }

    public static class CodeMessage
    {
        public static string GetMessage(this CodeError codeError)
        {
            switch (codeError)
            {
                case CodeError.TokenInvalid:
                    return "Token invalid!";
                case CodeError.UsernameAlreadyTaken:
                    return "Email already taken";
                case CodeError.InvalidCredentials:
                    return "Invalid password or email";
                case CodeError.YouShoulLoginAsMaster:
                    return "You should login as master";
                case CodeError.YouShoulLoginAsClient:
                    return "You should login as client";
                case CodeError.InvalidCountryId:
                    return "Invalid country id";
                case CodeError.InvalidCityId:
                    return "Invalid city id";
                case CodeError.InvalidStationId:
                    return "Invalid station id";
                case CodeError.InvalidMasterServiceId:
                    return "Invalid master services id";
                case CodeError.InvalidServiceTypeId:
                    return "Invalid services type id";

                case CodeError.You_Pass_Dublicate_MasterService_WithSrviceTypeId:
                    return "You pass dubliacte master service with same service type id ";

                case CodeError.You_Pass_Dublicate_MasterService_With_SameId:
                    return "You Pass Dublicate MasterService With SameId ";

                case CodeError.YouTypedTooMuchTextMaxLengthIs:
                    return "You Typed Too Much Text. Max length is : ";
                case CodeError.YouPassDubliacteDayOffWeek:
                    return "You Pass Dubliacte Day Off Week. : ";
                case CodeError.YouPassDubliacteScheduleId:
                    return "You Pass Dubliacte Schedule Id : ";
                case CodeError.InternalServerIncorrectData:
                    return "Internal Server Incorrect Data";
                case CodeError.DoesNotExistUser:
                    return "Does not exist user";



                default:
                    return "Default message";
                    
            }
        }
    }
}
