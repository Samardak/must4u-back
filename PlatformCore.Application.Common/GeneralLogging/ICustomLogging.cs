﻿using PlatformCore.Common.GeneralLogging;

namespace PlatformCore.Application.Common.GeneralLogging
{
    public interface ICustomLogging
    {
        void LoggingRequestAndResponse(CustomLoggingInput input);
    }
}
