﻿namespace PlatformCore.Application.Common.GeneralLogging
{
    public class CustomLoggingInput
    {
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public string Path { get; set; }
        public int StatusCode { get; set; }
    }
}
