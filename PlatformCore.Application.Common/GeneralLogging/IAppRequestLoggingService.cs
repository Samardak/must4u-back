﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Application.Common.GeneralLogging;

namespace PlatformCore.Common.GeneralLogging
{
    public interface IAppRequestLoggingService
    {
        void LoggingRequestAndResponse(CustomLoggingInput input);
    }
}
