﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Application.Common.GeneralLogging;

namespace PlatformCore.Common.GeneralLogging
{
    public class CustomLogging : ICustomLogging
    {
        private static object _lockFile = new object();
        public static string PathToFile { get; private set; }
        public CustomLogging(string fullPathToFile)
        {
            PathToFile = fullPathToFile;
        }

        public void LoggingRequestAndResponse(CustomLoggingInput input)
        {
            lock (_lockFile)
            {
                try
                {
                    if (!File.Exists(PathToFile))
                    {
                        File.Create(PathToFile);
                    }
                    var logWriter = File.AppendText(PathToFile);
                    var dtN = DateTimeOffset.Now;


                    logWriter.WriteLine(new string('_',50));
                    logWriter.WriteLine($"Path         : {input?.Path} ");
                    logWriter.WriteLine($"Time         : {dtN}");
                    logWriter.WriteLine($"StatusCode   : {input?.StatusCode}");
                    logWriter.WriteLine($"RequestBody  : {input?.RequestBody}");
                    logWriter.WriteLine($"ResponseBody : {input?.ResponseBody}");
                    logWriter.WriteLine(new string('_', 50));
                    logWriter.WriteLine();
                    logWriter.WriteLine();
                    logWriter.WriteLine();

                    logWriter.Dispose();
                }
                catch(Exception exception)
                {

                }
            }
        }
    }
}
