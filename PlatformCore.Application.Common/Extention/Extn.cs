﻿using System;

namespace PlatformCore.Application.Common.Extention
{
    public static class Extn
    {
        public static U Then<T, U>(this T o, Func<T, U> f) { return f(o); }
        public static U Then<T, U>(this T o, Func<T, Func<U>> fe) { return fe(o)(); }
    }
}
