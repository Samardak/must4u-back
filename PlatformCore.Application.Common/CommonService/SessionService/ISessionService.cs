﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PlatformCore.Common.Game.Data;

namespace PlatformCore.Common.CommonService.SessionService
{
    public interface ISessionService
    {
        void SetSessionData(SessionData input);

        SessionData GetSessionData(string token);
        SessionData GetSessionMasterData(string token);

        Task SetSessionDataAsync(SessionData input);

        Task<SessionData> GetSessionDataAsync(string token);
        Task<SessionData> GetSessionMasterDataAsync(string token);

        List<string> GetListTokenByCustomerId(long customerId);

        Task RemoveSessionAsync(string token);


    }
}
