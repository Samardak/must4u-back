﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.Game.Data;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;
using PlatformCore.DAL.Common.CacheService;

namespace PlatformCore.Common.CommonService.SessionService
{
    public class SessionService : ISessionService
    {
        private readonly IСacheService _cacheService;
        private readonly string customerStr = "customer";
        public SessionService(IСacheService сacheService)
        {
            _cacheService = сacheService;
        }

        public void SetSessionData(SessionData input)
        {
            string strValue = JsonConvert.SerializeObject(input);
            _cacheService.StringSet(input.Token, strValue);

            var key = this.customerStr + input.UserId;
            var listToken = this.GetListTokenForCustomer(key);
            listToken.Add(input.Token);
            this.SetListTokenFoCustomer(key,listToken);
        }

        public SessionData GetSessionData(string token)
        {
            if(string.IsNullOrEmpty(token))
                throw new BaseException(CodeError.CodeError.TokenInvalid, CodeError.CodeError.TokenInvalid.GetMessage(),string.Empty,HttpStatusCode.Unauthorized);
            var jsonValue = _cacheService.StringGet(token);
            if (string.IsNullOrEmpty(jsonValue))
                throw new BaseException(CodeError.CodeError.TokenInvalid, CodeError.CodeError.TokenInvalid.GetMessage(), string.Empty, HttpStatusCode.Unauthorized);
                //return null;
            return JsonConvert.DeserializeObject<SessionData>(jsonValue);
        }
        
        public SessionData GetSessionMasterData(string token)
        {
            if (string.IsNullOrEmpty(token))
                throw new BaseException(CodeError.CodeError.TokenInvalid, CodeError.CodeError.TokenInvalid.GetMessage(), string.Empty, HttpStatusCode.Unauthorized);
            var jsonValue = _cacheService.StringGet(token);
            if (string.IsNullOrEmpty(jsonValue))
                throw new BaseException(CodeError.CodeError.TokenInvalid, CodeError.CodeError.TokenInvalid.GetMessage(), string.Empty, HttpStatusCode.Unauthorized);
            //return null;
            
            var session = JsonConvert.DeserializeObject<SessionData>(jsonValue);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.CodeError.YouShoulLoginAsMaster, CodeError.CodeError.YouShoulLoginAsMaster.GetMessage());
            return session;
        }

        

        public async Task SetSessionDataAsync(SessionData input)
        {
            await Task.Run(() => SetSessionData(input));
        }

        public async Task<SessionData> GetSessionDataAsync(string token)
        {
            return await Task.Run(() => GetSessionData(token));
        }

        public async Task<SessionData> GetSessionMasterDataAsync(string token)
        {
            return await Task.Run(() => GetSessionMasterData(token));
        }

        public List<string> GetListTokenByCustomerId(long customerId)
        {
            var key = this.customerStr + customerId;
            var listToken = this.GetListTokenForCustomer(key);
            return listToken;
        }

        public async Task RemoveSessionAsync(string token)
        {
            await Task.Run(() => _cacheService.RemoveKey(token));
        }


        #region private methods
        private void SetListTokenFoCustomer(string key, List<string> listToken)
        {
            _cacheService.StringSet(key, JsonConvert.SerializeObject(listToken), new TimeSpan(0, 1, 0, 0));
        }

        private List<string> GetListTokenForCustomer(string key)
        {
            var ret = new List<string>();
            var strList = _cacheService.StringGet(key);
            if (!string.IsNullOrEmpty(String.Empty))
            {
                ret = JsonConvert.DeserializeObject<List<string>>(strList);
            }
            return ret;
        } 
        #endregion

    }
}
