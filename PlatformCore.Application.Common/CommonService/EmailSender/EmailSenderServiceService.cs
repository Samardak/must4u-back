﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using PlatformCore.Common.CommonService.EmailSender.Input;

namespace PlatformCore.Common.CommonService.EmailSender
{
    public class EmailSenderServiceService : IEmailSenderService
    {
        public void SendEmail(EmailInput input)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("Администрация сайта", "nvsamardak@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", input.Email));
            emailMessage.Subject = input.Subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = input.Message
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587);
                //client.Authenticate("nvsamardak@gmail.com", "alinanikitasuccess");
                //client.Authenticate("samardak.nikita@betconstruct.com", "success698238");
                client.Authenticate("galustik@gmail.com", "galondues");
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }

        public async Task SendEmailAsync(EmailInput input)
        {
            await Task.Run(() => SendEmail(input));
        }



        //public void TestEmail()
        //{
        //    var message = new MimeMessage();
        //    message.From.Add(new MailboxAddress("Anuraj", "anuraj.p@example.com"));
        //    message.To.Add(new MailboxAddress("Anuraj", "anuraj.p@example.com"));
        //    message.Subject = "Hello World - A mail from ASPNET Core";
        //    message.Body = new TextPart("plain")
        //    {
        //        Text = "Hello World - A mail from ASPNET Core"
        //    };

        //    using (var client = new SmtpClient())
        //    {
        //        client.Connect("smtp.example.com", 587, false);
        //        client.AuthenticationMechanisms.Remove("XOAUTH2");
        //        // Note: since we don't have an OAuth2 token, disable 	// the XOAUTH2 authentication mechanism.     client.Authenticate("anuraj.p@example.com", "password");
        //        client.Send(message);
        //        client.Disconnect(true);
        //    }
        //}

        //public async Task SendEmailAsync(string email, string subject, string message)
        //{
        //    var emailMessage = new MimeMessage();
        //    emailMessage.From.Add(new MailboxAddress("Администрация сайта", "nvsamardak@gmail.com"));
        //    emailMessage.To.Add(new MailboxAddress("", email));
        //    emailMessage.Subject = subject;
        //    emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
        //    {
        //        Text = message
        //    };

        //    using (var client = new SmtpClient())
        //    {
        //        client.Connect("smtp.gmail.com", 587);
        //        //client.Authenticate("nvsamardak@gmail.com", "alinanikitasuccess");
        //        //client.Authenticate("samardak.nikita@betconstruct.com", "success698238");
        //        client.Authenticate("galustik@gmail.com", "galondues");
        //        client.Send(emailMessage);
        //        client.Disconnect(true);
        //    }
        //}
    }
}
