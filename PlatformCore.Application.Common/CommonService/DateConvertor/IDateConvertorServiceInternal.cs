﻿using System;

namespace PlatformCore.Application.Common.CommonService.DateConvertor
{
    public interface IDateConvertorServiceInternal
    {
        long ConvertToSecond(DateTimeOffset input);
        DateTimeOffset ConvertToDate(long seconds);


        long ConvertToMs(DateTimeOffset input);
        DateTimeOffset ConvertToDateFromMs(long ms);
    }
}
