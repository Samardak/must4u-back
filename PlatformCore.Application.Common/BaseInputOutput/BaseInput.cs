﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseInput
    {
        [Required]
        public string Token { get; set; }
    }
}
