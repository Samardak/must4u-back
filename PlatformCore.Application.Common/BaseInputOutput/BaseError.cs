﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseError
    {
        //public int HttpStatusCode { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }

        public BaseError()
        {
            
        }

        public BaseError(int code, string message, string details, HttpStatusCode statusCode = System.Net.HttpStatusCode.InternalServerError)
        {
            this.Code = code;
            this.Message = message;
            this.Details = details;
            //this.HttpStatusCode = (int) statusCode;
        }

        public BaseError(BaseException exception)
        {
            this.Code = (int)exception.Code;
            this.Message = exception.Message;
            this.Details = exception.Details;
            //this.HttpStatusCode = exception.ResponseCode;
        }
    }
}
