﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.BaseInputOutput
{
    public class CustomerBaseInput : BaseInput
    {
        public long CustomerId { get; set; }
    }
}
