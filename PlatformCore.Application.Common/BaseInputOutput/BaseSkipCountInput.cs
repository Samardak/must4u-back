﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlatformCore.Common.BaseInputOutput;

namespace PlatformCore.Application.Common.BaseInputOutput
{
    public class BaseSkipCountInput : BaseInput
    {
        public int Skip { get; set; }
        public int Count { get; set; }
    }
}
