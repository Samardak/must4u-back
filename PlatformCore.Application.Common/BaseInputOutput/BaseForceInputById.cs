﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseForceInputById : BaseInputById
    {
        public bool Force { get; set; }
    }
}
