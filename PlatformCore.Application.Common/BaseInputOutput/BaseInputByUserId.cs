﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlatformCore.Common.BaseInputOutput;

namespace PlatformCore.Application.Common.BaseInputOutput
{
    public class BaseInputByUserId : BaseInput
    {
        [Required]
        public long UserId { get; set; }
    }
}
