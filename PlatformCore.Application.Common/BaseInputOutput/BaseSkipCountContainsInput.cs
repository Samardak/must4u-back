﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.BaseInputOutput
{
    public class BaseSkipCountContainsInput : BaseSkipCountInput
    {
        public string Contains { get; set; }
    }
}
