﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.BaseInputOutput
{
    public class BaseOutputCount<T>
    {
        public int Count { get; set; }
        public T Items { get; set; }

        public BaseOutputCount()
        {
                
        }
        public BaseOutputCount(T items, int count)
        {
            this.Items = items;
            this.Count = count;
        }
    }
}
