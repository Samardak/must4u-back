﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PlatformCore.Common.BaseInputOutput;

namespace PlatformCore.Common.RequestHelper
{
    public interface IRequestHelper
    {
        Task<string> PostRequestAsync(string requestUrl, object data, IDictionary<string, string> headers = null);
        Task<T> PostRequestAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null);
        Task<BaseOutput<T>> PostRequestBaseOutputAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null);
        //Task<SwarmBaseResponse<T>> PostRequestSwarmAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null);

        Task<string> GetRequestAsync(string requestUrl);

    }
}
