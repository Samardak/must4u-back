﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.RequestHelper
{
    //status code 204 //not content
    //public class NoContentException : Exception
    //{
        
    //}
    public class RequestHelper : IRequestHelper
    {

        //public async Task<string> PostRequestAsync(string requestUrl, object data, IDictionary<string, string> headers = null)
        //{
        //    var client = new HttpClient();
        //    var uri = new Uri(requestUrl);
        //    var jsonValue = JsonConvert.SerializeObject(data);
        //    var response = await client.PostAsync(uri, new StringContent(jsonValue, Encoding.UTF8, "application/json"));
        //    if (response.StatusCode == HttpStatusCode.NoContent)
        //    {
        //        throw new NoContentException("HttpStatusCode.NoContent");
        //    }
        //    if (!response.IsSuccessStatusCode)
        //    {
        //        var errorMessage = $"StatusCode: {response.StatusCode}, requestUrl: {requestUrl}";
        //        throw new Exception(errorMessage);
        //    }
        //    return await response.Content.ReadAsStringAsync();
        //}

        public async Task<string> PostRequestAsync(string requestUrl, object data, IDictionary<string, string> headers = null)
        {
            var client = new HttpClient();
            var uri = new Uri(requestUrl);
            var jsonValue = JsonConvert.SerializeObject(data);
            jsonValue = jsonValue.Replace("\\", "");

            #region add headers to request
            if (headers?.Count > 0)
            {
                foreach (var header in headers)
                {
                    client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }
            #endregion add headers to request

            var response = await client.PostAsync(uri, new StringContent(jsonValue, Encoding.UTF8, "application/json"));
            
            //if (response.StatusCode == HttpStatusCode.NoContent)
            //{
            //    throw new NoContentException("HttpStatusCode.NoContent");
            //}
            var retData = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                //var errorMessage = $"StatusCode: {response.StatusCode}, requestUrl: {requestUrl}, retData: {retData} ";
                //throw new Exception(errorMessage);
            }

            return retData;
            
        }

       



        public async Task<T> PostRequestAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null)
        {
            var stringResponse = await PostRequestAsync(requestUrl, data, headers);
            return JsonConvert.DeserializeObject<T>(stringResponse);
        }

        //public async Task<SwarmBaseResponse<T>> PostRequestSwarmAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null)
        //{
        //    var stringResponse = await PostRequestAsync(requestUrl, data, headers);
        //    SwarmBaseResponse<T> resp = null; 

        //    #region deserialize region of data T

        //    try
        //    {
        //        resp = JsonConvert.DeserializeObject<SwarmBaseResponse<T>>(stringResponse);
        //        //resp.Data = JsonConvert.DeserializeObject<T>(resp.DataStr);
        //    }
        //    catch (Exception)
        //    {
        //        stringResponse = stringResponse.Replace("data", "data_str");
        //        resp = JsonConvert.DeserializeObject<SwarmBaseResponse<T>>(stringResponse);
        //    }
        //    #endregion deserialize region of data T

        //    return resp;
        //}

        //SwarmBaseResponse

        public async Task<BaseOutput<T>> PostRequestBaseOutputAsync<T>(string requestUrl, object data, IDictionary<string, string> headers = null)
        {
            string stringResponse = string.Empty;
            BaseOutput<T> response = null;
            try
            {
                stringResponse = await PostRequestAsync(requestUrl, data, headers);
                response = JsonConvert.DeserializeObject<BaseOutput<T>>(stringResponse);
            }
            catch (Exception exception)
            {
                var isNoConten = exception is NoContentException;
                if (isNoConten)
                    return new BaseOutput<T>(default(T));

                exception = new Exception($"{exception.Message} (Perhaps the application is not running) {requestUrl}", exception);
                throw exception;
            }

            //response = JsonConvert.DeserializeObject<BaseOutput<T>>(stringResponse);
            if (!response.Success)
                throw new Exception(response.Error.Message, new Exception(response.Error.Details));
            return response;
        }

        public async Task<string> GetRequestAsync(string requestUrl)
        {
            var client = new HttpClient();
            var uri = new Uri(requestUrl);
            var response = await client.GetAsync(uri);
            if (response.StatusCode == HttpStatusCode.NoContent)
            {
                throw new NoContentException("HttpStatusCode.NoContent");
            }
            if (!response.IsSuccessStatusCode)
            {
                var errorMessage = $"StatusCode: {response.StatusCode}, requestUrl: {requestUrl}";
                return await response.Content.ReadAsStringAsync();
                //throw new Exception(errorMessage);
            }
            return await response.Content.ReadAsStringAsync();
        }


        //public async Task Test()
        //{
        //    var client = new HttpClient();
        //    var xx = new Uri("http://localhost:3331/api/Transaction/DoWin1");
        //    var response = await client.PostAsync(xx, new StringContent("{}", Encoding.UTF8, "application/json"));
        //    if (!response.IsSuccessStatusCode)
        //    {
        //        var errorMessage = $"StatusCode: {response.StatusCode}, requestUrl: ";
        //        throw new Exception();
        //    }
            
        //    var result = await response.Content.ReadAsStringAsync();
        //}
    }
}
