﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PlatformCore.Application.Common.Middleware;
using PlatformCore.Common.BaseInputOutput;

namespace PlatformCore.Common.Middleware
{
    public class ResponseMiddleware
    {
        private readonly RequestDelegate _next;

        public ResponseMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.Value.StartsWith("/api"))
            {
                await _next.Invoke(context);
                return;
            }

            #region for file manager proxy user!!!
            if (context.Request.Path.Value.IsGoForward())
            {
                await _next.Invoke(context);
                return;
            }
            #endregion for file manager proxy user!!!


            using (var buffer = new MemoryStream())
            {
                //replace the context response with our buffer
                var stream = context.Response.Body;
                context.Response.Body = buffer;

                //invoke the rest of the pipeline
                await _next.Invoke(context);

                //reset the buffer and read out the contents
                buffer.Seek(0, SeekOrigin.Begin);
                //var reader = new StreamReader(buffer);
                using (var bufferReader = new StreamReader(buffer))
                {
                    string body = await bufferReader.ReadToEndAsync();

                    var bodyJson = JsonConvert.DeserializeObject(body);
                    //reset to start of stream
                    //buffer.Seek(0, SeekOrigin.Begin);
                    //copy our content to the original stream and put it back
                    await buffer.CopyToAsync(stream);
                    context.Response.Body = stream;

                    //BaseOutput<object> resp = null;
                    //if (context.Response.StatusCode != 200)
                    //{
                    //    resp = new BaseOutput<object>(null,false,
                    //        new BaseError
                    //        {
                    //            Code = context.Response.StatusCode,
                    //            Message = $"StatusCode : {context.Response.StatusCode}"
                    //        });
                    //}
                    //else
                    //{
                    //    resp = new BaseOutput<object>(bodyJson);
                    //}
                    string json =
                       JsonConvert.SerializeObject(
                         bodyJson,
                         Formatting.Indented,
                         new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }
                       );

                    byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    await context.Response.Body.WriteAsync(byteArray, 0, byteArray.Length);
                }
            }
        }
    }
}
