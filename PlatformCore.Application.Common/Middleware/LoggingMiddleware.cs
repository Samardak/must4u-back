﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using PlatformCore.Application.Common.GeneralLogging;
using PlatformCore.Application.Common.Middleware;
using PlatformCore.Common.GeneralLogging;

namespace PlatformCore.Common.Middleware
{
    //public class LoggingMiddleware
    //{
    //    private readonly RequestDelegate _next;
    //    public LoggingMiddleware(RequestDelegate next)
    //    {
    //        this._next = next;
    //    }

    //    public async Task Invoke(HttpContext context)
    //    {
    //        //Workaround - copy original Stream
    //        var initalBody = context.Request.Body;

    //        using (var bodyReader = new StreamReader(context.Request.Body))
    //        {
    //            string body = await bodyReader.ReadToEndAsync();
    //            //Do something with body
    //            //Replace write only request body with read/write memorystream so you can read from it later

    //            context.Request.Body = new MemoryStream(Encoding.UTF8.GetBytes(body));

    //            //handle other middlewares
    //            await _next.Invoke(context);

    //            //Workaround - return back to original Stream
    //            context.Request.Body = initalBody;

    //            //var input = new StreamReader(context.Request.Body).ReadToEnd();
    //            //await _next.Invoke(context);
    //            //var responseStatusCode = context.Request.Body;

    //            //string requestBody = new StreamReader(context.Response.Body).ReadToEnd();
    //            //var result = requestBody;
    //        }
    //    }
    //}
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ICustomLogging _customLogging;

        public LoggingMiddleware(RequestDelegate next, ICustomLogging customLogging)
        {
            _next = next;
            _customLogging = customLogging;
        }

        public async Task Invoke(HttpContext context)
        {

            if (!context.Request.Path.Value.StartsWith("/api"))
            {
                await _next.Invoke(context);
                return;
            }

            #region for file manager proxy user!
            if (context.Request.Path.Value.IsGoForward())
            {
                await _next.Invoke(context);
                return;
            }
            #endregion for file manager proxy user!


            //http://stackoverflow.com/questions/37855384/log-httpresponse-body-for-asp-net-core-1-0-rest-api
            string requestBody = await LogRequest(context.Request);

            //if(string.IsNullOrEmpty(requestBody))
            //    throw new ArgumentNullException("Logging middleware throw exception. Request.Body was null");

            string responseBody = await LogResponseAndInvokeNext(context);
            var path = context.Request.Path.Value;
            var statusCodeResponse = context.Response.StatusCode;

            var customLoggingInput = new CustomLoggingInput
            {
                ResponseBody = responseBody,
                RequestBody = requestBody,
                Path = path,
                StatusCode = statusCodeResponse
            };


            _customLogging.LoggingRequestAndResponse(customLoggingInput);


        }

        private async Task<string> LogRequest(HttpRequest request)
        {
            using (var bodyReader = new StreamReader(request.Body))
            {
                string body = await bodyReader.ReadToEndAsync();
                request.Body = new MemoryStream(Encoding.UTF8.GetBytes(body));
                return body;
            }
        }

        private async Task<string> LogResponseAndInvokeNext(HttpContext context)
        {
            using (var buffer = new MemoryStream())
            {
                //replace the context response with our buffer
                var stream = context.Response.Body;
                context.Response.Body = buffer;

                //invoke the rest of the pipeline
                await _next.Invoke(context);

                //reset the buffer and read out the contents
                buffer.Seek(0, SeekOrigin.Begin);
                var reader = new StreamReader(buffer);
                using (var bufferReader = new StreamReader(buffer))
                {
                    string body = await bufferReader.ReadToEndAsync();
                    //reset to start of stream
                    buffer.Seek(0, SeekOrigin.Begin);
                    //copy our content to the original stream and put it back
                    await buffer.CopyToAsync(stream);
                    context.Response.Body = stream;
                    //System.Diagnostics.Debug.Print($"Response: {body}");
                    return body;
                }
            }
        }
    }


}
