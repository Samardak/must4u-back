﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PlatformCore.Application.Common.Middleware;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.Middleware
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        //http://stackoverflow.com/questions/38630076/asp-net-core-web-api-exception-handling
        //After many experiments I can say that middleware is the best for exception handling in ASP.NET Core Web API
        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {

            if (!context.Request.Path.Value.StartsWith("/api"))
            {
                await _next.Invoke(context);
                return;
            }

            if (context.Request.Path.Value.IsGoForward())
            {
                await _next.Invoke(context);
                return;
            }


            using (var buffer = new MemoryStream())
            {
                //replace the context response with our buffer
                var stream = context.Response.Body;
                context.Response.Body = buffer;
                try
                {
                    await _next.Invoke(context);

                    buffer.Seek(0, SeekOrigin.Begin);
                    await buffer.CopyToAsync(stream);
                    context.Response.Body = buffer;


                }
                catch (Exception exception)
                {
                    buffer.Seek(0, SeekOrigin.Begin);
                    await buffer.CopyToAsync(stream);
                    context.Response.Body = stream;

                    BaseException baseException = exception as BaseException;

                    BaseError respError = null;

                    if (baseException != null)
                    {

                        respError = new BaseError(baseException);
                        context.Response.StatusCode = baseException.ResponseCode;
                    }
                    else
                    {
                        respError = new BaseError((int)HttpStatusCode.InternalServerError, exception.Message,
                            exception.ToString());
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    }
                        

                    string json =
                       JsonConvert.SerializeObject(
                         respError,
                         Formatting.Indented,
                         new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }
                       );

                    byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    await context.Response.Body.WriteAsync(byteArray, 0, byteArray.Length);
                }
            }
        }


    }
}
