﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Middleware
{
    public static class ExceptThisUrl
    {
        public static bool IsGoForward(this string path)
        {
            path = path.ToLower();
            if (path.StartsWith("/api/Picture".ToLower()))
            {
                return true;
            }
            //if (path.StartsWith("/api/AdminPicture/UploadSportMobile".ToLower()))
            //{
            //    return true;
            //}
            //if (path.StartsWith("/api/AdminPicture/UploadSportIcon".ToLower()))
            //{
            //    return true;
            //}
            //if (path.StartsWith("/api/AdminPicture/UploadImageCompetition".ToLower()))
            //{
            //    return true;
            //}
            return false;
        }
    }
}
