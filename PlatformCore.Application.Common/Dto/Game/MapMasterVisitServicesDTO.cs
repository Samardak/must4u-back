﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Dto.Game
{
    public class MapMasterVisitServicesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long MasterVisitId { get; set; }
        public int ServiceTypeId { get; set; }
        public bool IsDeleted { get; set; }
    }
}
