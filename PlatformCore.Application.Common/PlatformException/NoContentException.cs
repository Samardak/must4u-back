﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.PlatformException
{
    public class NoContentException : Exception
    {
        public NoContentException(string messsage) : base(messsage)
        {
            
        }
    }
}
