﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PlatformCore.Common.PlatformException
{
    public class BaseException : Exception
    {
        
        public CodeError.CodeError Code { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public int ResponseCode { get; set; }

        public BaseException()
        {

        }

        public BaseException(CodeError.CodeError code, string message, string details = null, HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest)
        {
            this.Code = code;
            this.Message = message;
            this.Details = details;
            this.ResponseCode = (int)httpStatusCode;
        }


    }
}
