﻿namespace GameBLL.Settings
{
    public class AppSettings
    {
        public string FullPathToFileLog { get; set; }

        public string UrlDefaultPictureWhenRegister { get; set; }

        public string AccountingUrl { get; set; }
        public string ThirdPartyAuthWebApiUrl { get; set; }
        public long AllowWinAmountForBigWin { get; set; }


        public string ClientUrl { get; set; }
        public string ProxyPlayerUrl { get; set; }



        //this values used for link + verify code (email link) 
        //example  :   http://someDomain:99/api/{controller}{action}?input=TokenTokenToken
        public string UrlForgotPassword { get; set; }

        public string UrlConfirmation { get; set; }
        public string PathToGameWebApiXml { get; set; }
    }
}
