﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    public interface IVisitMasterService
    {
        Task<GetVisitsOutput> GetVisits(GetVisitsInput input);
        Task<object> ClientSetVisitToMaster(SetVisitsInput input);
    }

    public class VisitMasterService : IVisitMasterService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Users> _repoUser;
        private readonly IDateConvertorServiceInternal _convertorServiceInternal;
        private readonly IRepository<MasterServices, MasterServicesDTO> _repoMasterServiceDto;
        private readonly IRepository<ServicesType, ServicesTypeDTO> _repoServiceType;
        private readonly IRepository<MasterVisits> _repoMasterVisits;
        private readonly IRepository<MasterSchedules> _repoMasterSchedule;
        private readonly IMasterScheduleService _masterScheduleService;
        private readonly INotificationService _notificationService;
        private readonly IRepository<MapMasterVisitPictures> _repoMapMasterVisitPicute;
        private readonly IRepository<MapMasterVisitServices> _repoMapMasterViistService;
        private readonly IPictureService _pictureService;

        public VisitMasterService(ISessionService sessionService, 
            IRepository<Users> repoUser, IDateConvertorServiceInternal convertorServiceInternal, 
            IRepository<MasterServices, MasterServicesDTO> repoMasterServiceDto, 
            IRepository<ServicesType, ServicesTypeDTO> repoServiceType, 
            IMasterScheduleService masterScheduleService, 
            IRepository<MasterVisits> repoMasterVisits, 
            IRepository<MasterSchedules> repoMasterSchedule, 
            INotificationService notificationService, 
            IRepository<MapMasterVisitPictures> repoMapMasterVisitPicute, 
            IRepository<MapMasterVisitServices> repoMapMasterViistService, 
            IPictureService pictureService)
        {
            _sessionService = sessionService;
            _repoUser = repoUser;
            _convertorServiceInternal = convertorServiceInternal;
            _repoMasterServiceDto = repoMasterServiceDto;
            _repoServiceType = repoServiceType;
            _masterScheduleService = masterScheduleService;
            _repoMasterVisits = repoMasterVisits;
            _repoMasterSchedule = repoMasterSchedule;
            _notificationService = notificationService;
            _repoMapMasterVisitPicute = repoMapMasterVisitPicute;
            _repoMapMasterViistService = repoMapMasterViistService;
            _pictureService = pictureService;
        }

        public async Task<GetVisitsOutput> GetVisits(GetVisitsInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var master = await _repoUser.FirstOrDefault(i => i.Id == input.Id);
            if(master == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            var dtStart = _convertorServiceInternal.ConvertToDateFromMs(input.DateTimeStart);
            if (dtStart.DayOfWeek != DayOfWeek.Monday)
                throw new BaseException(CodeError.BadRequeast, "DayOfWeek of date time start should be as Monday");

            #region here we find a list of service type for current master
            var serviceTypeList = await _repoServiceType.GetList();
            var services = await _repoMasterServiceDto.GetList(i => !i.IsDeleted && i.UserId == input.Id);
            var servicesRet = new List<MasterServiceDiscription>();
            foreach (var masterServicese in services)
            {
                var item = new MasterServiceDiscription();
                item.ServicesTypeId = masterServicese.ServicesTypeId;
                item.ServicesTypeName = serviceTypeList.FirstOrDefault(i => i.Id == masterServicese.Id)?.Name;
                servicesRet.Add(item);
            }
            #endregion here we find a list of service type for current master

            var masterVisits = _repoMasterVisits
                .Queryable
                .Where(i => !i.IdDeleted && i.MasterId == master.Id)
                .ToList();

            #region here we find a list of master's time
            List<VisitsOutput> retList = new List<VisitsOutput>();
            for (int i = 0; i < 6; i++)
            {
                var retItem = new VisitsOutput();
                var currentDay = dtStart.AddDays(i);

                var it = await _masterScheduleService.GetDayScheduleForMaster(new GetDayScheduleForMasterInput
                {
                    Id = input.Id,
                    Token = session.Token,
                    Day = currentDay.Day,
                    Month = currentDay.Month,
                    Year = currentDay.Year
                });
                retItem.DayOfWeek = currentDay.DayOfWeek;
                retItem.MasterTimes = it.MyTimes.Select(d => new MasterTimes
                {
                    Id = d.Id,
                    StartTime = d.StartTime,
                    EndTime = d.EndTime,
                    IsFree = masterVisits.FirstOrDefault(find=> find.MasterScheduleId == d.Id) == null 
                    

                })
                .ToList();
                retList.Add(retItem);
            }
            #endregion here we find a list of master's time

            var ret = new GetVisitsOutput
            {
                Services = servicesRet,
                MasterTimes = retList
            };

            return ret;
        }

        /// <summary>
        /// клиент делает запрос на запись к мастеру только на свободное время!
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<object> ClientSetVisitToMaster(SetVisitsInput input)
        {
            var session = _sessionService.GetSessionData(input?.Token);

            var masterSchedule = await _repoMasterSchedule.
                FirstOrDefault(i => !i.IsDeleted && 
                i.Id == input.MasterScheduleId);

            if((input.MasterServiceTypes?.Count ?? 0) < 1)
                throw new BaseException(CodeError.BadRequeast, $"MasterServiceTypes can't be empty");

            if (masterSchedule == null)
                throw new BaseException(CodeError.BadRequeast, $"Does not exist master service id {input.MasterScheduleId}");

            var masterVisit = await _repoMasterVisits.FirstOrDefault(i => 
            !i.IdDeleted 
            && i.MasterId == masterSchedule.UserId 
            && i.IsMasterConfirm);

            if(masterVisit != null)
                throw new BaseException(CodeError.BadRequeast, "This time already taken");

            #region check serviceType

            var listvalidService = _repoMasterServiceDto.Queryable
                .Where(i => !i.IsDeleted && i.UserId == masterSchedule.UserId)
                .ToList();

            var listServiceTypeId = listvalidService.Select(i => i.ServicesTypeId);
            var invalidInputServiceType = input.MasterServiceTypes?.Except(listServiceTypeId).ToList();
            if (invalidInputServiceType?.Count > 0)
            {
                var strNotEixist = $"[{string.Join(",",invalidInputServiceType)}]";
                throw new BaseException(CodeError.BadRequeast, "Does not exsit service type id " + strNotEixist);
            }

            #endregion check serviceType

            var isAnyPictureNullOrEmpty = input.Pictures?.Any(string.IsNullOrEmpty);
            if (isAnyPictureNullOrEmpty.HasValue && isAnyPictureNullOrEmpty.Value)
                throw new BaseException(CodeError.BadRequeast, "Pass picture url can't be null or empty");
            
            var masterVisitCreated = await _repoMasterVisits.Insert(new MasterVisits
            {
                DateCreation = DateTimeOffset.Now,
                IdDeleted = false,
                IsMasterConfirm = false,
                IsUserConfirm = true,
                MasterId = masterSchedule.UserId,
                MasterScheduleId = masterSchedule.Id,
                UserId = session.UserId,
            });

            #region create reference to map master services
            for (var index = 0; index < input.MasterServiceTypes?.Count; index++)
            {
                var service = input.MasterServiceTypes[index];
                await _repoMapMasterViistService.Insert(new MapMasterVisitServices
                {
                    IsDeleted = false,
                    ServiceTypeId = service,
                    MasterVisitId = masterVisitCreated.Id,
                });
            }
            #endregion create reference to map service 


            #region create reference to map master pictures

            for (var index = 0; index < input.Pictures?.Count; index++)
            {
                var picture = input.Pictures[index];

                var createdPicture = await _pictureService.SavePicture(new BaseInputAndString
                {
                    Token = input.Token,
                    UrlPicture = picture
                });

                await _repoMapMasterVisitPicute.Insert(new MapMasterVisitPictures
                {
                    MasterVisitId = masterVisitCreated.Id,
                    PicutureId = createdPicture.Id
                });
            }

            #endregion



            await _notificationService.NotifyNewMasterVisit(new object());

            return new object();
        }

       


    }

    public class SetVisitsInput : BaseInput
    {
        [Required]
        public long MasterScheduleId { get; set; }
        [Required]
        public List<int> MasterServiceTypes { get; set; }
        [Required]
        public List<string> Pictures { get; set; }
    }

    public class GetVisitsOutput
    {
        public List<MasterServiceDiscription> Services { get; set; }
        public List<VisitsOutput> MasterTimes { get; set; }
    }

    public class GetVisitsInput : BaseInputById
    {
        [Required]
        public long DateTimeStart { get; set; }
    }

    public class VisitsOutput
    {
        public DayOfWeek DayOfWeek { get; set; }
        public List<MasterTimes> MasterTimes { get; set; }
    }

    public class MasterServiceDiscription
    {
        public int ServicesTypeId { get; set; }
        public string ServicesTypeName { get; set; }
    }

    public class MasterTimes : MasterScheduleDayOfWeeksInputOutput
    {
        public bool IsFree { get; set; }
    }
}
