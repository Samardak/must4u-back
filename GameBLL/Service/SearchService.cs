﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using GameBLL.Service.InternalService;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;
using StackExchange.Redis;

namespace GameBLL.Service
{
    public interface ISearchService
    {
        Task<List<CitiesDTO>> SearchCities(GetCities input);
        Task<List<StationRegionsDTO>> SearchStationRegions(GetStation input);


        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/bbd00c11-6214-43b0-8368-1e40d8c862ae/-
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<SearchMastersCountOut> SearchMastersCount(SearchMasterCountInp input);
        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/bbd00c11-6214-43b0-8368-1e40d8c862ae/-
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<List<SearchMasterOut>> SearchMasters(SearchMasterInp input);
        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </summary>
        /// <returns></returns>
        Task<SearchMastersCountOut> FullSearchMastersCount(FullSearchMasterCountInp input);
        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </summary>
        /// <returns></returns>
        Task<List<SearchMasterOut>> FullSearchMasters(FullSearchMasterInp input);
        
    }

    public class SearchService : ISearchService
    {
        private readonly IRepository<Countries, CountriesDTO> _repoCountryDto;
        private readonly IRepository<Cities, CitiesDTO> _repoCityDto;
        private readonly IRepository<StationRegions, StationRegionsDTO> _repoStationDto;


        private readonly IRepository<Users> _repoUsers;
        private readonly IRepository<MasterSchedules> _repoScheduleSrervice;

        private readonly ISkipCountCheckService _skipCountCheckService;

        public SearchService(IRepository<Countries, CountriesDTO> repoCountryDto, 
            IRepository<Cities, CitiesDTO> repoCityDto, 
            IRepository<StationRegions, StationRegionsDTO> repoStationDto, 
            
            IRepository<Users> repoUsers, IRepository<MasterSchedules> repoScheduleSrervice, ISkipCountCheckService skipCountCheckService)
        {
            _repoCountryDto = repoCountryDto;
            _repoCityDto = repoCityDto;
            _repoStationDto = repoStationDto;

            _repoUsers = repoUsers;
            _repoScheduleSrervice = repoScheduleSrervice;
            _skipCountCheckService = skipCountCheckService;
        }

        public async Task<List<CitiesDTO>> SearchCities(GetCities input)
        {
            var country = await _repoCountryDto.FirstOrDefault(i => i.Id == input.CountryId);
            if (country == null)
                throw new BaseException(CodeError.InvalidCountryId, CodeError.InvalidCountryId.GetMessage() + $" : {input.CountryId}");

            var citiesQuary = _repoCityDto.Queryable.Where(i => i.CountryId == input.CountryId);


            if(!string.IsNullOrEmpty(input.PartOfCity)) 
                citiesQuary = citiesQuary.Where(i => i.City.StartsWith(input.PartOfCity, StringComparison.OrdinalIgnoreCase)).Take(10);
            else
                citiesQuary = citiesQuary.OrderBy(i=> i.City).Take(10);
            var resp = await citiesQuary.ToListAsync();

            return resp.Select(Mapper.Map<CitiesDTO>).ToList();
        }
        public async Task<List<StationRegionsDTO>> SearchStationRegions(GetStation input)
        {
            var city = await _repoCityDto.FirstOrDefault(i => i.Id == input.CityId);
            if (city == null)
                throw new BaseException(CodeError.InvalidCountryId, CodeError.InvalidCountryId.GetMessage() + $" : {input.CityId}");

            var regionStantionQuery = _repoStationDto.Queryable.Where(i => i.CityId == input.CityId);


            if (!string.IsNullOrEmpty(input.PartStationRegion))
                regionStantionQuery = regionStantionQuery.Where(i => i.StationName.ToLower().Contains(input.PartStationRegion.ToLower()));

            regionStantionQuery = regionStantionQuery.Take(10);

            var resp = await regionStantionQuery.ToListAsync();

            return resp.Select(Mapper.Map<StationRegionsDTO>).ToList();
        }


        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/bbd00c11-6214-43b0-8368-1e40d8c862ae/-
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<SearchMastersCountOut> SearchMastersCount(SearchMasterCountInp input)
        {
            if (input.AvgPrice < 1)
                throw new BaseException(CodeError.BadRequeast, "AvgPrice value should be greater then 0");

            var query = _repoUsers.Queryable.Where(i=> i.UserTypeId == (int)EnumUserTypes.Master);

            if (input.IsManicure)
                query = query.Where(i => i.IsManicure);

            if (input.IsPedicure)
                query = query.Where(i => i.IsPedicure);

            if (input.Weekdays && !input.Weekend)
            {
                query = query
                    .Include(i => i.MasterSchedules)
                    .Where(i => i.MasterSchedules.Any(it => it.DayOfWeek < 6));
            }
            if (input.Weekend && !input.Weekdays)
            {
                query = query
                    .Include(i => i.MasterSchedules)
                    .Where(i => i.MasterSchedules.Any(it => it.DayOfWeek > 5));
            }

            if (input.StationRegionId.HasValue)
            {
                query = query.Where(i => i.StationRegionId == input.StationRegionId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster, query);
            }

            if (input.CityId.HasValue)
            {
                query = query.Where(i => i.CityId == input.CityId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster,query);
            }

            if (input.CityId.HasValue)
            {
                query = query.Where(i => i.CountryId == input.CountryId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster,query);
            }

            var countMasters = query.Count();
            return new SearchMastersCountOut(countMasters,query);

        }

        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/bbd00c11-6214-43b0-8368-1e40d8c862ae/-
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<List<SearchMasterOut>> SearchMasters(SearchMasterInp input)
        {
            var result = await this.SearchMastersCount(input);
            var query = result.Queryable;

            (input.Skip,input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);

            query = query.Skip(input.Skip).Take(input.Count);
            var ret = query.ToList();

            var retItem = ret.Select(i => new SearchMasterOut{ UserId = i.Id, FirstName = i.FirstName, LastName = i.LastName, Picture = i.PictureUrl})
                            .ToList();
            return retItem;
        }


        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </summary>
        /// <returns></returns>
        public async Task<SearchMastersCountOut> FullSearchMastersCount(FullSearchMasterCountInp input)
        {
            var query = _repoUsers.Queryable.Where(i => i.UserTypeId == (int)EnumUserTypes.Master);

            if (input.IsManicure)
                query = query.Where(i => i.IsManicure);

            if (input.IsPedicure)
                query = query.Where(i => i.IsPedicure);
            

            if (input.StationRegionId.HasValue)
            {
                query = query.Where(i => i.StationRegionId == input.StationRegionId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster, query);
            }

            if (input.CityId.HasValue)
            {
                query = query.Where(i => i.CityId == input.CityId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster, query);
            }

            if (input.CityId.HasValue)
            {
                query = query.Where(i => i.CountryId == input.CountryId.Value);
                var countMaster = query.Count();
                return new SearchMastersCountOut(countMaster, query);
            }

            var countMasters = query.Count();
            return new SearchMastersCountOut(countMasters, query);
        }

        /// <summary>
        /// https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </summary>
        /// <returns></returns>
        public async Task<List<SearchMasterOut>> FullSearchMasters(FullSearchMasterInp input)
        {
            var result = await this.FullSearchMastersCount(input);
            var query = result.Queryable;

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);

            query = query.Skip(input.Skip).Take(input.Count);
            var ret = query.ToList();

            var retItem = ret.Select(i => new SearchMasterOut { UserId = i.Id, FirstName = i.FirstName, LastName = i.LastName, Picture = i.PictureUrl })
                .ToList();
            return retItem;
        }

    }

    public class SearchMasterInp : SearchMasterCountInp
    {
        public int Skip { get; set; }
        public int Count { get; set; }
    }

    public class SearchMastersCountOut
    {
        public long MasterCount { get; set; }

        [JsonIgnore]
        public IQueryable<Users> Queryable { get; set; }

        public SearchMastersCountOut(){}

        public SearchMastersCountOut(long masterCount, IQueryable<Users> query)
        {
            this.Queryable = query;
            this.MasterCount = masterCount;
        }
    }

    public class SearchMasterCountInp
    {
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public long? StationRegionId { get; set; }
        public bool Weekdays { get; set; }//
        public bool Weekend { get; set; }//
        public bool IsManicure { get; set; }//
        public bool IsPedicure { get; set; }//
        [Required]
        public long AvgPrice { get; set; }//
    }


    public class FullSearchMasterCountInp
    {
        public long? CountryId { get; set; }
        public long? CityId { get; set; }
        public long? StationRegionId { get; set; }
        
        public bool IsManicure { get; set; }//
        public bool IsPedicure { get; set; }//

        public bool SortByWorkingExperience { get; set; }
        public bool SortByRate { get; set; }
        public bool SortAscPrice { get; set; }
        public bool SortDescPrice { get; set; }
        public bool SortComeAtHome { get; set; }
    }

    public class FullSearchMasterInp : FullSearchMasterCountInp
    {
        public int Skip { get; set; }
        public int Count { get; set; }
    }


    public class GetCities : BaseInput
    {
        [Required]
        public long CountryId { get; set; }
        public string PartOfCity { get; set; }
    }

    public class GetStation : BaseInput
    {
        [Required]
        public long CityId { get; set; }
        public string PartStationRegion { get; set; }
    }


    public class SearchMasterOut
    {
        public long UserId { get; set; }
        public string Picture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => FirstName + LastName;
    }
}
