﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBLL.Service.InternalService
{
    public interface ISkipCountCheckService
    {
        (int, int) CheckSkipCount(int skip, int count);
    }
    public class SkipCountCheckService : ISkipCountCheckService
    {
        public (int, int) CheckSkipCount(int skip, int count)
        {
            if (count > 50) count = 10;
            if (count < 1) count = 1;

            return (skip, count);
        }
    }
}
