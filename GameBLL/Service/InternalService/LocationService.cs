﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Common.Game.InputOutput.IRegistrationService;

namespace GameBLL.Service.InternalService
{
    public class LocationService
    {
        private readonly IRepository<Countries> _repoCountries;
        private readonly IRepository<Cities> _repoCities;

        public LocationService(IRepository<Countries> repoCountries, 
            IRepository<Cities> repoCities)
        {
            _repoCountries = repoCountries;
            _repoCities = repoCities;
        }

        public async Task<Location> FindLocationOrCreateIfNotExist(IpData input)
        {
            if(string.IsNullOrEmpty(input?.City) || string.IsNullOrEmpty(input?.Country))
                return new Location{CityId = 1,CountryId = 1};

            var country = _repoCountries.Queryable.Include(i => i.Cities).FirstOrDefault(i => i.CountryCode == input.CountryCode);
            if (country == null)
            {
                country = await _repoCountries.Insert(new Countries
                {
                    CountryCode = input.CountryCode,
                    Country = input.Country,
                    Cities = new List<Cities>
                    {
                        new Cities
                        {
                            City = input.City
                        }
                    }
                });
                return new Location
                {
                    CountryId = country.Id,
                    CityId = country.Cities.FirstOrDefault().Id
                };
            }
            else
            {
                var city = country.Cities.FirstOrDefault(i => i.City == input.City);
                if (city == null)
                {
                    city = await _repoCities.Insert(new Cities
                    {
                        City = input.City,
                        CountryId = country.Id
                    });
                }

                return new Location
                {
                    CountryId = country.Id,
                    CityId = city.Id
                };
            }

        }
    }

    public class Location
    {
        public long CountryId { get; set; }
        public long CityId { get; set; }
    }
}
