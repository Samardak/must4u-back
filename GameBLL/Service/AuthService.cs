﻿using System;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service.InternalService;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Data;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.Game.GameTokenService;
using PlatformCore.Common.Game.InputOutput.IRegistrationService;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    public interface IAuthService
    {
        Task<RegisterOutput> Register(RegisterInput input);
        Task<LoginOutput> RegisterAndLogin(RegisterInput input);
        Task<LoginOutput> Login(LoginInput input);
        Task<object> LogOut(BaseInput input);

    }
    public class AuthService : IAuthService
    {
        private readonly IRepository<Users,UsersDTO> _repoWithDtoUsers;
        private readonly IRepository<AuthDatas,AuthDatasDTO> _repoAuthDatas;
        private readonly ISessionService _sessionService;
        private readonly ITokenService _tokenService;
        private readonly LocationService _locationService;

        public AuthService(IRepository<Users,UsersDTO> repositoryWithDtoUsers, IRepository<AuthDatas,AuthDatasDTO> repoAuthDatas, ISessionService sessionService, ITokenService tokenService, LocationService locationService)
        {
            _repoWithDtoUsers = repositoryWithDtoUsers;
            _repoAuthDatas = repoAuthDatas;
            _sessionService = sessionService;
            _tokenService = tokenService;
            _locationService = locationService;
        }

        public async Task<LoginOutput> RegisterAndLogin(RegisterInput input)
        {
            await this.Register(input);
            var resp = await this.Login(new LoginInput
            {
                Email = input.Email,
                Password = input.Password,
                UserType = input.UserType
            });
            return resp;
        }

        public async Task<RegisterOutput> Register(RegisterInput input)
        {
            var authData = await _repoAuthDatas.FirstOrDefault(i => i.AuthTypeId == ((int) EnumAuthTypes.Username) && i.Username == input.Email && i.UserTypeId == (int)input.UserType);
            if(authData != null)
                throw new BaseException(CodeError.UsernameAlreadyTaken, CodeError.UsernameAlreadyTaken.GetMessage());

            var location = await _locationService.FindLocationOrCreateIfNotExist(input?.IpData);
            
            var user = new Users
            {
                UserTypeId = (int) input.UserType,
                FirstName = input.FirstName,
                LastName = input.LastName,
                PictureUrl = "http://coyotechronicle.net/wp-content/uploads/2015/02/facebook-logo.jpeg",
                Email = input.Email,
                Currency = "UA",
                CountryId = location.CountryId,
                CityId = location.CityId,
                StationRegionId = null,
                DateCreation = DateTimeOffset.Now,
            };
            
            user = await _repoWithDtoUsers.Insert(user);

            authData = new AuthDatas
            {
                AuthTypeId    = (int)EnumAuthTypes.Username,
                ExternalId = "",
                Password = input.Password,
                Username = input.Email,
                UserTypeId = (int)input.UserType,
                UserId = user.Id,
            };

            authData = await _repoAuthDatas.Insert(authData);
            return new RegisterOutput();
        }

        public async Task<LoginOutput> Login(LoginInput input)
        {
            var authData = await _repoAuthDatas.FirstOrDefault(
                i =>
                    i.AuthTypeId == (int) EnumAuthTypes.Username && 
                    i.UserTypeId == (int) input.UserType &&
                    i.Username == input.Email && 
                    i.Password == input.Password);

            if(authData == null)
                throw new BaseException(CodeError.InvalidCredentials, CodeError.InvalidCredentials.GetMessage());

            var user = _repoWithDtoUsers.Queryable
                .Include(i=> i.Country)
                .Include(i => i.City)
                .Include(i=> i.StationRegion)
                .FirstOrDefault(i => i.Id == authData.UserId);


            var token = _tokenService.GetNewToken(user.Id);
            _sessionService.SetSessionData(new SessionData
            {
                Email = input.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PictureUrl = user.PictureUrl,
                UserId = user.Id,
                Token = token,
                UserType = input.UserType
            });

            


            var login =  new LoginOutput()
            {
                Email = input.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                PictureUrl = user.PictureUrl,
                UserId = user.Id,
                Token = token,
            };

            if (user.Country != null)
                login.CountryData = new CountryData
                {
                    CountryId = user.CountryId,
                    CountryCode = user.Country.CountryCode,
                    CountryName = user.Country.Country
                };
            if(user.City != null)
                login.CityData = new CityData
                {
                    CityId = user.CityId,
                    CityName = user.City.City
                };

            if(user.StationRegion != null)
                login.StationRegionData = new StationRegionData
                {
                    StationRegionId = user.StationRegion.Id,
                    StationRegionName = user.StationRegion.StationName,
                };

            return login;
        }

        public async Task<object> LogOut(BaseInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            await _sessionService.RemoveSessionAsync(input.Token);
            return new object();
        }
    }

    

   

   
}
