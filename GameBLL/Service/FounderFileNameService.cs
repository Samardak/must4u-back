﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBLL.Service
{

    
    public interface IFounderFileNameService
    {
        string FindNameForFile(string pathDicrectory);
    }
    public class FounderFileNameService : IFounderFileNameService
    {
        
        public string FindNameForFile(string pathDicrectory)
        {
            
            var directory = new DirectoryInfo(pathDicrectory);

            var myFile = directory.GetFiles()
                .OrderByDescending(f => f.LastWriteTime)
                .FirstOrDefault();

            var name = myFile?.Name;
            name = name?.Split('.').FirstOrDefault();

            long n = 0;
            Int64.TryParse(name ?? "", out n);

            n = n + 1;
            return n.ToString();
        }
    }
}
