﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    /// <summary>
    /// Update or get profile data masters and clients
    /// </summary>
    public interface IProfileService
    {
        Task<ChangePictureOut> ChangePicture(ChangePictureInput input);
    }
    /// <summary>
    /// Update or get profile data masters and clients
    /// </summary>
    public class ProfileService : IProfileService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Users> _repoUser;
        private readonly IRepository<ServicesType> _repoServiceType;

        public ProfileService(ISessionService sessionService, IRepository<Users> repoUser, 
            IRepository<ServicesType> repoServiceType)
        {
            _sessionService = sessionService;
            _repoUser = repoUser;
            _repoServiceType = repoServiceType;
        }

        public async Task<ChangePictureOut> ChangePicture(ChangePictureInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            var user = await _repoUser.FirstOrDefault(i => !i.IsDeleted && i.Id == session.UserId);

            user.PictureUrl = input.PictureUrl;
            await _repoUser.Update(user);
            return new ChangePictureOut {PictureOut = input?.PictureUrl};
        }

        /// <summary>
        /// Находим профайл мастера по id
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<GetMasterProfileByIdOut> GetMasterProfileById(BaseInputById input)
        {
            var session = _sessionService.GetSessionDataAsync(input?.Token);

            var user = _repoUser.Queryable
                .Include(i=> i.Country)
                .Include(i=> i.City)
                .Include(i=> i.StationRegion)
                .Include(i=> i.MasterServices)
                .Include(i=> i.UserSocialNetworks)
                .FirstOrDefault(i => !i.IsDeleted && i.Id == input.Id && i.UserTypeId == (int)EnumUserTypes.Master);
            if(user == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            GetMasterProfileByIdOut output = null;

            List<ServicesType> serviceTypes = await _repoServiceType.GetList();
            List<MasterServicesOut> masterServices = user
                .MasterServices
                .Where(i => !i.IsDeleted)
                .ToList()
                .Select(i=> new MasterServicesOut
                {
                    Id = i.Id,
                    UserId = i.UserId,
                    ServicesTypeId = i.ServicesTypeId,
                    Price = i.Price,
                    DuringTimeMinutes = i.DuringTimeMinutes,
                    ServicesTypeName = serviceTypes.FirstOrDefault(it=> it.Id == i.ServicesTypeId)?.Name
                })
                .OrderBy(i=> i.ServicesTypeId)
                .ToList();

            output = new GetMasterProfileByIdOut
            {
                Country = user.Country?.Country,
                City = user.City?.City,
                StationRegion = user.StationRegion?.StationName,
                

                WorkingExperience = user.WorkingExperience,
                IsOnlyHome = user.IsOnlyHome,
                AboutText = user.AboutText,
                
                MasterServices = masterServices,
                UserSocialNetwork = AutoMapper.Mapper.Map<UserSocialNetworksDTO>(user.UserSocialNetworks.FirstOrDefault()),

                RateTotal = 1,
                RateAttitudesToClient = 1,
                RateNeatness = 1,
                RateQualityOfWork = 1
            };

            return output;
        }

        /// <summary>
        /// Возвращает страницу для мастера
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<MyMasterProfile> MyMasterProfile(BaseInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input?.Token);

            var masterPage = await this.GetMasterProfileById(new BaseInputById
            {
                Id = session.UserId,
                Token = input.Token
            });

            return new MyMasterProfile(
                input: masterPage, 
                viewsToday: 10, 
                viewsThisMonth: 131, 
                placeInTop: 3, 
                allCountUser: 58123);
        }
    }

    public class MyMasterProfile : GetMasterProfileByIdOut
    {
        public int ViewsToday { get; set; }
        public int ViewsThisMonth { get; set; }
        public int PlaceInTop { get; set; }
        public int AllCountUser { get; set; }

        public MyMasterProfile(GetMasterProfileByIdOut input, int viewsToday, int viewsThisMonth, int placeInTop, int allCountUser)
        {
            this.ViewsToday = viewsToday;
            this.ViewsThisMonth = viewsThisMonth;
            this.PlaceInTop = placeInTop;
            this.AllCountUser = allCountUser;

            if (input == null)
                return;
            this.Country = input.Country;
            this.City = input.City;
            this.StationRegion = input.StationRegion;

            this.RateTotal = input.RateTotal;
            this.RateNeatness = input.RateNeatness;
            this.RateQualityOfWork = input.RateQualityOfWork;
            this.RateAttitudesToClient = input.RateAttitudesToClient;

            this.WorkingExperience = input.WorkingExperience;
            this.IsOnlyHome = input.IsOnlyHome;
            this.AboutText = input.AboutText;

            this.MasterServices = input.MasterServices;
            this.UserSocialNetwork = input.UserSocialNetwork;


        }

        public MyMasterProfile()
        {
            
        }
    }

    public class GetMasterProfileByIdOut
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string StationRegion { get; set; }

        public int RateTotal { get; set; }
        public int RateNeatness { get; set; }
        public int RateQualityOfWork { get; set; }
        public int RateAttitudesToClient { get; set; }

        public int WorkingExperience { get; set; }
        public bool IsOnlyHome { get; set; }
        public string AboutText { get; set; }

        public List<MasterServicesOut> MasterServices { get; set; }
        public UserSocialNetworksDTO UserSocialNetwork { get; set; }
    }

    public class ChangePictureOut
    {
        public string PictureOut { get; set; }
    }

    public class ChangePictureInput : BaseInput
    {
        [Required]
        public string PictureUrl { get; set; }
    }
    
}
