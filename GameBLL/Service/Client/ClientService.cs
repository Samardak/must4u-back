﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service.Client
{
    public class ClientService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Users> _repoUsers;

        public ClientService(ISessionService sessionService, IRepository<Users> repoUsers)
        {
            _sessionService = sessionService;
            _repoUsers = repoUsers;
        }

        public async Task<ClientProfileOut> Profile(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var user = _repoUsers.Queryable
                .FirstOrDefault(i => !i.IsDeleted && i.Id == input.Id && i.UserTypeId == (int)EnumUserTypes.Client);

            if (user == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            return new ClientProfileOut
            {
                UserId = user.Id,
                PictureUrl = user.PictureUrl,
                OnlineText = "Online",
                FullName = $"{user.FirstName} {user.LastName}",
                IsOnline = true
            };
        }
    }

    public class ClientProfileOut
    {
        public long UserId { get; set; }
        public string PictureUrl { get; set; }
        public string FullName { get; set; }
        public string OnlineText { get; set; }
        public bool IsOnline { get; set; }
    }
}
