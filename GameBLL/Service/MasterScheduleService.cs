﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    public interface IMasterScheduleService
    {
        Task<GetWeekScheduleOutput> GetWeekSchedule(BaseInput input);
        Task<object> SetWeekSchedule(GetWeekScheduleInput input);





        Task<GetDayScheduleOutput> GetDaySchedule(GetDayScheduleInput input);
        Task<object> SetDayScheduleEveryWeek(SetDayScheduleInput input);
        Task<object> SetDayScheduleCurrentDay(SetDayScheduleInput input);
        Task<object> SetDayScheduleFreeDay(SetDayScheduleInput input);

        Task<GetDayScheduleOutput> GetDayScheduleForMaster(GetDayScheduleForMasterInput input);
    }
    public class MasterScheduleService : IMasterScheduleService
    {
        private readonly ISessionService _sessionService;
        
        private readonly IRepository<MasterSchedules, MasterSchedulesDTO> _repoMasterSchedule;

        public MasterScheduleService(ISessionService sessionService, IRepository<MasterSchedules, MasterSchedulesDTO> repoMasterSchedule)
        {
            _sessionService = sessionService;
            _repoMasterSchedule = repoMasterSchedule;
        }


        
        public async Task<GetWeekScheduleOutput> GetWeekSchedule(BaseInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            var dayOfWeek = _repoMasterSchedule.Queryable.Where(i => !i.IsDeleted && i.UserId == session.UserId && i.IsWeekly)
                .OrderBy(i => i.DayOfWeek)
                .ToList();


            var ret = new GetWeekScheduleOutput { MyDays = new List<GetWeeokScheduleList>() };

            var distWeakOfDay = dayOfWeek.Select(i => i.DayOfWeek).Distinct();
            foreach (var day in distWeakOfDay)
            {
                var list = dayOfWeek.Where(i => i.DayOfWeek == day);
                var outputItem = new GetWeeokScheduleList
                {
                    DayOfWeek = (DayOfWeek)day,
                    MyTimes = list
                    .Select(i => new MasterScheduleDayOfWeeksInputOutput
                    {
                        Id = i.Id,
                        StartTime = i.StartTime,
                        EndTime = i.EndTime
                    })
                    .OrderBy(i => i.StartTime).ToList()
                };
                ret.MyDays.Add(outputItem);
            }
            return ret;
        }

        public async Task<object> SetWeekSchedule(GetWeekScheduleInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);


            #region check valid value of dayOfWeek
            var dayOfWeek = input.MyDays.Select(i => i.DayOfWeek).Distinct();
            foreach (var inpDayOfWeek in dayOfWeek)
            {
                if (inpDayOfWeek < DayOfWeek.Sunday || inpDayOfWeek > DayOfWeek.Saturday)
                    throw new BaseException(CodeError.BadRequeast, "Используйте значения дня дней недели от 0 до 6 (0:Воскресенье, 6:Суббота)");
            }
            #endregion check valid value of dayOfWeek


            //1 проверяем, вдруг есть одинаковые дни 
            //2 проверяем адекватность данных во времени работы 
            //(что бы старт был меньше чем конец, и следущее время было меньше чем конец предыдущего)

            #region check dublicate day off week
            var dublicateDayOfWeek = from day in input.MyDays
                                     group day by day.DayOfWeek
                                     into g
                                     where g.Count() > 1
                                     select new { DayOfWeek = g.Key };
            var lisDublicateDayOffWeek = dublicateDayOfWeek.Select(i => i.DayOfWeek).ToList();
            if (lisDublicateDayOffWeek.Count > 0)
            {
                var str = $" [{string.Join(",", lisDublicateDayOffWeek.ToList())}]";
                throw new BaseException(CodeError.YouPassDubliacteDayOffWeek, CodeError.YouPassDubliacteDayOffWeek.GetMessage() + str);
            }
            #endregion check dublicate day off week

            var allTime = new List<MasterScheduleDayOfWeeksInputOutput>();
            foreach (var getWeeokScheduleList in input.MyDays)
            {
                foreach (var time in getWeeokScheduleList.MyTimes)
                {
                    time.DayOfWeek = getWeeokScheduleList.DayOfWeek;
                    allTime.Add(time);
                }
            }

            #region check dublicate schedule id
            var gr = allTime.Where(i => i.Id > 0).GroupBy(i => i.Id, i => i, (key, g) => new { key = key, count = g.Count() }).ToList();
            var dublicateId = gr.Where(i => i.count > 1).Select(i => i.key).ToList();
            if (dublicateId.Count > 0)
            {
                var str = $" [{string.Join(",", dublicateId.ToList())}]";
                throw new BaseException(CodeError.YouPassDubliacteScheduleId, CodeError.YouPassDubliacteScheduleId.GetMessage() + str);
            }
            #endregion check dublicate schedule id

            #region check valid time in my day
            foreach (var myDay in input.MyDays)
            {
                this.Check(myDay);
            }
            #endregion check valid time in my day

            //1. update and remove  already exist in db
            //2. create new schedule

            #region check valid data of master schedule id
            var schedulers = await _repoMasterSchedule.GetList(i => !i.IsDeleted && i.UserId == session.UserId);
            var schedulersId = schedulers.Select(i => i.Id).ToList();
            var notExistId = allTime.Where(i => i.Id > 0 && !schedulersId.Contains(i.Id)).Select(i=>i.Id).ToList();
            if (notExistId.Count > 0)
            {
                var str = $"[{string.Join(",", notExistId)}]";
                throw new BaseException(CodeError.BadRequeast, "Does not exist list of this master schedule id : " + str);
            }
            #endregion check valid data of master schedule id

            #region update and delete schedules
            foreach (var schedule in schedulers)
            {
                var current = allTime.FirstOrDefault(i => i.Id == schedule.Id);
                if (current == null)
                {
                    schedule.IsDeleted = true;
                    await _repoMasterSchedule.Update(schedule);
                }
                else
                {
                    schedule.StartTime = current.StartTime;
                    schedule.EndTime = current.EndTime;
                    schedule.DayOfWeek = (int)current.DayOfWeek;
                    await _repoMasterSchedule.Update(schedule);
                }
            }
            #endregion update and delete schedules

            //2
            #region create new schedule
            var allNew = allTime.Where(i => i.Id == 0).ToList();
            foreach (var newSchedule in allNew)
            {
                await _repoMasterSchedule.Insert(new MasterSchedules
                {
                    UserId = session.UserId,
                    StartTime = newSchedule.StartTime,
                    EndTime = newSchedule.EndTime,
                    IsWeekly = true,
                    DayOfWeek = (int)newSchedule.DayOfWeek,
                    IsDeleted = false,
                });
            }
            #endregion create new schedule


            
            return new object();

        }






        public async Task<GetDayScheduleOutput> GetDaySchedule(GetDayScheduleInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            var scheduleForThisDay = _repoMasterSchedule.Queryable.Where(i =>
                !i.IsWeekly &&
                !i.IsDeleted &&
                i.UserId == session.UserId &&
                i.Nyear == input.Year &&
                i.Nmonth == input.Month &&
                i.Nday == input.Day).ToList();

            if (scheduleForThisDay.Any())
            {
                var allFreeDay = scheduleForThisDay.All(i => i.IsFreeDay);
                if (allFreeDay)
                {
                    var retR =  new GetDayScheduleOutput
                    {
                        ScheduleDayMode = EnumSetDaySchedule.FreeDay,
                        MyTimes = scheduleForThisDay.Select(i=> new MasterScheduleDayOfWeeksInputOutput
                        {
                            Id  = i.Id,
                            StartTime = i.StartTime,
                            EndTime = i.EndTime
                        })
                        .OrderBy(i=> i.StartTime)
                        .ToList()
                    };


                    #region если заглужки мы создали сами она не должна отображаться в системе 
                    if (retR.MyTimes.Count == 1)
                    {
                        var f = retR.MyTimes.FirstOrDefault();
                        if (new TimeSpan(0, 1, 3, 3, 3).Subtract(f.StartTime).TotalSeconds == 0 && new TimeSpan(0, 11, 3, 3, 3).Subtract(f.EndTime).TotalSeconds == 0)
                        {
                            retR.MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>();
                        }
                    }
                    return retR;

                    #endregion если заглужки мы создали сами она не должна отображаться в системе 
                }
                var allNotFreeDay = scheduleForThisDay.All(i => !i.IsFreeDay);
                if (allNotFreeDay)
                {
                    return new GetDayScheduleOutput
                    {
                        ScheduleDayMode = EnumSetDaySchedule.OnlyThisDay,
                        MyTimes = scheduleForThisDay.Select(i => new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = i.Id,
                                StartTime = i.StartTime,
                                EndTime = i.EndTime
                            })
                            .OrderBy(i => i.StartTime)
                            .ToList()
                    };
                }


                throw new BaseException(CodeError.InternalServerIncorrectData, CodeError.InternalServerIncorrectData.GetMessage());

            }


            DateTime dateTime;
            #region cast to datetimeoffset
            try
            {
                dateTime = new DateTime(year: input.Year, month: input.Month, day: input.Day);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect vale of (year or month or day). Please check your date");
            }
            #endregion cast to datetimeoffset
            var dayOfWeek = dateTime.DayOfWeek;

            var scheduleDayWeek = _repoMasterSchedule.Queryable.Where(i => i.IsWeekly &&
                                                                           !i.IsDeleted &&
                                                                           i.UserId == session.UserId &&
                                                                           i.DayOfWeek == (int)dayOfWeek)
                                                                           .ToList();
            var ret =  new GetDayScheduleOutput
            {
                ScheduleDayMode = EnumSetDaySchedule.AllWeeks,
                MyTimes = scheduleDayWeek.Select(i=>new MasterScheduleDayOfWeeksInputOutput
                    {
                        Id = i.Id,StartTime = i.StartTime,EndTime = i.EndTime
                    })
                .OrderBy(i=> i.StartTime)
                .ToList()
            };


            
            return ret;


        }


        public async Task<GetDayScheduleOutput> GetDayScheduleForMaster(GetDayScheduleForMasterInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);

            var scheduleForThisDay = _repoMasterSchedule.Queryable.Where(i =>
                !i.IsWeekly &&
                !i.IsDeleted &&
                i.UserId == input.Id &&
                i.Nyear == input.Year &&
                i.Nmonth == input.Month &&
                i.Nday == input.Day).ToList();

            if (scheduleForThisDay.Any())
            {
                var allFreeDay = scheduleForThisDay.All(i => i.IsFreeDay);
                if (allFreeDay)
                {
                    var retR = new GetDayScheduleOutput
                    {
                        ScheduleDayMode = EnumSetDaySchedule.FreeDay,
                        MyTimes = scheduleForThisDay.Select(i => new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = i.Id,
                                StartTime = i.StartTime,
                                EndTime = i.EndTime
                            })
                            .OrderBy(i => i.StartTime)
                            .ToList()
                    };


                    #region если заглужки мы создали сами она не должна отображаться в системе 
                    if (retR.MyTimes.Count == 1)
                    {
                        var f = retR.MyTimes.FirstOrDefault();
                        if (new TimeSpan(0, 1, 3, 3, 3).Subtract(f.StartTime).TotalSeconds == 0 && new TimeSpan(0, 11, 3, 3, 3).Subtract(f.EndTime).TotalSeconds == 0)
                        {
                            retR.MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>();
                        }
                    }
                    return retR;

                    #endregion если заглужки мы создали сами она не должна отображаться в системе 
                }
                var allNotFreeDay = scheduleForThisDay.All(i => !i.IsFreeDay);
                if (allNotFreeDay)
                {
                    return new GetDayScheduleOutput
                    {
                        ScheduleDayMode = EnumSetDaySchedule.OnlyThisDay,
                        MyTimes = scheduleForThisDay.Select(i => new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = i.Id,
                                StartTime = i.StartTime,
                                EndTime = i.EndTime
                            })
                            .OrderBy(i => i.StartTime)
                            .ToList()
                    };
                }


                throw new BaseException(CodeError.InternalServerIncorrectData, CodeError.InternalServerIncorrectData.GetMessage());

            }


            DateTime dateTime;
            #region cast to datetimeoffset
            try
            {
                dateTime = new DateTime(year: input.Year, month: input.Month, day: input.Day);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect vale of (year or month or day). Please check your date");
            }
            #endregion cast to datetimeoffset
            var dayOfWeek = dateTime.DayOfWeek;

            var scheduleDayWeek = _repoMasterSchedule.Queryable.Where(i => i.IsWeekly &&
                                                                           !i.IsDeleted &&
                                                                           i.UserId == input.Id &&
                                                                           i.DayOfWeek == (int)dayOfWeek)
                .ToList();
            var ret = new GetDayScheduleOutput
            {
                ScheduleDayMode = EnumSetDaySchedule.AllWeeks,
                MyTimes = scheduleDayWeek.Select(i => new MasterScheduleDayOfWeeksInputOutput
                    {
                        Id = i.Id,
                        StartTime = i.StartTime,
                        EndTime = i.EndTime
                    })
                    .OrderBy(i => i.StartTime)
                    .ToList()
            };



            return ret;


        }


        public async Task<object> SetDayScheduleEveryWeek(SetDayScheduleInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            DateTime dateTime;
            #region cast to datetimeoffset
            try
            {
                dateTime = new DateTime(year: input.Year, month: input.Month, day: input.Day);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect vale of (year or month or day). Please check your date");
            }
            #endregion cast to datetimeoffset
            var dayOfWeek = dateTime.DayOfWeek;

            var allTime = new List<MasterScheduleDayOfWeeksInputOutput>();
            foreach (var time in input.MyTimes)
            {
                time.DayOfWeek = dayOfWeek;
                allTime.Add(time);
            }

            #region check dublicate schedule id
            var gr = allTime.Where(i => i.Id > 0).GroupBy(i => i.Id, i => i, (key, g) => new { key = key, count = g.Count() }).ToList();
            var dublicateId = gr.Where(i => i.count > 1).Select(i => i.key).ToList();
            if (dublicateId.Count > 0)
            {
                var str = $" [{string.Join(",", dublicateId.ToList())}]";
                throw new BaseException(CodeError.YouPassDubliacteScheduleId, CodeError.YouPassDubliacteScheduleId.GetMessage() + str);
            }
            #endregion check dublicate schedule id

            #region check valid time in my day
            this.Check(new GetWeeokScheduleList { MyTimes = input.MyTimes, DayOfWeek = dayOfWeek });
            #endregion check valid time in my day

            #region check valid data of master schedule id
            var schedulers = await _repoMasterSchedule.GetList(i => !i.IsDeleted && 
                                                                    i.UserId == session.UserId && 
                                                                    i.DayOfWeek == (long)dayOfWeek);
            var schedulersId = schedulers.Select(i => i.Id).ToList();
            var notExistId = allTime.Where(i => i.Id > 0 && !schedulersId.Contains(i.Id)).Select(i => i.Id).ToList();
            if (notExistId.Count > 0)
            {
                var str = $"[{string.Join(",", notExistId)}]";
                throw new BaseException(CodeError.BadRequeast, "Does not exist list of this master schedule id : " + str);
            }
            #endregion check valid data of master schedule id

            #region update and delete schedules
            foreach (var schedule in schedulers)
            {
                var current = allTime.FirstOrDefault(i => i.Id == schedule.Id);
                if (current == null)
                {
                    schedule.IsDeleted = true;
                    await _repoMasterSchedule.Update(schedule);
                }
                else
                {
                    //TODO Call Notify server for thow notification to user(он изменил время, а может к нему кто то был записан!!!)
                    schedule.StartTime = current.StartTime;
                    schedule.EndTime = current.EndTime;
                    schedule.DayOfWeek = (int)current.DayOfWeek;
                    schedule.IsWeekly = true;
                    await _repoMasterSchedule.Update(schedule);
                }
            }
            #endregion update and delete schedules

            #region create new schedule
            var allNew = allTime.Where(i => i.Id == 0).ToList();
            foreach (var newSchedule in allNew)
            {
                await _repoMasterSchedule.Insert(new MasterSchedules
                {
                    UserId = session.UserId,
                    StartTime = newSchedule.StartTime,
                    EndTime = newSchedule.EndTime,
                    IsWeekly = true,
                    DayOfWeek = (int)dayOfWeek,
                    IsDeleted = false,
                });
            }
            #endregion create new schedule

            return new object();
        }

        public async Task<object> SetDayScheduleCurrentDay(SetDayScheduleInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            DateTime dateTime;
            #region cast to datetimeoffset
            try
            {
                dateTime = new DateTime(input.Year, input.Month, input.Day);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect vale of (year or month or day). Please check your date");
            }
            #endregion cast to datetimeoffset
            var dayOfWeek = dateTime.DayOfWeek;

            var allTime = new List<MasterScheduleDayOfWeeksInputOutput>();
            foreach (var time in input.MyTimes)
            {
                time.DayOfWeek = dayOfWeek;
                allTime.Add(time);
            }

            #region check dublicate schedule id
            var gr = allTime.Where(i => i.Id > 0).GroupBy(i => i.Id, i => i, (key, g) => new { key = key, count = g.Count() }).ToList();
            var dublicateId = gr.Where(i => i.count > 1).Select(i => i.key).ToList();
            if (dublicateId.Count > 0)
            {
                var str = $" [{string.Join(",", dublicateId.ToList())}]";
                throw new BaseException(CodeError.YouPassDubliacteScheduleId, CodeError.YouPassDubliacteScheduleId.GetMessage() + str);
            }
            #endregion check dublicate schedule id

            #region check valid time in my day
            this.Check(new GetWeeokScheduleList { MyTimes = input.MyTimes, DayOfWeek = dayOfWeek });
            #endregion check valid time in my day

            #region check valid data of master schedule id
            var schedulers = await _repoMasterSchedule.GetList(i => !i.IsDeleted &&
                                                                    i.UserId == session.UserId 
                                                                    &&
                                                                    (i.DayOfWeek == (long)dayOfWeek || (i.Nyear == input.Year && i.Nmonth == input.Month && i.Nday == input.Day))
                                                                    );
            var schedulersId = schedulers.Select(i => i.Id).ToList();
            var notExistId = allTime.Where(i => i.Id > 0 && !schedulersId.Contains(i.Id)).Select(i => i.Id).ToList();
            if (notExistId.Count > 0)
            {
                var str = $"[{string.Join(",", notExistId)}]";
                throw new BaseException(CodeError.BadRequeast, "Does not exist list of this master schedule id : " + str);
            }
            #endregion check valid data of master schedule id


            var isNotWeekly = schedulers.Where(i => !i.IsWeekly).ToList();
            var listIdIsNitWeekly = isNotWeekly.Select(i => i.Id).ToList();
            
            #region update and delete schedules
            foreach (var schedule in isNotWeekly)
            {
                var current = allTime.FirstOrDefault(i => i.Id == schedule.Id);
                if (current == null)
                {
                    schedule.IsDeleted = true;
                    await _repoMasterSchedule.Update(schedule);
                }
                else
                {
                    //TODO Call Notify server for thow notification to user(он изменил время, а может к нему кто то был записан!!!)
                    schedule.StartTime = current.StartTime;
                    schedule.EndTime = current.EndTime;
                    schedule.DayOfWeek = (int)current.DayOfWeek;
                    schedule.IsWeekly = input.IsFreeDay;
                    schedule.IsFreeDay = false;
                    await _repoMasterSchedule.Update(schedule);
                }
            }
            #endregion update and delete schedules
            
            #region create new schedule
            var allNew = allTime.Where(i => !listIdIsNitWeekly.Contains(i.Id)).ToList();
            foreach (var newSchedule in allNew)
            {
                await _repoMasterSchedule.Insert(new MasterSchedules
                {
                    UserId = session.UserId,
                    StartTime = newSchedule.StartTime,
                    EndTime = newSchedule.EndTime,
                    IsWeekly = false,
                    IsFreeDay = input.IsFreeDay,
                    Nyear = input.Year,
                    Nmonth = input.Month,
                    Nday = input.Day,
                    IsDeleted = false,
                });
            }
            #endregion create new schedule
            return new object();
        }

        public async Task<object> SetDayScheduleFreeDay(SetDayScheduleInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            input.IsFreeDay = true;
            await this.SetDayScheduleCurrentDay(input);

            var first = _repoMasterSchedule.Queryable.FirstOrDefault(
                                                     i => i.IsFreeDay &&
                                                     i.UserId == session.UserId &&
                                                     !i.IsDeleted &&
                                                     i.Nyear == input.Year && 
                                                     i.Nmonth == input.Month &&
                                                     i.Nday == input.Day
                                                     );

            if (first == null)
            {
                await _repoMasterSchedule.Insert(new MasterSchedules
                {
                    IsWeekly = false,
                    Nyear = input.Year,
                    Nmonth = input.Month,
                    Nday = input.Day,
                    IsFreeDay = true,
                    IsDeleted = false,
                    UserId = session.UserId,
                    StartTime = new TimeSpan(0,1,3,3,3),
                    EndTime = new TimeSpan(0, 11, 3, 3, 3),
                });
            }
            return new object();

        }

        

        

        











        private void Check(GetWeeokScheduleList check)
        {
            TimeSpan? beforTime = null;

            var error = @"Вы указали список дат, которые не являются логичными для системы. Пожалуйста проверьте корректность дат в Вашем запросе!
                            Дата старта должна быть менше даты конца и больше чем предыдущая дата конца в этом дне, если она существует.";
            foreach (var scheduler in check.MyTimes)
            {
                if (beforTime != null)
                {
                    if(scheduler.StartTime < beforTime.Value)
                        throw new BaseException(CodeError.BadRequeast, "Incorrect time in this object : " + JsonConvert.SerializeObject(check), 
                            error);
                }
                beforTime = scheduler.StartTime;

                if (scheduler.EndTime < beforTime.Value)
                {
                     throw new BaseException(CodeError.BadRequeast, "Incorrect time in this object : " + JsonConvert.SerializeObject(check)
                         ,error );
                }
                beforTime = scheduler.EndTime;
            }
        }


        //private async Task<long> GetWeakOfDayId(long userId, DayOfWeek input)
        //{
        //    var exist = await _repoDayWeek
        //            .FirstOrDefault(i => !i.IsDeleted
        //                            && i.UserId == userId
        //                            && i.DayOfWeek == (int)input);
        //    if (exist == null)
        //    {
        //        _repoDayWeek.Insert(new MasterDayOfWeeks
        //        {
        //            DayOfWeek = input=> input.
        //        })
        //    }
        //}



    }



    public class GetDayScheduleInput : BaseInput
    {
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Day { get; set; }
    }

    public class GetDayScheduleForMasterInput : BaseInputById
    {
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Day { get; set; }
    }

    public class GetDayScheduleOutput
    {
        public EnumSetDaySchedule ScheduleDayMode { get; set; }
        public List<MasterScheduleDayOfWeeksInputOutput> MyTimes { get; set; }
    }

    public class SetDayScheduleInput : BaseInput
    {
        [Required]
        public int Year { get; set; }
        [Required]
        public int Month { get; set; }
        [Required]
        public int Day { get; set; }
        [Required]
        public List<MasterScheduleDayOfWeeksInputOutput> MyTimes { get; set; }

        [JsonIgnore]
        public bool IsFreeDay { get; set; }

    }

    public enum EnumSetDaySchedule
    {
        OnlyThisDay,
        FreeDay,
        AllWeeks
    }

    public class GetWeekScheduleOutput
    {
        public List<GetWeeokScheduleList> MyDays { get; set; }
    }

    public class GetWeekScheduleInput : BaseInput
    {
        public List<GetWeeokScheduleList> MyDays { get; set; }
    }

    public class GetWeeokScheduleList
    {
        public DayOfWeek DayOfWeek { get; set; }

        public List<MasterScheduleDayOfWeeksInputOutput> MyTimes { get; set; }
    }

    public class MasterScheduleDayOfWeeksInputOutput
    {
        [Required]
        public long Id { get; set; }
        [JsonIgnore]
        public DayOfWeek DayOfWeek { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
    }
}
