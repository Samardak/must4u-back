﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GameBLL.Service.InternalService;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;

using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Application.Common.Game.Enum;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Application.Common.BaseInputOutput;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using PlatformCore.Application.Common.Extention;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    public interface IPostService
    {
        Task<PostsDTO> CreatePost(CreatePostInput input);
        Task<List<PostsDTO>> GetPostsByUserId(GetPostsByUserId input);
        Task<PostsDTO> SetImpression(SetImpressionInp input);
        Task<GetCommentOut> CreatePostComment(CreatePostCommentInp input);
        Task<PostsDTO> TagUser(TagUserInput input);
        Task<List<GetPostOut>> GetPosts(GetPostInp input);
        Task<List<GetCommentOut>> GetComments(GetCommentInp input);
    }

    public class PostService : IPostService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Posts, PostsDTO> _repoPosts;
        private readonly ISkipCountCheckService _skipCountCheckService;
        private readonly IRepository<PostLikes, PostLikesDTO> _repoPostLikes;
        private readonly IRepository<PostComents, PostComentsDTO> _repoPostComments;
        private readonly IRepository<PostTags, PostTagsDTO> _repoPostTag;
        private readonly IRepository<Users> _repoUsers;
        private readonly IRepository<UserFiends> _repoFriends;
        private readonly IFriendService _friendService;
        private readonly IDateConvertorServiceInternal _convertorServiceInternal;

        public PostService(IRepository<Posts, PostsDTO> repoPosts, 
            ISessionService sessionService, 
            ISkipCountCheckService skipCountCheckService, 
            IRepository<PostLikes, PostLikesDTO> repoPostLikes, 
            IRepository<PostComents, PostComentsDTO> repoPostComments, 
            IRepository<PostTags, PostTagsDTO> repoPostTag, 
            IRepository<Users> repoUsers, IRepository<UserFiends> repoFriends, 
            IFriendService friendService, IDateConvertorServiceInternal convertorServiceInternal)
        {
            _repoPosts = repoPosts;
            _sessionService = sessionService;
            _skipCountCheckService = skipCountCheckService;
            _repoPostLikes = repoPostLikes;
            _repoPostComments = repoPostComments;
            _repoPostTag = repoPostTag;
            _repoUsers = repoUsers;
            _repoFriends = repoFriends;
            _friendService = friendService;
            _convertorServiceInternal = convertorServiceInternal;
        }

        public async Task<PostsDTO> CreatePost(CreatePostInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input.Token);

            var post = await _repoPosts.Insert(new Posts
            {
                PictureUrl = input.PictureUrl,
                DateCreation = DateTimeOffset.Now,
                PostTypeId = input.IsAd ? (int)EnumPostTypes.AddPost: (int)EnumPostTypes.SimplePost,
                UserId = session.UserId,
            });

            return Mapper.Map<PostsDTO>(post);
        }
        public async Task<List<PostsDTO>> GetPostsByUserId(GetPostsByUserId input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);

            var posts = _repoPosts.Queryable.Where(i => !i.IsDeleted && i.UserId == input.UserId)
                .OrderByDescending(i=> i.DateCreation)
                .Skip(input.Skip)
                .Take(input.Count)
                .ToList();

            var ret = posts.Select(Mapper.Map<PostsDTO>).ToList();
            return ret;
        }
        public async Task<PostsDTO> SetImpression(SetImpressionInp input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var post = await _repoPosts.FirstOrDefault(i => !i.IsDeleted && i.Id == input.PostId);
            if(post == null)
                throw new BaseException(CodeError.BadRequeast, nameof(CodeError.BadRequeast), $"Does not exsit post with id {input.PostId}");

            var postlike = await _repoPostLikes.FirstOrDefault(i => i.PostId == input.PostId && i.UserId == session.UserId);
            if (postlike == null)
            {
                await _repoPostLikes.Insert(new PostLikes
                {
                    PostId = post.Id,
                    DateCreation = DateTimeOffset.Now,
                    IsLike = input.IsLike,
                    UserId = session.UserId
                });
                input.IsLike.IfDo(() => post.CountLikes++, () => post.CountDislike++);
                await _repoPosts.Update(post);
            }
            else
            {

                if (input.IsLike != postlike.IsLike)
                {
                    input.IsLike.IfDo(
                        () => { post.CountDislike--; post.CountLikes++; },  
                        () => { post.CountDislike++; post.CountLikes--; });

                    await _repoPosts.Update(post);

                    postlike.IsLike = input.IsLike;
                    await _repoPostLikes.Update(postlike);
                }
            }
            return post.Then(Mapper.Map<PostsDTO>);
        }
        public async Task<GetCommentOut> CreatePostComment(CreatePostCommentInp input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var post = await _repoPosts.FirstOrDefault(i => i.Id == input.PostId);

            if (post == null)
                throw new BaseException(CodeError.BadRequeast, nameof(CodeError.BadRequeast), $"Does not exist post with id {input.PostId}");
            post.CountComments++;
            await _repoPosts.Update(post);

            var postComments = await _repoPostComments.Insert(new PostComentsDTO
            {
                PostId = input.PostId,
                CreatorId = session.UserId,
                Text = input.PostText,
                DateCreation = DateTimeOffset.Now
            });
            return new GetCommentOut
            {
                UserId = session.UserId,
                UserPicture = session.PictureUrl,
                UserFullName = session.FirstName + " " + session.LastName,
                CommentId = postComments.Id,
                DateComment = _convertorServiceInternal.ConvertToMs(postComments.DateCreation),
                TextComment = postComments.Text,
                PostId = postComments.PostId
            };
        }

        public async Task<List<GetCommentOut>> GetComments(GetCommentInp input)
        {
            var session = _sessionService.GetSessionDataAsync(input?.Token);
            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);
            var q = from user in _repoUsers.Queryable
                join comment in _repoPostComments.Queryable on user.Id equals comment.CreatorId into comments
                from com in comments.DefaultIfEmpty()
                where com.PostId == input.PostId
                orderby com.Id descending 
                select new
                {
                    CreatorId = com.CreatorId,
                    Id = com.Id,
                    DateCreation = com.DateCreation,
                    Text = com.Text,
                    UserPicture = user.PictureUrl,
                    UserFullName = user.FirstName + " " + user.LastName,
                    PostId = com.PostId
                };
            q = q.Skip(input.Skip).Take(input.Count);



            var result = q.ToList()
                .Select(i => new GetCommentOut
                {
                    UserId = i.CreatorId,
                    CommentId = i.Id,
                    DateComment = _convertorServiceInternal.ConvertToMs(i.DateCreation),
                    TextComment = i.Text,
                    UserFullName = i.UserFullName,
                    UserPicture = i.UserPicture,
                    PostId = i.PostId
                }).ToList();

            return result;
        }

        public async Task<PostsDTO> TagUser(TagUserInput input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input?.Token);

            var post = await _repoPosts.FirstOrDefaultDto(i => !i.IsDeleted && i.UserId == session.UserId);
            if(post == null)
                throw new BaseException(CodeError.BadRequeast, nameof(CodeError.BadRequeast), 
                    $"You does not have post with id {input.PostId}");

            var user = await _repoUsers.FirstOrDefault(i => !i.IsDeleted && i.Id == input.UserId);
            if (user == null)
                throw new BaseException(CodeError.DoesNotExistUser, nameof(CodeError.DoesNotExistUser),
                    CodeError.DoesNotExistUser.GetMessage());

            var postTag = await _repoPostTag.FirstOrDefault(i => !i.IsDeleted && i.UserId == input.UserId &&
                                                   i.PostId == input.PostId);

            if (postTag == null)
            {
                await _repoPostTag.Insert(new PostTags
                {
                    UserId = input.UserId,
                    DateCreation = DateTimeOffset.Now,
                    IsDeleted = false,
                    PostId = input.PostId,
                });
            }
            return post;
        }

        public async Task<List<GetPostOut>> GetPosts(GetPostInp input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            var postTypeId = input.IsAd ? (int) EnumPostTypes.AddPost : (int) EnumPostTypes.SimplePost;
            var idMyFriends = _repoFriends.Queryable
                .Where(i => i.User1Id == session.UserId || i.User2Id == session.UserId)
                .Select(i => i.User1Id == session.UserId ? i.User2Id : i.User1Id).Distinct();
            

            var q = from post in _repoPosts.Queryable
                join like in _repoPostLikes.Queryable on post.Id equals like.PostId into likes
                join tag in _repoPostTag.Queryable on post.Id equals tag.Id into tags
                join comment in _repoPostComments.Queryable on post.Id equals comment.Id into comments
                join user in _repoUsers.Queryable on post.UserId equals user.Id
                join friend in idMyFriends on post.UserId equals friend into FFF
                from ff in FFF.DefaultIfEmpty()
                orderby post.Id descending 
                where ff > 0 && post.PostTypeId == postTypeId
                    select new
                    {
                        PostId = post.Id,
                        Date = post.DateCreation,
                        CreatorId = user.Id,
                        UserPicture = user.PictureUrl,
                        PostPicture = post.PictureUrl,
                        CountComments = comments.Count(),
                        CountLike =likes.Count(i=> i.IsLike),
                        CountDislike = likes.Count(i => !i.IsLike)
                    };
            var resultAnonymus = q.Skip(input.Skip).Take(input.Count);
            var result = resultAnonymus.ToList().Select(i=> new GetPostOut
            {
                PostId = i.PostId,
                Date = _convertorServiceInternal.ConvertToMs(i.Date),
                CreatorId = i.CreatorId,
                UserPicture = i.UserPicture,
                PostPicture = i.PostPicture,
                CountComments = i.CountComments,
                CountLike = i.CountLike,
                CountDislike = i.CountDislike,
            }).ToList();
            return result;



        }

    }

    public class GetCommentInp : BaseInput
    {
        public long PostId { get; set; }
        public int Skip { get; set; }
        public int Count { get; set; }
    }

    public class GetCommentOut
    {
        public long UserId { get; set; }
        public string UserFullName { get; set; }
        public long CommentId { get; set; }
        public string UserPicture { get; set; }
        public long DateComment { get; set; }
        public string TextComment { get; set; }
        public long PostId { get; set; }
    }

    public class TagUserInput : BaseInput
    {
        [Required]
        public long PostId { get; set; }
        [Required]
        public long UserId { get; set; }
    }

    public class SetImpressionInp : BaseInput
    {
        [Required]
        public long PostId { get; set; }
        [Required]
        public bool IsLike { get; set; }
    }


    public class GetPostsByUserId : BaseSkipCountInput
    {
        [Required]
        public long UserId { get; set; }
    }
    public class CreatePostInput : BaseInput
    {
        [Required]
        public string PictureUrl { get; set; }
        [Required]
        public bool IsAd { get; set; }
    }



    public class CreatePostCommentInp : BaseInput
    {
        [Required]
        public long PostId { get; set; }
        [Required]
        public string PostText { get; set; }
    }

    public class GetPostInp : BaseInput
    {
        public int Skip { get; set; }
        public int Count { get; set; }
        [Required]
        public bool IsAd { get; set; }
    }

    public class GetPostOut
    {
        public long PostId { get; set; }
        public long Date  { get; set; }
        public long CreatorId { get; set; }
        public string UserPicture { get; set; }
        public string PostPicture { get; set; }
        public int CountComments { get; set; }
        public int CountLike { get; set; }
        public int CountDislike { get; set; }
    }
}
