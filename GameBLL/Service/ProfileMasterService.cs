﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{

    public interface IProfileMasterService
    {
        Task<ProfilePlace> GetProfilePlace(BaseInput input);

        

        Task<object> SetProfilePlace(SetProfilePlace input);


        Task<GetProfileServices> GetProfileServices(BaseInput input);
        Task<object> SetProfileServices(SetProfileService input);


        Task<UserSocialNetworksDTO> GetSocaialLink(BaseInput input);
        Task<object> SetSocialLink(SocialNetworkInput input);


        Task<GetAboutText> GetAboutText(BaseInput input);
        Task<object> SetAboutText(SetAboutText input);
    }

    public class ProfileMasterService : IProfileMasterService
    {

        private readonly ISessionService _sessionService;
        private readonly IRepository<Users> _repoUser;
        private readonly IRepository<Countries, CountriesDTO> _repoCountryDto;
        private readonly IRepository<Cities, CitiesDTO> _repoCityDto;
        private readonly IRepository<StationRegions, StationRegionsDTO> _repoStationDto;
        private readonly IRepository<MasterServices, MasterServicesDTO> _repoMasterDto;
        private readonly IRepository<ServicesType, ServicesTypeDTO> _repoServicesTypeDto;
        private readonly IRepository<UserSocialNetworks, UserSocialNetworksDTO> _repoSocNetDto;

        public ProfileMasterService(ISessionService sessionService, IRepository<Countries, CountriesDTO> repoCountryDto, 
            IRepository<Users> repoUser, IRepository<Cities, CitiesDTO> repoCityDto, IRepository<StationRegions, StationRegionsDTO> repoStationDto, 
            IRepository<MasterServices, MasterServicesDTO> repoMasterDto, IRepository<ServicesType, ServicesTypeDTO> repoServicesTypeDto, 
            IRepository<UserSocialNetworks, UserSocialNetworksDTO> repoSocNetDto)
        {
            _sessionService = sessionService;
            _repoCountryDto = repoCountryDto;
            _repoUser = repoUser;
            _repoCityDto = repoCityDto;
            _repoStationDto = repoStationDto;
            _repoMasterDto = repoMasterDto;
            _repoServicesTypeDto = repoServicesTypeDto;
            _repoSocNetDto = repoSocNetDto;
        }

        public async Task<ProfilePlace> GetProfilePlace(BaseInput input)
        {

            var ret = new ProfilePlace();
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            if(session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());


            var user = await _repoUser.FirstOrDefault(i => !i.IsDeleted && i.Id == session.UserId);

            ret.Countries = await _repoCountryDto.GetListDto();
            ret.Country = ret.Countries.FirstOrDefault(i => i.Id == user.CountryId);

            ret.City = await _repoCityDto.FirstOrDefaultDto(i => i.Id == user.CityId);


            ret.Stations = await _repoStationDto.GetListDto();
            ret.Station = ret.Stations.FirstOrDefault(i => i.Id == user.StationRegionId);

            ret.IsOnlyHome = user.IsOnlyHome;
            ret.WorkingExperience = user.WorkingExperience;
            return ret;
        }

        

        public async Task<object> SetProfilePlace(SetProfilePlace input)
        {
            var session = await _sessionService.GetSessionMasterDataAsync(input?.Token);

            var user = await _repoUser.FirstOrDefault(i => i.Id == session.UserId);

            var city = await _repoCityDto.FirstOrDefault(i => i.Id == input.CityId);


            if (city == null)
                throw new BaseException(CodeError.InvalidCityId, CodeError.InvalidCityId.GetMessage() + $" : {input.CityId}");

            user.CountryId = city.CountryId;
            user.CityId = city.Id;

            if (input.StationId.HasValue)
            {
                if (input.StationId != 0)
                {
                    var station = await _repoStationDto.FirstOrDefault(i => i.Id == input.StationId.Value);
                    if (station == null)
                        throw new BaseException(CodeError.InvalidStationId, CodeError.InvalidStationId.GetMessage() + $" : {input.StationId}");
                }
                user.StationRegionId = input.StationId.Value;

            }
            user.StationRegionId = input.StationId;
            user.IsOnlyHome = input.IsOnlyHome;
            user.WorkingExperience = input.WorkingExperience;

            await _repoUser.Update(user);
            
            return new object();
        }




        public async Task<GetProfileServices> GetProfileServices(BaseInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());
            GetProfileServices resp = new GetProfileServices();

            var serviceTypes = await _repoServicesTypeDto.GetListDto();
            var myServices =  await _repoMasterDto.GetListDto(i => !i.IsDeleted && i.UserId == session.UserId);
            myServices = myServices.OrderBy(i => i.DateCreation).ToList();

            resp.MyServices = myServices.Select(i => new MasterServicesOut
            {
                Id = i.Id,
                ServicesTypeId = i.ServicesTypeId,
                UserId = i.UserId,
                DuringTimeMinutes = i.DuringTimeMinutes,
                Price = i.Price,
                ServicesTypeName = serviceTypes.FirstOrDefault(i2 => i2.Id == i.ServicesTypeId)?.Name ?? "",
            }).ToList();

            resp.ServicesAllowedForMe = serviceTypes;
            return resp;
        }

        public async Task<object> SetProfileServices(SetProfileService input)
        {
            var session = _sessionService.GetSessionMasterData(input?.Token);
            //if (session.UserType != EnumUserTypes.Master)
            //    throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());
            var myServiceInDb = await _repoMasterDto.GetList(i => i.UserId == session.UserId && !i.IsDeleted);
            var listMyServicesId = myServiceInDb.Select(i => i.Id);

           
            var serviceTypesId = (await _repoServicesTypeDto.GetList()).Select(i=> i.Id).ToList();
            var listInputServiceType = input.MyServices.Select(i => i.ServicesTypeId);

            #region check service type 
            var incorrectServiceTypes = listInputServiceType.Where(i => !serviceTypesId.Contains(i)).ToList();
            if (incorrectServiceTypes.Count > 0)
            {
                #region check invalid services types id

                var str = $" [{string.Join(",", incorrectServiceTypes)}]";
                throw new BaseException(CodeError.InvalidServiceTypeId,
                    CodeError.InvalidServiceTypeId.GetMessage(),
                    CodeError.InvalidServiceTypeId.GetMessage() + str);

                #endregion check invalid master services types id
            }
            #endregion check service type 



            #region check master services
            var currServices = input.MyServices.Where(i => i.Id != 0).ToList();
            if (currServices.Count > 0)
            {
                #region check invalid master services id

                var incorrectServices =
                    currServices.Where(i => !listMyServicesId.Contains(i.Id)).Select(i => i.Id).ToList();
                if (incorrectServices.Count > 0)
                {
                    var str = $" [{string.Join(",", incorrectServices)}]";
                    throw new BaseException(CodeError.InvalidMasterServiceId,
                        CodeError.InvalidMasterServiceId.GetMessage(),
                        CodeError.InvalidMasterServiceId.GetMessage() + str);
                }
                #endregion check invalid master services id
            }
            #endregion check master services

            #region check dublicate

            //dublicate service type ID
            var count = input.MyServices.Select(i => new
            {
                typeId = i.ServicesTypeId, count = input.MyServices.Count(d=> d.ServicesTypeId == i.ServicesTypeId)
            }).Where(i=> i.count > 1).Select(i=> i.typeId).Distinct().ToList();

            if (count.Count > 0)
            {
                var str = $" [{string.Join(",", count)}]";
                throw new BaseException(CodeError.You_Pass_Dublicate_MasterService_WithSrviceTypeId, CodeError.You_Pass_Dublicate_MasterService_WithSrviceTypeId.GetMessage() + str);
            }


            //dublicate master service id
            var q = from x in input.MyServices
                where x.Id > 0
                group x by x.Id
                into g
                where g.Count() > 1
                select new {id = g.Key};
            if(q.ToList().Count > 0)
            {
                var str = $" [{string.Join(",", q.ToList())}]";
                throw new BaseException(CodeError.You_Pass_Dublicate_MasterService_With_SameId, CodeError.You_Pass_Dublicate_MasterService_With_SameId.GetMessage() + str);
            }



            #endregion check dublicate


                      //1. remove in db does not exist in input and
                      //2. update exist in db which exist in input
                      //3. create in db new masterService

                      #region remove and update 
            foreach (var masterService in myServiceInDb)
            {
                var myInput = input.MyServices.FirstOrDefault(i => i.Id == masterService.Id);
                if (myInput == null)
                    masterService.IsDeleted = true;

                else
                {
                    masterService.ServicesTypeId = myInput.ServicesTypeId;
                    masterService.DuringTimeMinutes = myInput.DuringTimeMinutes;
                    masterService.Price = myInput.Price;
                }
                await _repoMasterDto.Update(masterService);
            }
            #endregion remove and update

            #region create 
            var newMyService = input.MyServices.Where(i => i.Id == 0).ToList();
            foreach (var newService in newMyService)
            {
                var alreadyExist =
                    await _repoMasterDto.FirstOrDefault(
                        i => !i.IsDeleted && i.UserId == session.UserId && i.ServicesTypeId == newService.ServicesTypeId);

                //if (alreadyExist != null)
                //    throw new BaseException(CodeError.Already_Exist_MasterService_With_ServiceType_Id, 
                //        CodeError.Already_Exist_MasterService_With_ServiceType_Id.GetMessage() + newService.ServicesTypeId);

                var item = new MasterServices
                {
                    DateCreation = DateTimeOffset.Now,
                    DuringTimeMinutes = newService.DuringTimeMinutes,
                    IsDeleted = false,
                    Price = newService.Price,
                    ServicesTypeId = newService.ServicesTypeId,
                    UserId = session.UserId,
                };
                await _repoMasterDto.Insert(item);
            }
            #endregion create 

            return new object();

        }




        public async Task<UserSocialNetworksDTO> GetSocaialLink(BaseInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());

            var socNet = await _repoSocNetDto.FirstOrDefaultDto(i => i.UserId == session.UserId);
            if(socNet == null)
                socNet = new UserSocialNetworksDTO
                {
                    UserId = session.UserId,
                };

            return socNet;
        }

        public async Task<object> SetSocialLink(SocialNetworkInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());

            var socNet = await _repoSocNetDto.FirstOrDefault(i => i.UserId == session.UserId);
            if (socNet == null)
            {
                socNet = new UserSocialNetworks
                {
                    UserId = session.UserId,
                    Facebook = input.Facebook,
                    Instagram = input.Instagram,
                    Vk = input.Vk,
                    Youtube = input.Youtube
                };
                await _repoSocNetDto.Insert(socNet);
            }
            else
            {
                socNet.Facebook = input.Facebook;
                socNet.Instagram = input.Instagram;
                socNet.Vk = input.Vk;
                socNet.Youtube = input.Youtube;
                await _repoSocNetDto.Update(socNet);
            }
            return new object();
        }



        public async Task<GetAboutText> GetAboutText(BaseInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());

            var user = await _repoUser.FirstOrDefault(i => i.Id == session.UserId);
            return new GetAboutText
            {
                AboutText = user?.AboutText
            };
        }

        public async Task<object> SetAboutText(SetAboutText input)
        {
            var session = await _sessionService.GetSessionDataAsync(input.Token);
            if (session.UserType != EnumUserTypes.Master)
                throw new BaseException(CodeError.YouShoulLoginAsMaster, CodeError.YouShoulLoginAsMaster.GetMessage());


            if ((input?.AboutText?.Length ?? 0) > 500)
                throw new BaseException(CodeError.YouTypedTooMuchTextMaxLengthIs, CodeError.YouTypedTooMuchTextMaxLengthIs.GetMessage() + "500");

            var user = await _repoUser.FirstOrDefault(i => i.Id == session.UserId);
            user.AboutText = input?.AboutText;
            await _repoUser.Update(user);
            return new object();
        }
    }

   

    public class ProfilePlace
    {
        public CountriesDTO Country { get; set; }

        public List<CountriesDTO> Countries { get; set; }

        public CitiesDTO City { get; set; }

        public StationRegionsDTO Station { get; set; }

        public List<StationRegionsDTO> Stations { get; set; }

        public int WorkingExperience { get; set; }

        public bool IsOnlyHome { get; set; }
    }

    public class SetProfilePlace : BaseInput
    {
        [Required]
        public long CityId { get; set; }
        [Required]
        public long CountryId { get; set; }
        public long? StationId { get; set; }
        [Required]
        public int WorkingExperience { get; set; }
        [Required]
        public bool IsOnlyHome { get; set; }
    }

    










    public class GetProfileServices
    {
        public List<MasterServicesOut> MyServices { get; set; }

        public List<ServicesTypeDTO> ServicesAllowedForMe { get; set; }
    }

    public class SetProfileService : BaseInput
    {
        public List<MasterServicesOut> MyServices { get; set; }

    }

    public class MasterServicesOut
    {
        [JsonProperty("masterServiceId")]
        [Required]
        public long Id { get; set; }
        [Required]
        public int ServicesTypeId { get; set; }
        public string ServicesTypeName { get; set; }
        [JsonIgnore]
        public long UserId { get; set; }
        [Required]
        public int DuringTimeMinutes { get; set; }
        [Required]
        public int Price { get; set; }
    }





    public class SocialNetworkInput : BaseInput
    {
        public string Vk { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
    }



    public class GetAboutText
    {
        public string AboutText { get; set; }
    }
    public class SetAboutText : BaseInput
    {
        public string AboutText { get; set; }
    }



}















