﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;

namespace GameBLL.Service.CacheServices
{
    public class ServiceTypeCacheServices
    {
        private readonly IRepository<ServicesType> _repoServiceType;
        private static List<ServicesType> _servicesTypes;

        public ServiceTypeCacheServices(IRepository<ServicesType> repoServiceType)
        {
            _repoServiceType = repoServiceType;
            if (_servicesTypes == null)
            {
                _servicesTypes = _repoServiceType.Queryable.ToList();
            }
        }

        public List<ServicesType> GetServiceTypeList()
        {
            return _servicesTypes;
        }
    }
}
