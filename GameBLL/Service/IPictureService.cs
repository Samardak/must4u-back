﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CommonService.SessionService;

namespace GameBLL.Service
{
    public interface IPictureService
    {
        Task<Pictures> FindPicturePyId(BaseInputById input);
        Task<PicturesDTO> SavePicture(BaseInputAndString input);
    }

    public class PictureService : IPictureService
    {
        private readonly IRepository<Pictures,PicturesDTO> _repoPicture;
        private readonly ISessionService _sessionService;

        public PictureService( 
            ISessionService sessionService, IRepository<Pictures, PicturesDTO> repoPicture)
        {
            _sessionService = sessionService;
            _repoPicture = repoPicture;
        }

        public async Task<PicturesDTO> SavePicture(BaseInputAndString input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var insertvalue = await _repoPicture.Insert(new Pictures
            {
                FullUrl = input.UrlPicture,
                Url = input.UrlPicture,
            });

            return AutoMapper.Mapper.Map<PicturesDTO>(insertvalue);
        }


        public async Task<Pictures> FindPicturePyId(BaseInputById input)
        {
            var session = _sessionService.GetSessionDataAsync(input?.Token);

            return await _repoPicture.FirstOrDefault(i => i.Id == input.Id);
        }
    }
}
