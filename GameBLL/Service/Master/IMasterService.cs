﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameBLL.Service.CacheServices;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service.Master
{
    public interface IMasterService
    {
        Task<MasterProfileOut> Profile(BaseInputById input);
    }

    public class MasterService : IMasterService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Users> _repoUsers;
        private readonly IRepository<ServicesType> _repoServicesType;
        private readonly IRepository<MasterServices> _reposMasterServices;
        private readonly ServiceTypeCacheServices _serviceTypeCacheServices;

        public MasterService(ISessionService sessionService, 
            IRepository<Users> repoUsers, 
            IRepository<ServicesType> repoServicesType, 
            IRepository<MasterServices> reposMasterServices, 
            ServiceTypeCacheServices serviceTypeCacheServices)
        {
            _sessionService = sessionService;
            _repoUsers = repoUsers;
            _repoServicesType = repoServicesType;
            _reposMasterServices = reposMasterServices;
            _serviceTypeCacheServices = serviceTypeCacheServices;
        }

        public async Task<MasterProfileOut> Profile(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var user = _repoUsers.Queryable
                .Include(i=> i.MasterServices)
                .Include(i=> i.Country)
                .Include(i=> i.City)
                .Include(i=> i.StationRegion)
                .Include(i=> i.UserSocialNetworks)
                .FirstOrDefault(i => !i.IsDeleted && i.Id == input.Id && i.UserTypeId == (int)EnumUserTypes.Master);

            if(user == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            var location = $"{user.Country.Country}, {user.City.City}";
            if (user.StationRegion != null)
                location = $"{location}, {user.StationRegion.StationName}";
            ;


            var myServices = _reposMasterServices.Queryable.Where(i => !i.IsDeleted && i.UserId == session.UserId);
            myServices = myServices.OrderBy(i => i.DateCreation);

            var types = _serviceTypeCacheServices.GetServiceTypeList();

            var services = myServices.ToList().Select(i => new MasterServicesOut
            {
                Id = i.Id,
                ServicesTypeId = i.ServicesTypeId,
                UserId = i.UserId,
                DuringTimeMinutes = i.DuringTimeMinutes,
                Price = i.Price,
                ServicesTypeName = types.FirstOrDefault(i2 => i2.Id == i.ServicesTypeId)?.Name ?? "",
            }).ToList();

            var links = user.UserSocialNetworks.FirstOrDefault();

            return new MasterProfileOut
            {
                UserId = user.Id,
                Picture = user.PictureUrl,
                FullName = user.FirstName + " " + user.LastName,
                OnlineText = "Online",

                IsOnlyHome = user.IsOnlyHome,
                WorkingExperience = user.WorkingExperience,

                PlaceInTop = 453,
                ViewToday = 15,
                ViewMonth = 45,

                Location = location,
                AboutText = user.AboutText,

                ListServices = services,

                Vk = links?.Vk,
                Facebook = links?.Facebook,
                Instagram = links?.Instagram,
                Youtube = links?.Youtube,


            };
        }

        
    }

    public class MasterProfileOut
    {
        public long UserId { get; set; }
        public string Picture { get; set; }
        public string FullName { get; set; }
        public string OnlineText { get; set; }
        public bool IsOnline { get; set; }

        public long ViewToday { get; set; }
        public long ViewMonth { get; set; }
        public long PlaceInTop { get; set; }
        public string Location { get; set; }
        public string AboutText { get; set; }

        public List<MasterServicesOut> ListServices { get; set; }

        public string Vk { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }
        public int WorkingExperience { get; set; }
        public bool IsOnlyHome { get; set; }
    }

    
}
