﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameBLL.Service.InternalService;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.AspNetCore.Mvc.DataAnnotations.Internal;
using Microsoft.EntityFrameworkCore;
using PlatformCore.Application.Common.BaseInputOutput;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.PlatformException;

namespace GameBLL.Service
{
    public interface IFriendService
    {
        Task<BaseOutputCount<List<GetFriendOutput>>> GetFriends(GetFriensInp input);
        Task<BaseOutputCount<List<GetFriendOutput>>> GetInvites(BaseSkipCountInput input);

        Task<object> InviteFriend(BaseInputById input);
        Task<object> AcceptInvite(BaseInputById input);
        Task<object> CancelInvite(BaseInputById input);
        Task<object> RemoveFriend(BaseInputById input);

        Task<GetFriendsWithClientCountAndMasterCountOut> GetFriendsWithCount(
            GetFriendsWithClientCountAndMasterCountInp input);

        Task<GetStatusUserAsMyFriendOut> GetStatusUserAsMyFriend(BaseInputById input);
    }
    public class FriendService : IFriendService
    {
        private readonly IRepository<UserFiends, UserFiendsDTO> _repoUserFriend;
        private readonly IRepository<Users> _reposUsers;
        private readonly ISessionService _sessionService;
        private readonly ISkipCountCheckService _skipCountCheckService;

        public FriendService(IRepository<UserFiends, UserFiendsDTO> repoUserFriend, 
            IRepository<Users> reposUsers, ISessionService sessionService, 
            ISkipCountCheckService skipCountCheckService)
        {
            _repoUserFriend = repoUserFriend;
            _reposUsers = reposUsers;
            _sessionService = sessionService;
            _skipCountCheckService = skipCountCheckService;
        }


        public async Task<BaseOutputCount<List<GetFriendOutput>>> GetFriends(GetFriensInp input)
        {
            List<GetFriendOutput> ret = new List<GetFriendOutput>();
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var userId = session.UserId;
            if (input.Id > 0)
            {
                userId = input.Id;
                var user = _reposUsers.Queryable.FirstOrDefault(i => !i.IsDeleted && i.Id == userId);
                if(user == null)
                    throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());
            }
                

            var query = _repoUserFriend.Queryable
                .Include(i => i.User1)
                .Include(i => i.User2)
                .Where(i => !i.IdDeleted && 
                i.IsConfirm &&
                (i.User1Id == userId || i.User2Id == userId)).ToList();
            
            var friends = query.Select(i => new
            {
                myFriend =
                i.User1Id == userId
                    ? i.User2 ?? _reposUsers.FirstOrDefault(d => d.Id == i.User2Id).Result
                    : i.User1 ?? _reposUsers.FirstOrDefault(d => d.Id == i.User1Id).Result,
            });
            if(!string.IsNullOrEmpty(input.Contains))
                friends = friends.Where(i => 
                                    i.myFriend.FirstName.StartsWith(input.Contains,StringComparison.OrdinalIgnoreCase) ||
                                    i.myFriend.LastName.StartsWith(input.Contains, StringComparison.OrdinalIgnoreCase) ||
                                    (i.myFriend.FirstName + " " + i.myFriend.LastName).StartsWith(input.Contains, StringComparison.OrdinalIgnoreCase) ||
                                    (i.myFriend.LastName + " " + i.myFriend.FirstName).StartsWith(input.Contains, StringComparison.OrdinalIgnoreCase)
                                    );

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);
            var allCount = friends.Count();
            friends = friends.Skip(input.Skip).Take(input.Count);
            foreach (var friend in friends)
            {
                ret.Add(new GetFriendOutput
                {
                    UserId = friend.myFriend.Id,
                    FirstName = friend.myFriend.FirstName,
                    LastName = friend.myFriend.LastName,
                    Picture = friend.myFriend.PictureUrl,
                    UserTypeId = friend.myFriend.UserTypeId
                });
            }
            return new BaseOutputCount<List<GetFriendOutput>>(ret, allCount);
        }

        public async Task<BaseOutputCount<List<GetFriendOutput>>> GetInvites(BaseSkipCountInput input)
        {
            List<GetFriendOutput> ret = new List<GetFriendOutput>();
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            var query = _repoUserFriend.Queryable
                .Include(i => i.User1)
                .Include(i => i.User2)
                .Where(i => !i.IdDeleted &&
                            !i.IsConfirm &&
                            (i.User1Id == session.UserId || i.User2Id == session.UserId) && 
                            i.IniciatorId != session.UserId
                            )
                .ToList();

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);
            var friends = query.Select(i => new
            {
                myFriend = 
                i.User1Id == session.UserId
                    ? i.User2 ?? _reposUsers.FirstOrDefault(d => d.Id == i.User2Id).Result
                    : i.User1 ?? _reposUsers.FirstOrDefault(d => d.Id == i.User1Id).Result,
                row = i
            });
            var allCount = friends.Count();
            friends = friends.Skip(input.Skip).Take(input.Count);
            foreach (var friend in friends)
            {
                ret.Add(new GetFriendOutput
                {
                    UserId = friend.myFriend.Id,
                    FirstName = friend.myFriend.FirstName,
                    LastName = friend.myFriend.LastName,
                    Picture = friend.myFriend.PictureUrl,
                });
            }
            return new BaseOutputCount<List<GetFriendOutput>>(ret, allCount);
        }

        public async Task<object> InviteFriend(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var inviteUser = await _reposUsers.FirstOrDefault(i => !i.IsDeleted && i.Id == input.Id);
            if(inviteUser == null)
                throw new BaseException(CodeError.DoesNotExistUser, 
                    CodeError.DoesNotExistUser.GetMessage(), 
                    CodeError.DoesNotExistUser.GetMessage() + $"with id {input.Id}");

            await _repoUserFriend.Insert(new UserFiends
            {
                User1Id = session.UserId,
                User2Id = inviteUser.Id,
                DateCreation = DateTimeOffset.Now,
                IniciatorId = session.UserId,
            });
            return new object();
        }

        public async Task<object> AcceptInvite(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var invite = await _repoUserFriend.FirstOrDefault(
                i => !i.IdDeleted &&
                     !i.IsConfirm &&
                     (i.User1Id == session.UserId && i.User2Id == input.Id ||

                     i.User2Id == session.UserId && i.User1Id == input.Id));
            if (invite == null)
                throw new BaseException(CodeError.BadRequeast, $"You does not have invite from user with id {input.Id}");

            invite.IsConfirm = true;
            await _repoUserFriend.Update(invite);
            return new object();
        }

        public async Task<object> CancelInvite(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            var invite = _repoUserFriend.Queryable.Where(i => (i.User1Id == session.UserId && i.User2Id == input.Id) ||
                                                          (i.User2Id == session.UserId && i.User1Id == input.Id));

            if(!invite.Any())
                return new object();

            var dbSet = _repoUserFriend.GetDbSet();
            dbSet.RemoveRange(invite);
            await _repoUserFriend.SaveShanges();
            
            return new object();
        }

        public async Task<object> RemoveFriend(BaseInputById input)
        {
            return await this.CancelInvite(input);
        }

        public async Task<GetFriendsWithClientCountAndMasterCountOut> GetFriendsWithCount(GetFriendsWithClientCountAndMasterCountInp input)
        {
            BaseOutputCount<List<GetFriendOutput>> friends = await this.GetFriends(input);

            return new GetFriendsWithClientCountAndMasterCountOut
            {
                List = friends,
                MasterCount = 12,
                ClientCount = 15,
                FriendCount = friends.Count
            };
        }

        public async Task<GetStatusUserAsMyFriendOut> GetStatusUserAsMyFriend(BaseInputById input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            //var userFriendr = await _repoUserFriend.GetList();

            var userFriend = _repoUserFriend.Queryable.FirstOrDefault(i => 
            (i.User1Id == input.Id && i.User2Id == session.UserId) 
            || 
            (i.User1Id == session.UserId && i.User2Id == input.Id));

            if(userFriend == null)
                return new GetStatusUserAsMyFriendOut(StatusAsMyFriend.NotFriend);
            
            if(userFriend.IsConfirm)
                return new GetStatusUserAsMyFriendOut(StatusAsMyFriend.Friend);

            if (!userFriend.IsConfirm && userFriend.User1Id == session.UserId)
                return new GetStatusUserAsMyFriendOut(StatusAsMyFriend.Invited);

            return new GetStatusUserAsMyFriendOut(StatusAsMyFriend.WaitAccept);

        }


    }

    public class GetStatusUserAsMyFriendOut
    {
        public StatusAsMyFriend Status { get; set; }

        public GetStatusUserAsMyFriendOut()
        {
            
        }
        public GetStatusUserAsMyFriendOut(StatusAsMyFriend status)
        {
            this.Status = status;
        }
    }

    public enum StatusAsMyFriend
    {
        Friend = 1,
        NotFriend = 2,
        Invited = 3,
        WaitAccept = 4
    }




    public class GetFriensInp : BaseSkipCountContainsInput
    {
        public long Id { get; set; }
    }

    public class GetFriendsWithClientCountAndMasterCountOut
    {
        public BaseOutputCount<List<GetFriendOutput>> List { get; set; }
        public int FriendCount { get; set; }
        public int ClientCount { get; set; }
        public int MasterCount { get; set; }
    }

    public class GetFriendsWithClientCountAndMasterCountInp : GetFriensInp
    {
        [Required]
        public FriendsTypeEnum FriendsType { get; set; }
    }

    public enum FriendsTypeEnum
    {
        Friends = 1,
        Clients = 2, 
        Master = 3,
    }


    public class GetFriendOutput
    {
        public long UserId  { get; set; }
        public string Picture { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName => FirstName + " " + LastName;
        public int UserTypeId { get; set; }
    }



    
}
