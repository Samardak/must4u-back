﻿using System.Threading.Tasks;

namespace GameBLL.Service
{
    public interface INotificationService
    {
        Task<object> NotifyNewMessage(object message);
        Task<object> NotifyNewCountMessage(object message);
        Task<object> NotifyNewMasterVisit(object input);
    }

    public class NotificationService : INotificationService
    {
        public async Task<object> NotifyNewMessage(object message)
        {
            return message;
        }

        public async Task<object> NotifyNewCountMessage(object message)
        {
            return message;
        }


        /// <summary>
        /// Присылает мастеру уведомление о новом записаном клиенте
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<object> NotifyNewMasterVisit(object input)
        {
            return input;
        }
    }



}
