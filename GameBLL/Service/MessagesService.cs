﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameBLL.Service.InternalService;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using PlatformCore.Application.Common.BaseInputOutput;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.PlatformException;
using Remotion.Linq.Clauses;

namespace GameBLL.Service
{
    public interface IMessagesService
    {
        Task<SendMessageOutput> SendMessage(SendMessageInput input);
        Task<List<GetMessagesWithUserIdOutput>> GetMessagesWithUserId(GetMessagesWithUserId input);
        Task<List<GetAllGroup>> GetAllMessageGroup(BaseSkipCountInput input);
    }

    public class MessagesService : IMessagesService
    {
        private readonly ISessionService _sessionService;
        private readonly IRepository<Users, UsersDTO> _repoUsers;

        private readonly IRepository<MessageGroups, MessageGroupsDTO> _repoMessageGroup;
        private readonly IRepository<MapMessagesGroupUsers, MapMessagesGroupUsersDTO> _repoMapMessageGroupUsersRepository;
        private readonly IRepository<Messages, MessagesDTO> _repoMessages;

        private readonly INotificationService _notificationService;
        private readonly IDateConvertorServiceInternal _convertorServiceInternal;
        private readonly ISkipCountCheckService _skipCountCheckService;



        public MessagesService(
            ISessionService sessionService, 
            IRepository<Users, UsersDTO> repoUsers, 
            IRepository<MessageGroups, MessageGroupsDTO> repoMessageGroup, 
            IRepository<MapMessagesGroupUsers, MapMessagesGroupUsersDTO> repoMapMessageGroupUsersRepository, 
            IRepository<Messages, MessagesDTO> repoMessages, INotificationService notificationService, IDateConvertorServiceInternal convertorServiceInternal, ISkipCountCheckService skipCountCheckService)
        {
            _sessionService = sessionService;
            _repoUsers = repoUsers;
            _repoMessageGroup = repoMessageGroup;
            _repoMapMessageGroupUsersRepository = repoMapMessageGroupUsersRepository;
            _repoMessages = repoMessages;
            _notificationService = notificationService;
            _convertorServiceInternal = convertorServiceInternal;
            _skipCountCheckService = skipCountCheckService;
        }

        public async Task<SendMessageOutput> SendMessage(SendMessageInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            
            
            var user = await _repoUsers.FirstOrDefault(i => i.Id == input.Id);
            if(user == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            var groupId = this.GetGroupId(user.Id, session.UserId);
            var group = await _repoMessageGroup.FirstOrDefault(i => i.GroupId == groupId);
            #region create group and particepan
            if (group == null)
            {
                group = new MessageGroups
                {
                    GroupId = groupId,
                    IniciatorId = session.UserId,
                };
                group = await _repoMessageGroup.Insert(group);

                MapMessagesGroupUsers mapMessageGroupUserIniciator = new MapMessagesGroupUsers
                {
                    UserId = session.UserId,
                    GroupId = group.Id,
                    LastTimeView = DateTimeOffset.Now,
                };
                MapMessagesGroupUsers mapMessageGroupUserParticipant = new MapMessagesGroupUsers
                {
                    UserId = user.Id,
                    GroupId = group.Id,
                    LastTimeView = DateTimeOffset.Now,
                };


                await _repoMapMessageGroupUsersRepository.Insert(mapMessageGroupUserIniciator);
                await _repoMapMessageGroupUsersRepository.Insert(mapMessageGroupUserParticipant);
            }
            #endregion create group and particepan


            var message = await _repoMessages.Insert(new Messages
            {
                DateCreation = DateTimeOffset.Now,
                GroupId = group.Id,
                IsRead = false,
                UserIdFrom = session.UserId,
                UserIdTo = user.Id,
                Text = input.Text,
            });

            await _notificationService.NotifyNewMessage(new
            {
                MessageId = 1,
                Fullname = "",
                PictureUrl = "",
                DateCreaion = 1
            });
            
            return new SendMessageOutput
            {
                DateCreation = _convertorServiceInternal.ConvertToMs(message.DateCreation),
                Text = message.Text,
                MessageId = message.Id
            };
        }

        public async Task<List<GetMessagesWithUserIdOutput>> GetMessagesWithUserId(GetMessagesWithUserId input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);
            var user = await _repoUsers.FirstOrDefault(i => i.Id == input.UserId);
            
            if(user == null)
                throw new BaseException(CodeError.DoesNotExistUser, CodeError.DoesNotExistUser.GetMessage());

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);

            var groupId = this.GetGroupId(session.UserId, input.UserId);
            var group = await _repoMessageGroup.FirstOrDefault(i => i.GroupId == groupId);
            if (group == null)
                return new List<GetMessagesWithUserIdOutput>();
            var messages = _repoMessages.Queryable
                .Where(i => i.GroupId == group.Id)
                .OrderByDescending(i=> i.DateCreation)
                .Skip(input.Skip)
                .Take(input.Count).ToList();

            foreach (var message in messages)
            {
                if (message.UserIdTo == session.UserId)
                {
                    message.IsRead = true;
                    await _repoMessages.Update(message);
                }
            }

            Dictionary<long, string> dict = new Dictionary<long, string>();
            dict[session.UserId] = session.PictureUrl;
            dict[user.Id] = user.PictureUrl;

            var ret = messages.Select(i => new GetMessagesWithUserIdOutput
            {
                UserId = i.UserIdFrom,
                DateCreation = _convertorServiceInternal.ConvertToMs(i.DateCreation),
                Text = i.Text,
                PictureUrl = dict[i.UserIdFrom]
            }).OrderBy(i=> i.DateCreation).ToList();

            

            return ret;
        }

        public async Task<List<GetAllGroup>> GetAllMessageGroup(BaseSkipCountInput input)
        {
            var session = await _sessionService.GetSessionDataAsync(input?.Token);

            (input.Skip, input.Count) = _skipCountCheckService.CheckSkipCount(input.Skip, input.Count);

            var queryGroups = from row in _repoMessages.Queryable
                              where row.UserIdFrom == session.UserId || row.UserIdTo == session.UserId
                              group row by row.GroupId
                              into g
                              select new
                              {
                                  groupId = g.Key,
                                  row = g.OrderByDescending(t => t.Id).FirstOrDefault()
                              };

            var groups = queryGroups.ToList();

            groups = groups
                .OrderByDescending(i => i.row.DateCreation)
                .Skip(input.Skip)
                .Take(input.Count).ToList();

            List<GetAllGroup> ret = new List<GetAllGroup>();
            foreach (var group in groups)
            {
                var userFrom = await _repoUsers.FirstOrDefault(i => i.Id == group.row.UserIdFrom);
                var item = new GetAllGroup
                {
                    UserId = group.row.UserIdFrom == session.UserId ? group.row.UserIdTo : group.row.UserIdFrom,
                    Text = group.row.Text,
                    DateCreation = _convertorServiceInternal.ConvertToMs(group.row.DateCreation),
                    PictureUrl = userFrom?.PictureUrl,
                    IsRead = group.row.IsRead
                };
                ret.Add(item);
            }

            return ret;
        }
        


        
        public string GetGroupId(long userId1, long userId2)
        {
            if(userId1 == userId2)
                throw new BaseException(CodeError.BadRequeast, $"UserId1 should not equals userId2: ({userId1},{userId2})");

            var first = userId1 < userId2 ? userId1 : userId2;
            var second = userId2 < userId1 ? userId1 : userId2;

            var lenFirst = first.ToString().Length;

            var groupId = $"{first}{second}{lenFirst}";
            return groupId;
        }

        public (long userId1, long userId2) GetUsersId(string groupId)
        {
            if (groupId.Length < 4)
            {
                throw new BaseException(CodeError.BadRequeast, "groupId string length should be greater then 3");
            }
            var lenFirstStr = groupId[groupId.Length-1];
            if (!int.TryParse(lenFirstStr.ToString(), out var lenFirst))
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect groupId string");
            }

            var minValueOfStr = lenFirst + 2;
            if (groupId.Length < minValueOfStr)
            {
                throw new BaseException(CodeError.BadRequeast, "Incorrect data of groupId string");
            }

            var firstStr = groupId.Substring(0,lenFirst);
            long.TryParse(firstStr, out var first);
            var secondStr = groupId.Slice(lenFirst, groupId.Length - 1);
            long.TryParse(secondStr, out var second);
            
            return (first, second);
        }

        
    }

    public class GetAllGroup
    {
        public long UserId { get; set; }
        public string Text { get; set; }
        public long DateCreation { get; set; }
        public string PictureUrl { get; set; }
        public bool IsRead { get; set; }
    }

    public class GetMessagesWithUserId : BaseSkipCountInput
    {
        [Required]
        public long UserId { get; set; }
    }

    public class GetMessagesWithUserIdOutput
    {
        public long UserId { get; set; }
        public string Text { get; set; }
        public long DateCreation { get; set; }
        public string PictureUrl { get; set; }
        
    }

    public static class StringExtention
    {
        public static string Slice(this string input, int start, int end)
        {
            var len = end - start;
            return input.Substring(start, len);
        }
    }

    public class SendMessageInput: BaseInputById
    {
        [Required]
        public string Text { get; set; }
    }

    public class SendMessageOutput
    {
        public long MessageId { get; set; }
        public string Text { get; set; }
        public long DateCreation { get; set; }
    }
}
