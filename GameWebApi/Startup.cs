﻿using System.Threading;
using AutoMapper;
using Game.Tests.BLL.Services;
using GameBLL.Service;
using GameBLL.Service.CacheServices;
using GameBLL.Service.Client;
using GameBLL.Service.InternalService;
using GameBLL.Service.Master;
using GameBLL.Settings;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using GameDAL.RepositoryGoGaWi;
using GameWebApi.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Application.Common.GeneralLogging;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.GameTokenService;
using PlatformCore.Common.GeneralLogging;
using PlatformCore.Common.Middleware;
using PlatformCore.Common.RequestHelper;
using PlatformCore.DAL.Common.CacheService;
using Swashbuckle.AspNetCore.Swagger;

namespace GameWebApi
{
    public class Startup
    {
        private Timer _timer;
        
        
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }
        

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            #region configure app settings
            var configSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(configSection);
            var fullPathToFileLog = configSection.GetValue<string>("FullPathToFileLog");
            if (string.IsNullOrEmpty(fullPathToFileLog))
            {
                var path = PlatformServices.Default.Application.ApplicationBasePath;
                fullPathToFileLog = path + "\\game.txt";
            }
            #endregion

            services.AddMvc();

            #region service db repositoryWithDto
            services.AddDbContext<Must4uModuleContext>();
            

            services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));




            #endregion service db repositoryWithDto

            #region auto mapper
            //pacckage AutoMapper
            Mapper.Initialize(config =>
            {
                //config.CreateMap<TournamentGameDto, TournamentGames>().ReverseMap();
                //config.CreateMap<Sports, SportDto>().ReverseMap();
                //config.CreateMap<CompetitionDto, Competitions>().ReverseMap();

                config.CreateMap<AuthDatas, AuthDatasDTO>().ReverseMap();
                config.CreateMap<AuthTypes, AuthTypesDTO>().ReverseMap();
                config.CreateMap<Cities, CitiesDTO>().ReverseMap();
                config.CreateMap<Countries, CountriesDTO>().ReverseMap();
                config.CreateMap<StationRegions, StationRegionsDTO>().ReverseMap();
                config.CreateMap<Users, UsersDTO>().ReverseMap();
                config.CreateMap<ServicesType, ServicesTypeDTO>().ReverseMap();
                config.CreateMap<MasterServices, MasterServicesDTO>().ReverseMap();
                config.CreateMap<UserSocialNetworks, UserSocialNetworksDTO>().ReverseMap();
                config.CreateMap<Posts, PostsDTO>().ReverseMap();
                config.CreateMap<PostComents, PostComentsDTO>().ReverseMap();
                config.CreateMap<PostLikes, PostLikesDTO>().ReverseMap();
                config.CreateMap<PostTags, PostTagsDTO>().ReverseMap();



                config.CreateMap<MasterSchedules, MasterSchedulesDTO>().ReverseMap();


                config.CreateMap<MapMessagesGroupUsers, MapMessagesGroupUsersDTO>().ReverseMap();
                config.CreateMap<MessageGroups, MessageGroupsDTO>().ReverseMap();
                config.CreateMap<Messages, MessagesDTO>().ReverseMap();

                config.CreateMap<MasterServices, MasterServicesDTO>().ReverseMap();
                

            });
            
            #endregion auto mapper

            #region redis DI
            services.AddSingleton<IСacheService>(serviceProvider => new СacheService(
                    //$"{ConnectionConfig.ServerAddress}:6379,allowAdmin=true",
                    $"localhost:6379,allowAdmin=true",
                    1)
                );
            #endregion

            #region all service DI

            

            services.AddScoped(typeof(IRequestHelper), typeof(RequestHelper));
            services.AddScoped(typeof(ISessionService), typeof(SessionService));
            services.AddScoped(typeof(ITokenService), typeof(TokenService));
            
            services.AddScoped(typeof(IDateConvertorServiceInternal), typeof(DateConvertorServiceInternal));
            services.AddScoped(typeof(IAuthService), typeof(AuthService));
            services.AddScoped(typeof(IProfileMasterService), typeof(ProfileMasterService));
            services.AddScoped(typeof(IMasterScheduleService), typeof(MasterScheduleService));
            services.AddScoped(typeof(IFounderFileNameService), typeof(FounderFileNameService));
            services.AddScoped(typeof(ISkipCountCheckService), typeof(SkipCountCheckService));
            services.AddScoped(typeof(IPostService), typeof(PostService));
            services.AddScoped(typeof(IProfileService), typeof(ProfileService));
            services.AddScoped(typeof(IFriendService), typeof(FriendService));


            services.AddScoped(typeof(IMessagesService), typeof(MessagesService));
            services.AddScoped(typeof(INotificationService), typeof(NotificationService));

            services.AddScoped(typeof(ISearchService), typeof(SearchService));
            services.AddScoped(typeof(LocationService));
            services.AddScoped(typeof(ServiceTypeCacheServices));

            services.AddScoped(typeof(IMasterService), typeof(MasterService));
            services.AddScoped(typeof(ClientService));








            services.AddSingleton<ICustomLogging>(
               //servicesProvider => new CustomLogging(@"E:\PlatformCore\PlatformCore\ClientWebApi\client.txt"));
               servicesProvider => new CustomLogging(fullPathToFileLog));
            /// Will write all request and response data
            /// to database (table AppRequestLogging)
            //services.AddScoped(typeof(IAppRequestLoggingService), typeof(AppRequestLoggingGameService));

            #endregion all service DI


            #region cors
            var corsBuilder = new CorsPolicyBuilder();
            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            corsBuilder.AllowCredentials();
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll", corsBuilder.Build());
            });
            services.AddCors(); 
            #endregion cors


            services.AddMvc().AddJsonOptions(options =>
            {
                //options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.Formatting = Formatting.Indented;
            });

            var hostEnv = services.BuildServiceProvider().GetRequiredService<IHostingEnvironment>();
            var appSettings = services.BuildServiceProvider().GetRequiredService<IOptions<AppSettings>>().Value;
            var pathToXmlFile = appSettings.PathToGameWebApiXml;

            if (string.IsNullOrEmpty(pathToXmlFile))
            {
                pathToXmlFile = $@"{hostEnv.ContentRootPath}\wwwroot\GameWebApi.xml";
            }

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                c.IncludeXmlComments(pathToXmlFile);
                //http://www.talkingdotnet.com/how-to-upload-file-via-swagger-in-asp-net-core-web-api/
                c.OperationFilter<FileUploadOperation>();
            });


            
            
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware<ExceptionMiddleware>(); //для того, что если еррор возникнет в кастомных midleware
            ////app.UseMiddleware<AppRequestLoggingMiddleware>(); //для того, что если еррор возникнет в кастомных midleware
            ////Have a bugs :   Additional text encountered after finished reading JSON content: {. Path '', line 9, position 1.
            app.UseMiddleware<LoggingMiddleware>();  //пишет request and response for client in txt.file
            app.UseMiddleware<ExceptionMiddleware>();//catch exxception which I will throw in BLL (or DAL) 
            app.UseMiddleware<ResponseMiddleware>(); //Wrap success response to BaseOutput<T>

            //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/static-files
            app.UseStaticFiles();
            app.UseFileServer(true);

            app.UseCors("AllowAll");

            #region use signalR
            app.UseCors(cors =>
            {
                cors.AllowAnyHeader();
                cors.AllowAnyOrigin();
                cors.AllowAnyMethod();
                cors.AllowCredentials();
            });

            //app.UseWebSockets();
            //app.UseSignalR();
            #endregion use signalR


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}",
                    defaults: new { controller = "Values", action = "Get" });
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();


            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                
            });

        }
    }
}
