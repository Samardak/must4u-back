﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;

namespace GameWebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {

            var config = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json", optional: true)
              .Build();

            var serverUrl = config.GetValue<string>("server.urls");

            var host = new WebHostBuilder()
                .UseKestrel()
                 //http://benfoster.io/blog/how-to-configure-kestrel-urls-in-aspnet-core-rc2 
                 /*
                  * How to configure Kestrel URLs in ASP.NET Core RC2
                  * http://benfoster.io/blog/how-to-configure-kestrel-urls-in-aspnet-core-rc2 
                  */
                 .UseConfiguration(config)
                .UseUrls(serverUrl)
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
