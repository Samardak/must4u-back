﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.ApplicationInsights.AspNetCore.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.PlatformException;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Picture
{
    
    [Route("api/[controller]")]
    public class PictureController : Controller
    {
        private readonly IFounderFileNameService _founderFileNameService;
        private readonly IHostingEnvironment _hostingEnvironment;


        public PictureController(IFounderFileNameService founderFileNameService, IHostingEnvironment hostingEnvironment)
        {
            _founderFileNameService = founderFileNameService;
            _hostingEnvironment = hostingEnvironment;
        }



        /// <summary>
        /// Загружая фото получаем ссылку на нее. И уже эту ссылку можем сохранять для разных сущностей(новости, аватарки, акции и тд)
        /// </summary>
        /// <param name="uploadedFile"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Upload")]
        public object Upload([FromForm] IFormFile uploadedFile)
        {
            try
            {

                var curUrl = HttpContext.Request.GetUri();
                var httpPath = $"{curUrl.Scheme}://{curUrl.Host}:{curUrl.Port}";
                string wwwRootPath = _hostingEnvironment.WebRootPath;
                wwwRootPath = wwwRootPath.Replace("\\", "/");

                #region get and check image
                //var files = Request.Form.Files;
                IFormFile postedFile = uploadedFile;//files.FirstOrDefault();
                if (postedFile == null)
                    throw new BaseException(CodeError.BadRequeast, "Please upload photo to Form.Files", "");
                int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB
                IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                var fileName = postedFile.FileName;
                var extension = ext.ToLower();
                if (!AllowedFileExtensions.Contains(extension))
                    throw new BaseException(CodeError.BadRequeast, "Please Upload image of type .jpg,.gif,.png.", "");
                if (postedFile.Length > MaxContentLength)
                    throw new BaseException(CodeError.BadRequeast, "Please Upload a file upto 5 mb.", "");
                #endregion get and check image

                var pathToFolderSport = $"{wwwRootPath}/root";
                var name = _founderFileNameService.FindNameForFile(pathToFolderSport);
                name = name + ext;
                SaveFile(postedFile, pathToFolderSport + "/" + name);
                var pictureUrl = $"{httpPath}/root/{name}";


                
                return new UploadFileOutput { PictureUrl = pictureUrl };
                

            }
            catch (Exception exception)
            {
                #region handle exception
                BaseException baseException = exception as BaseException;

                BaseError respError = null;

                if (baseException != null)
                    respError = new BaseError(baseException);
                else
                    respError = new BaseError((int)HttpStatusCode.InternalServerError, exception.Message, exception.ToString());

                return respError;
                #endregion handle exception
            }
            //http://www.talkingdotnet.com/how-to-upload-file-via-swagger-in-asp-net-core-web-api/
            //TODO: Save file
        }




        private void SaveFile(IFormFile postedFile, string path)
        {
            using (FileStream fs = System.IO.File.Create(path))
            {
                postedFile.CopyTo(fs);
                fs.Flush();
            }
        }




        //[HttpPost]
        //[Route("UploadFile")]
        //public async Task<UploadFileOutput> UploadFile()
        //{
        //    try
        //    {
        //        var curUrl = HttpContext.Request.GetUri();
        //        var httpPath = $"{curUrl.Scheme}://{curUrl.Host}:{curUrl.Port}";
        //        string wwwRootPath = _hostingEnvironment.WebRootPath;
        //        wwwRootPath = wwwRootPath.Replace("\\", "/");

        //        #region get and check image
        //        var files = Request.Form.Files;
        //        IFormFile postedFile = files.FirstOrDefault();
        //        if (postedFile == null)
        //            throw new BaseException(CodeError.BadRequeast, "Please upload photo to Form.Files", "");
        //        int MaxContentLength = 1024 * 1024 * 5; //Size = 5 MB
        //        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
        //        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //        var fileName = postedFile.FileName;
        //        var extension = ext.ToLower();
        //        if (!AllowedFileExtensions.Contains(extension))
        //            throw new BaseException(CodeError.BadRequeast, "Please Upload image of type .jpg,.gif,.png.", "");
        //        if (postedFile.Length > MaxContentLength)
        //            throw new BaseException(CodeError.BadRequeast, "Please Upload a file upto 5 mb.", "");
        //        #endregion get and check image

        //        var pathToFolderSport = $"{wwwRootPath}/root";
        //        var name = _founderFileNameService.FindNameForFile(pathToFolderSport);
        //        name = name + ext;
        //        SaveFile(postedFile, pathToFolderSport + "/" + name);
        //        var pictureUrl = $"{httpPath}/root/{name}";

        //        return new UploadFileOutput { PictureUrl = pictureUrl };
        //    }
        //    catch (Exception exception)
        //    {
        //        throw new BaseException(CodeError.InternalServerIncorrectData, "Ошибка при загрузки фото!", exception.ToString() );
        //    }
        //}

    }

    public class UploadFileOutput
    {
        public string PictureUrl { get; set; }
    }
}
