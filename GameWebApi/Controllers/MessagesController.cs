﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.BaseInputOutput;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : IMessagesService
    {
        private readonly IMessagesService _messagesService;

        public MessagesController(IMessagesService messagesService)
        {
            _messagesService = messagesService;
        }

        /// <summary>
        /// Отправляем сообщение другу. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/5b98a486-35ee-40e5-98f3-69c854f282f0/-1
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendMessage")]
        public async Task<SendMessageOutput> SendMessage([FromBody]SendMessageInput input)
        {
            return await _messagesService.SendMessage(input);
        }
        /// <summary>
        /// Получаем последние N сообщений по userId. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/5b98a486-35ee-40e5-98f3-69c854f282f0/-1
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetMessagesWithUserId")]
        public async Task<List<GetMessagesWithUserIdOutput>> GetMessagesWithUserId([FromBody]GetMessagesWithUserId input)
        {
            return await _messagesService.GetMessagesWithUserId(input);
        }
        /// <summary>
        /// Получаем все чаты, в которых мы учавствуем. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/9ad91ead-4ba1-45ea-8a34-ab8548efbfdd/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAllMessageGroup")]
        public async Task<List<GetAllGroup>> GetAllMessageGroup([FromBody]BaseSkipCountInput input)
        {
            return await _messagesService.GetAllMessageGroup(input);
        }
    }
}
