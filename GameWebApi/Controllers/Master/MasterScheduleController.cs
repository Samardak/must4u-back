﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Master
{
    [Route("api/[controller]")]
    public class MasterScheduleController : ValidateController //, IMasterScheduleService
    {
        private readonly IMasterScheduleService _masterScheduleService;

        public MasterScheduleController(IMasterScheduleService masterScheduleService)
        {
            _masterScheduleService = masterScheduleService;
        }



        #region region week schedule
        /// <summary>
        /// Получаем текущий график по установленным дням. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/4b001636-b409-4b5c-b45c-e9933e6767e9/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetWeekSchedule")]
        public async Task<GetWeekScheduleOutput> GetWeekSchedule([FromBody]BaseInput input)
        {
            return await _masterScheduleService.GetWeekSchedule(input);
        }
        /// <summary>
        /// Устанавливаем график по всем дням.(необходимо присылать весь обьект, не только то что добавилось) Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/4b001636-b409-4b5c-b45c-e9933e6767e9/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetWeekSchedule")]
        public async Task<object> SetWeekSchedule([FromBody]GetWeekScheduleInput input)
        {
            return await _masterScheduleService.SetWeekSchedule(input);
        }
        #endregion region week schedule


        #region day schedule
        /// <summary>
        /// Получаем график по выбранному дню. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/6a6d59fa-29f6-438e-8123-25d77f5d0146/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetDaySchedule")]
        public async Task<GetDayScheduleOutput> GetDaySchedule([FromBody]GetDayScheduleInput input)
        {
            return await _masterScheduleService.GetDaySchedule(input);
        }
        /// <summary>
        /// Устанавливаем график для выбраного дня по всем неделям. (Присылаем если стоит radio btn 1) Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/6a6d59fa-29f6-438e-8123-25d77f5d0146/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetDayScheduleEveryWeek")]
        public async Task<object> SetDayScheduleEveryWeek([FromBody]SetDayScheduleInput input)
        {
            return await _masterScheduleService.SetDayScheduleEveryWeek(input);
        }
        /// <summary>
        /// Устанавливаем график только для выбраного даты. (Присылаем если стоит radio btn 3) Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/6a6d59fa-29f6-438e-8123-25d77f5d0146/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetDayScheduleCurrentDay")]
        public async Task<object> SetDayScheduleCurrentDay([FromBody]SetDayScheduleInput input)
        {
            return await _masterScheduleService.SetDayScheduleCurrentDay(input);
        }
        /// <summary>
        /// Устанавливаем выходной для текущей датты. (Присылаем если стоит radio btn 2) Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/e4118a29-24a3-4b25-af3b-a0422e9ac63d/-1
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetDayScheduleFreeDay")]
        public async Task<object> SetDayScheduleFreeDay([FromBody]SetDayScheduleInput input)
        {
            return await _masterScheduleService.SetDayScheduleCurrentDay(input);
        }
        #endregion day schedule
    }
}
