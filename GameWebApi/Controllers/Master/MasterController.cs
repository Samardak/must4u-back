﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service.Master;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Master
{
    [Route("api/[controller]")]
    public class MasterController : ValidateController, IMasterService
    {
        private readonly IMasterService _masterService;

        public MasterController(IMasterService masterService)
        {
            _masterService = masterService;
        }

        [HttpPost]
        [Route("Profile")]
        public async Task<MasterProfileOut> Profile([FromBody]BaseInputById input)
        {
            return await _masterService.Profile(input);
        }
    }
}
