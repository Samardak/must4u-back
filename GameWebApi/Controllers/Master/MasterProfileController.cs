﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Master
{
    [Route("api/[controller]")]
    public class MasterProfileController : ValidateController, IProfileMasterService
    {
        private readonly IProfileMasterService _profileMasterService;
        private readonly ISearchService _searchService;

        public MasterProfileController(IProfileMasterService profileMasterService, 
            ISearchService searchService)
        {
            _profileMasterService = profileMasterService;
            _searchService = searchService;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Получение профайла у мастера. Страница местоположением мастера. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/5f342a19-d5c2-4e2a-8632-870fb3b6143b/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetProfilePlace")]
        public async Task<ProfilePlace> GetProfilePlace([FromBody]BaseInput input)
        {
            return await _profileMasterService.GetProfilePlace(input);
        }
        /// <summary>
        /// Находим города по указанному id странны. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/de5b2dc0-4539-48a2-bfda-9944eec3f3dc/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchCities")]
        public async Task<List<CitiesDTO>> SearchCities([FromBody]GetCities input)
        {
            return await _searchService.SearchCities(input);
        }
        /// <summary>
        /// Находим станции или регион города по указанному id корода. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/de5b2dc0-4539-48a2-bfda-9944eec3f3dc/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchStationRegions")]
        public async Task<List<StationRegionsDTO>> SearchStationRegions([FromBody] GetStation input)
        {
            return await _searchService.SearchStationRegions(input);
        }


        /// <summary>
        /// Устанавливаем данные на странице с местоположением мастера. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/de5b2dc0-4539-48a2-bfda-9944eec3f3dc/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetProfilePlace")]
        public async Task<object> SetProfilePlace([FromBody]SetProfilePlace input)
        {
            return await _profileMasterService.SetProfilePlace(input);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /// <summary>
        /// Получаем установленные услуги мастера, а также возможный список услуг. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/4b0792ce-19a9-4e6c-bc7b-eed0f910363a/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetProfileServices")]
        public async Task<GetProfileServices> GetProfileServices([FromBody]BaseInput input)
        {
            return await _profileMasterService.GetProfileServices(input);
        }
        /// <summary>
        /// Устанавливаем услуги мастера. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/4b0792ce-19a9-4e6c-bc7b-eed0f910363a/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetProfileServices")]
        public async Task<object> SetProfileServices([FromBody]SetProfileService input)
        {
            return await _profileMasterService.SetProfileServices(input);
        }




        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Получаем ссылки на соц. сети у мастера. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/ec1768cf-7f6b-47f6-9e10-9b339aa3b03e/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetSocaialLink")]
        public async Task<UserSocialNetworksDTO> GetSocaialLink([FromBody]BaseInput input)
        {
            return await _profileMasterService.GetSocaialLink(input);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Устанавливаем ссылки на соц. сети у мастера. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/ec1768cf-7f6b-47f6-9e10-9b339aa3b03e/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetSocialLink")]
        public async Task<object> SetSocialLink([FromBody]SocialNetworkInput input)
        {
            return await _profileMasterService.SetSocialLink(input);
        }




        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Получаем описание мастера(обращение к клиентам). Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/d5ca00e5-cfa3-49ba-ac96-2a9a91b3b301/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetAboutText")]
        public async Task<GetAboutText> GetAboutText([FromBody] BaseInput input)
        {
            return await _profileMasterService.GetAboutText(input);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Устанавливаем описание мастера(обращение к клиентам). Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/d5ca00e5-cfa3-49ba-ac96-2a9a91b3b301/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SetAboutText")]
        public async Task<object> SetAboutText([FromBody] SetAboutText input)
        {
            return await _profileMasterService.SetAboutText(input);
        }







    }
}
