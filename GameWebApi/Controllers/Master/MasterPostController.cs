﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Master
{
    [Route("api/[controller]")]
    public class MasterPostController : ValidateController
    {
        private readonly IPostService _postService;

        public MasterPostController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        /// Создаем новый пост. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/dcb0e69f-7a5a-434d-9d36-7800269feaab/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(CreatePost))]
        public async Task<PostsDTO> CreatePost([FromBody]CreatePostInput input)
        {
            return await _postService.CreatePost(input);
        }
    }
}
