﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class MasterVisitsController : ValidateController, IVisitMasterService
    {
        private readonly IVisitMasterService _visitMasterService;

        public MasterVisitsController(IVisitMasterService visitMasterService)
        {
            _visitMasterService = visitMasterService;
        }


        /// <summary>
        /// Получаем график работы мастера(доступные и уже занятые) и все его услуги. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/fa7943f1-2b96-4a85-837e-4870ea298aac/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [Route("GetVisits")]
        [HttpPost]
        public Task<GetVisitsOutput> GetVisits([FromBody] GetVisitsInput input)
        {
            return _visitMasterService.GetVisits(input);
        }

        /// <summary>
        /// Записываемся к мастеру на свободное время (мастеру будет отправлено уведомление). Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/fa7943f1-2b96-4a85-837e-4870ea298aac/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [Route("ClientSetVisitToMaster")]
        [HttpPost]
        public Task<object> ClientSetVisitToMaster([FromBody]SetVisitsInput input)
        {
            return _visitMasterService.ClientSetVisitToMaster(input);
        }
    }
}
