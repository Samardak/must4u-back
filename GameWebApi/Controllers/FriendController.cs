﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.BaseInputOutput;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class FriendController : ValidateController, IFriendService
    {
        private readonly IFriendService _friendService;

        public FriendController(IFriendService friendService)
        {
            _friendService = friendService;
        }

        /// <summary>
        /// Получаем всех друзей. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/94e8f150-0d8e-4553-a4c6-4624d153dd99/-
        /// </remarks>
        /// <param name="input">Contains -> строка которую должны содержать имена или фамилии.
        /// Можно ничего не передавать, прийдут в рандомном порядке.
        /// Skip -> сколько нужно пропустить пользователей.
        /// Count -> сколько нужно взять пользователей(не больше чем 10)
        /// </param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetFriends")]
        public async Task<BaseOutputCount<List<GetFriendOutput>>> GetFriends([FromBody]GetFriensInp input)
        {
            return await _friendService.GetFriends(input);
        }
        /// <summary>
        /// Получаем приглошения. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/08f6632b-d637-429d-87cf-7652247d5910/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetInvites")]
        public async Task<BaseOutputCount<List<GetFriendOutput>>> GetInvites([FromBody]BaseSkipCountInput input)
        {
            return await _friendService.GetInvites(input);
        }
        /// <summary>
        /// Приглашаем друга. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/cbb25ba2-e62b-4924-b972-e9bb8b443d8f/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InviteFriend")]
        public async Task<object> InviteFriend([FromBody]BaseInputById input)
        {
            return await _friendService.InviteFriend(input);
        }
        /// <summary>
        /// Принимаем приглашение друга. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/031ff8c5-61ae-4fa7-b688-74d556c5316a/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AcceptInvite")]
        public async Task<object> AcceptInvite([FromBody]BaseInputById input)
        {
            return await _friendService.AcceptInvite(input);
        }

        [HttpPost]
        [Route("CancelInvite")]
        public async Task<object> CancelInvite([FromBody]BaseInputById input)
        {
            return await _friendService.CancelInvite(input);
        }

        [HttpPost]
        [Route("RemoveFriend")]
        public async Task<object> RemoveFriend([FromBody]BaseInputById input)
        {
            return await _friendService.RemoveFriend(input);
        }

        /// <summary>
        /// Получаем друзей и количество в других параметрах. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/d230ca32-7f41-49b0-b6f4-959de483ac4b/-1
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetFriendsWithCount")]
        public async Task<GetFriendsWithClientCountAndMasterCountOut> GetFriendsWithCount([FromBody]GetFriendsWithClientCountAndMasterCountInp input)
        {
            return await _friendService.GetFriendsWithCount(input);
        }

        /// <summary>
        /// Получаем статус юзера (Friend = 1,NotFriend = 2,Invited = 3,WaitAccept = 4). Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/cbb25ba2-e62b-4924-b972-e9bb8b443d8f/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetStatusUserAsMyFriend")]
        public async Task<GetStatusUserAsMyFriendOut> GetStatusUserAsMyFriend([FromBody]BaseInputById input)
        {
            return await _friendService.GetStatusUserAsMyFriend(input);
        }
    }
}
