﻿using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;
using PlatformCore.Common.Game.InputOutput.IRegistrationService;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : ValidateController, IAuthService
    {

        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// Регистрация для клиента и мастера через почту. Link inside
        /// </summary>
        /// <remarks>
        /// Link : http://94.249.192.145:8765/%D1%81%D0%BB%D0%B0%D0%B9%D0%B4%D1%8B/%D0%9A%D0%BB%D0%B8%D0%B5%D0%BD%D1%82/%D0%92%D1%85%D0%BE%D0%B4%20-%20%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F%20%E2%80%93%20%D0%9C%D1%8B%D0%BB%D0%BE@2x.png
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Register")]
        public async Task<RegisterOutput> Register([FromBody]RegisterInput input)
        {
            return await _authService.Register(input);
        }
        /// <summary>
        /// Регистрация для клиента и мастера через почту и авторизация. Link inside
        /// </summary>
        /// <remarks>
        /// Link : http://94.249.192.145:8765/%D1%81%D0%BB%D0%B0%D0%B9%D0%B4%D1%8B/%D0%9A%D0%BB%D0%B8%D0%B5%D0%BD%D1%82/%D0%92%D1%85%D0%BE%D0%B4%20-%20%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F%20%E2%80%93%20%D0%9C%D1%8B%D0%BB%D0%BE@2x.png
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RegisterAndLogin")]
        public async Task<LoginOutput> RegisterAndLogin([FromBody]RegisterInput input)
        {
            return await _authService.RegisterAndLogin(input);
        }
        /// <summary>
        /// Авторизация для мастера и для клиента. Link inside
        /// </summary>
        /// <remarks>
        /// Link : http://94.249.192.145:8765/%D1%81%D0%BB%D0%B0%D0%B9%D0%B4%D1%8B/%D0%9A%D0%BB%D0%B8%D0%B5%D0%BD%D1%82/%D0%92%D1%85%D0%BE%D0%B4%20-%20%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F-1@2x.png
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public async Task<LoginOutput> Login([FromBody]LoginInput input)
        {
            return await _authService.Login(input);
        }

        [HttpPost]
        [Route("LogOut")]
        public async Task<object> LogOut([FromBody]BaseInput input)
        {
            return await _authService.LogOut(input);
        }
    }
}
