﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Search
{
    [Route("api/[controller]")]
    public class SearchController : ValidateController, ISearchService
    {
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }
        /// <summary>
        /// Поиск городов в стране.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchCities")]
        public async Task<List<CitiesDTO>> SearchCities([FromBody]GetCities input)
        {
            return await _searchService.SearchCities(input);
        }

        /// <summary>
        /// Поиск регионов и станций в городе.
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchStationRegions")]
        public async Task<List<StationRegionsDTO>> SearchStationRegions([FromBody]GetStation input)
        {
            return await _searchService.SearchStationRegions(input);
        }
        
        /// <summary>
        /// Поиск количества мастеров по фильтру . Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/d1b3dc6c-1168-4b9a-b08e-394510055213/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchMastersCount")]
        public async Task<SearchMastersCountOut> SearchMastersCount([FromBody]SearchMasterCountInp input)
        {
            return await _searchService.SearchMastersCount(input);
        }
        /// <summary>
        /// Поиск мастеров по фильтру . Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/d1b3dc6c-1168-4b9a-b08e-394510055213/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchMasters")]
        public async Task<List<SearchMasterOut>> SearchMasters([FromBody]SearchMasterInp input)
        {
            return await _searchService.SearchMasters(input);
        }
        /// <summary>
        /// Полный поиск количества мастеров по фильтру . Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FullSearchMastersCount")]
        public async Task<SearchMastersCountOut> FullSearchMastersCount([FromBody]FullSearchMasterCountInp input)
        {
            return await _searchService.FullSearchMastersCount(input);
        }
        /// <summary>
        /// Полынй поиск мастеров по фильтру . Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/906e471b-2082-498e-9f35-81e19653bb17/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("FullSearchMasters")]
        public async Task<List<SearchMasterOut>> FullSearchMasters([FromBody]FullSearchMasterInp input)
        {
            return await _searchService.FullSearchMasters(input);
        }
    }
}
