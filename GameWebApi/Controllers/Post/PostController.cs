﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers.Post
{
    [Route("api/[controller]")]
    public class PostController : ValidateController, IPostService
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }


        /// <summary>
        /// Создаем пост от имени мастера и можем кого то отмечать. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/dcb0e69f-7a5a-434d-9d36-7800269feaab/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(CreatePost))]
        public async Task<PostsDTO> CreatePost([FromBody]CreatePostInput input)
        {
            return await _postService.CreatePost(input);
        }
        /// <summary>
        /// Получаем все посты по юзеру. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/74babef3-a8a2-4ef2-881f-466c27ab5dbd/-1
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(GetPostsByUserId))]
        public async Task<List<PostsDTO>> GetPostsByUserId([FromBody]GetPostsByUserId input)
        {
            return await _postService.GetPostsByUserId(input);
        }


        /// <summary>
        /// Ставим like или dislike для поста. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/76f6fc1a-eef8-4f73-9b5f-20414ef21034/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(SetImpression))]
        public async Task<PostsDTO> SetImpression([FromBody]SetImpressionInp input)
        {
            return await _postService.SetImpression(input);
        }
        /// <summary>
        /// Создаем коммент к посту. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/76c46707-9d40-49e1-8b70-519adbef2691/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(CreatePostComment))]
        public async Task<GetCommentOut> CreatePostComment([FromBody]CreatePostCommentInp input)
        {
            return await _postService.CreatePostComment(input);
        }

        /// <summary>
        /// Отмечаем юзера на нашем посте. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/9a63966e-9b6f-44f8-8af3-d8a219821dcc/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(TagUser))]
        public async Task<PostsDTO> TagUser([FromBody]TagUserInput input)
        {
            return await _postService.TagUser(input);
        }

        /// <summary>
        /// Берем все новости или же акции по нашему юзеру. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/76f6fc1a-eef8-4f73-9b5f-20414ef21034/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(GetPosts))]
        public async Task<List<GetPostOut>> GetPosts([FromBody]GetPostInp input)
        {
            return await _postService.GetPosts(input);
        }
        /// <summary>
        /// Берем все коменты по посту. Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/76c46707-9d40-49e1-8b70-519adbef2691/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(GetComments))]
        public async Task<List<GetCommentOut>> GetComments([FromBody]GetCommentInp input)
        {
            return await _postService.GetComments(input);
        }
    }
}
