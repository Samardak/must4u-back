﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameBLL.Service;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.BaseInputOutput;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly IRepository<Must4uTestTable> _repositoryWithDto;

        public ValuesController(IRepository<Must4uTestTable> repositoryWithDto)
        {
            _repositoryWithDto = repositoryWithDto;
        }

        // GET api/values
        [HttpGet]
        [Route("Get")]
        public IEnumerable<string> Get()
        {
            //return null;
            return new string[] { "value22", "value2" };
        }

        [HttpGet]
        [Route("DtTest")]
        public async Task<List<Must4uTestTable>> DtTest()
        {
            return await _repositoryWithDto.GetList();
        }

        [HttpGet]
        [Route("DtTest2")]
        public async Task<List<Must4uTestTable>> DtTest2()
        {
            return await _repositoryWithDto.GetList();
        }


        //[HttpGet]
        //[Route("GetError")]
        //[SwaggerResponse(200, Type = typeof(BaseOutput<RequestSessionResponse>))]
        //public IEnumerable<string> GetError()
        //{
        //    throw new BaseException(0,"","");
        //    throw new Exception();
        //}

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/values
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
