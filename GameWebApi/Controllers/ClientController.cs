﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service.Client;
using Microsoft.AspNetCore.Mvc;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CustomController;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GameWebApi.Controllers
{
    [Route("api/[controller]")]
    public class ClientController : ValidateController
    {
        private readonly ClientService _clientService;

        public ClientController(ClientService clientService)
        {
            _clientService = clientService;
        }

        /// <summary>
        /// Получаем профайл клиента Link inside
        /// </summary>
        /// <remarks>
        /// Link : https://xd.adobe.com/view/2f9adab8-2d90-4819-88c6-069bca6a1b2b/screen/efffccf2-aacb-4c0e-bcc1-6061cac76193/-
        /// </remarks>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Profile")]
        public async Task<ClientProfileOut> Profile([FromBody]BaseInputById input)
        {
            return await _clientService.Profile(input);
        }
        
    }
}
