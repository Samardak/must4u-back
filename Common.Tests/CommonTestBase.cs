﻿using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.CommonService.EmailSender;

namespace Common.Tests
{
    public class CommonTestBase
    {
        private ServiceCollection Services { get; set; }
        public IServiceProvider ServiceProvider { get; set; }

        public CommonTestBase()
        {
            Services = new ServiceCollection();
            #region service db repositoryWithDto
            //http://6figuredev.com/technology/generic-repositoryWithDto-dependency-injection-with-net-core/
            //Services.AddDbContext<AccountingDAL.Models.AccountingModuleContext>();
            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));


            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));

            //Services.AddScoped(typeof(IRepositoryTransactions), typeof(RepositoryTransactions));
            //Services.AddScoped(typeof(IRepositoryTransactionDocuments), typeof(RepositoryTransactionDocuments));
            //Services.AddScoped(typeof(IRepositoryFinancialTypeMappings), typeof(RepositoryFinancialTypeMappings));


            //Services.AddScoped(typeof(IRepositoryWithDto<>), typeof(Repository<>));
            #endregion service db repositoryWithDto


            ConfigureService();
            ConfigurateAutomapper();


            ServiceProvider = Services.BuildServiceProvider();
            //_repositoryAccounting = serviceProvider.GetRequiredService<IRepositoryWithDto<Accounts>>();
        }

        public void ConfigureService()
        {
            //Services.AddScoped(typeof (IAccountingServiceApp), typeof (AccountingServiceApp));
            //Services.AddScoped(typeof(ITransactionServiceApp), typeof(TransactionServiceApp));

            Services.AddScoped(typeof(IEmailSenderService), typeof(EmailSenderServiceService));

            //Services.AddScoped(typeof(ITransactionService), typeof(TransactionService));

        }

        public void ConfigurateAutomapper()
        {
            Mapper.Initialize(config =>
            {
                //config.CreateMap<Accounts, AccountsDto>().ReverseMap();

                //config.CreateMap<Transactions, TransactionsDto>().ReverseMap();
                //config.CreateMap<TransactionDocuments, TransactionDocumentsDto>().ReverseMap();
                //config.CreateMap<TransactionDocumentTypes, TransactionDocumentTypesDto>().ReverseMap();

                //config.CreateMap<FinancialTypeMappings, FinancialTypeMappingsDto>().ReverseMap();
            });
        }
    }
}