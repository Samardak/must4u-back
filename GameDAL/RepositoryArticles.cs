﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using AutoMapper;
//using GameDAL.IRepositoryWithDto;
//using GameDAL.Models;
//using Microsoft.EntityFrameworkCore;
//using PlatformCore.Common.Game.Dto;

//namespace GameDAL.Repository
//{
//    public class RepositoryArticles : IRepositoryArticles
//    {
//        private GameModuleContext dbContext { get; set; }
//        private DbSet<Articles> _db;

//        public RepositoryArticles(GameModuleContext dbContext)
//        {
//            this.dbContext = dbContext;
//            this._db = dbContext.Set<Articles>();
//        }

//        public List<ArticleDto> GetList()
//        {
//            var ret = _db.ToList().Select(i => Mapper.Map<ArticleDto>(i)).ToList();
//            return ret;
//        }

//        public async Task<List<ArticleDto>> GetList()
//        {
//            return await Task.Run(() => this.GetList());
//        }

//        public ArticleDto Find(long id)
//        {
//            var entity = _db.FirstOrDefault(i => i.Id == id);
//            return Mapper.Map<ArticleDto>(id);
//        }

//        public async Task<ArticleDto> FindAsync(long id)
//        {
//            var entity = await _db.FirstOrDefault(i => i.Id == id && !i.IsDeleted);
//            return Mapper.Map<ArticleDto>(entity);
//        }

//        public ArticleDto Insert(ArticleDto input)
//        {
//            var entity = Mapper.Map<Articles>(input);
//            entity.CreationTime = DateTime.Now;
//            entity.IsDeleted = false;
//            entity.Id = 0;
//            _db.Add(entity);
//            dbContext.SaveChanges();
//            input.Id = entity.Id;
//            return input;
//        }

//        public async Task<ArticleDto> InsertAsync(ArticleDto input)
//        {
//            return await Task.Run(() => this.Insert(input));
//        }

//        public ArticleDto Update(ArticleDto input)
//        {
//            var exist = _db.FirstOrDefault(i => i.Id == input.Id);
//            if (exist == null)
//                throw new Exception("Does not exitst entity with currentId");

//            var entity = Mapper.Map<Articles>(input);
//            exist.CreationTime = exist.CreationTime;
//            exist.Id = entity.Id;
//            exist.LastModificationTime = DateTime.Now;
//            exist.IsDeleted = entity.IsDeleted;
//            exist.BannerImage = entity.BannerImage;
//            exist.Status = entity.Status;
//            exist.Content = entity.Content;
//            exist.PublicationDate = entity.PublicationDate;
//            exist.Title = entity.Title;
//            exist.TitleImage = entity.TitleImage;
//            exist.IsNews = entity.IsNews;
//            exist.Price = entity.Price;
//            exist.Category = entity.Category;
//            _db.Update(exist);
//            dbContext.SaveChanges();
//            return input;
//        }

//        public async Task<ArticleDto> UpdateAsync(ArticleDto input)
//        {
//            return await Task.Run(() => this.Update(input));
//        }
//    }
//}
