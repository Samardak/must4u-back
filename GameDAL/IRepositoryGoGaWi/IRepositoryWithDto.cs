﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace GameDAL.IRepositoryGoGaWi
{
    public interface IRepository<T,TDto> where T : class
    {
        Task<T> FirstOrDefault(Expression<Func<T, bool>> func);
        Task<TDto> FirstOrDefaultDto(Expression<Func<T, bool>> func);

        Task<List<T>> GetList(Expression<Func<T, bool>> func);
        Task<List<TDto>> GetListDto(Expression<Func<T, bool>> func);

        Task<List<T>> GetList();
        Task<List<TDto>> GetListDto();

        Task<T> Insert(T input);
        Task<TDto> Insert(TDto input);

        Task<T> Update(T input);
       
        IQueryable<T> Queryable { get; }
        DbSet<T> GetDbSet();

        Task SaveShanges();
    }
}
