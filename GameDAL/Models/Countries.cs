﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Countries
    {
        public Countries()
        {
            Cities = new HashSet<Cities>();
            StationRegions = new HashSet<StationRegions>();
            Users = new HashSet<Users>();
        }

        public long Id { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }

        public virtual ICollection<Cities> Cities { get; set; }
        public virtual ICollection<StationRegions> StationRegions { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
