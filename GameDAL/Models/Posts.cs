﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Posts
    {
        public Posts()
        {
            PostComents = new HashSet<PostComents>();
            PostLikes = new HashSet<PostLikes>();
            PostTags = new HashSet<PostTags>();
        }

        public long Id { get; set; }
        public int PostTypeId { get; set; }
        public long UserId { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public int CountLikes { get; set; }
        public int CountDislike { get; set; }
        public int CountComments { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<PostComents> PostComents { get; set; }
        public virtual ICollection<PostLikes> PostLikes { get; set; }
        public virtual ICollection<PostTags> PostTags { get; set; }
        public virtual PostTypes PostType { get; set; }
        public virtual Users User { get; set; }
    }
}
