﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class StationRegions
    {
        public StationRegions()
        {
            Users = new HashSet<Users>();
        }

        public long Id { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public string StationName { get; set; }

        public virtual ICollection<Users> Users { get; set; }
        public virtual Cities City { get; set; }
        public virtual Countries Country { get; set; }
    }
}
