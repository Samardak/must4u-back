﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Users
    {
        public Users()
        {
            AuthDatas = new HashSet<AuthDatas>();
            MapMessagesGroupUsers = new HashSet<MapMessagesGroupUsers>();
            MasterSchedules = new HashSet<MasterSchedules>();
            MasterServices = new HashSet<MasterServices>();
            MasterVisitsMaster = new HashSet<MasterVisits>();
            MasterVisitsUser = new HashSet<MasterVisits>();
            MessageGroups = new HashSet<MessageGroups>();
            MessagesUserIdFromNavigation = new HashSet<Messages>();
            MessagesUserIdToNavigation = new HashSet<Messages>();
            PostComents = new HashSet<PostComents>();
            PostLikes = new HashSet<PostLikes>();
            Posts = new HashSet<Posts>();
            PostTags = new HashSet<PostTags>();
            UserFiendsIniciator = new HashSet<UserFiends>();
            UserFiendsUser1 = new HashSet<UserFiends>();
            UserFiendsUser2 = new HashSet<UserFiends>();
            UserSocialNetworks = new HashSet<UserSocialNetworks>();
        }

        public long Id { get; set; }
        public int UserTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureUrl { get; set; }
        public string Email { get; set; }
        public string Currency { get; set; }
        public long CountryId { get; set; }
        public long CityId { get; set; }
        public long? StationRegionId { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public int WorkingExperience { get; set; }
        public bool IsOnlyHome { get; set; }
        public string AboutText { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsManicure { get; set; }
        public bool IsPedicure { get; set; }

        public virtual ICollection<AuthDatas> AuthDatas { get; set; }
        public virtual ICollection<MapMessagesGroupUsers> MapMessagesGroupUsers { get; set; }
        public virtual ICollection<MasterSchedules> MasterSchedules { get; set; }
        public virtual ICollection<MasterServices> MasterServices { get; set; }
        public virtual ICollection<MasterVisits> MasterVisitsMaster { get; set; }
        public virtual ICollection<MasterVisits> MasterVisitsUser { get; set; }
        public virtual ICollection<MessageGroups> MessageGroups { get; set; }
        public virtual ICollection<Messages> MessagesUserIdFromNavigation { get; set; }
        public virtual ICollection<Messages> MessagesUserIdToNavigation { get; set; }
        public virtual ICollection<PostComents> PostComents { get; set; }
        public virtual ICollection<PostLikes> PostLikes { get; set; }
        public virtual ICollection<Posts> Posts { get; set; }
        public virtual ICollection<PostTags> PostTags { get; set; }
        public virtual ICollection<UserFiends> UserFiendsIniciator { get; set; }
        public virtual ICollection<UserFiends> UserFiendsUser1 { get; set; }
        public virtual ICollection<UserFiends> UserFiendsUser2 { get; set; }
        public virtual ICollection<UserSocialNetworks> UserSocialNetworks { get; set; }
        public virtual Cities City { get; set; }
        public virtual Countries Country { get; set; }
        public virtual StationRegions StationRegion { get; set; }
        public virtual UserTypes UserType { get; set; }
    }
}
