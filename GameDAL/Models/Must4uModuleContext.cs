﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GameDAL.Models
{
    public partial class Must4uModuleContext : DbContext
    {
        public virtual DbSet<AuthDatas> AuthDatas { get; set; }
        public virtual DbSet<AuthTypes> AuthTypes { get; set; }
        public virtual DbSet<Cities> Cities { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<MapMasterVisitPictures> MapMasterVisitPictures { get; set; }
        public virtual DbSet<MapMasterVisitServices> MapMasterVisitServices { get; set; }
        public virtual DbSet<MapMessagesGroupUsers> MapMessagesGroupUsers { get; set; }
        public virtual DbSet<MasterSchedules> MasterSchedules { get; set; }
        public virtual DbSet<MasterServices> MasterServices { get; set; }
        public virtual DbSet<MasterVisits> MasterVisits { get; set; }
        public virtual DbSet<MessageGroups> MessageGroups { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<Must4uTestTable> Must4uTestTable { get; set; }
        public virtual DbSet<Pictures> Pictures { get; set; }
        public virtual DbSet<PostComents> PostComents { get; set; }
        public virtual DbSet<PostLikes> PostLikes { get; set; }
        public virtual DbSet<PostTags> PostTags { get; set; }
        public virtual DbSet<PostTypes> PostTypes { get; set; }
        public virtual DbSet<Posts> Posts { get; set; }
        public virtual DbSet<ServicesType> ServicesType { get; set; }
        public virtual DbSet<StationRegions> StationRegions { get; set; }
        public virtual DbSet<UserFiends> UserFiends { get; set; }
        public virtual DbSet<UserSocialNetworks> UserSocialNetworks { get; set; }
        public virtual DbSet<UserTypes> UserTypes { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        // Unable to generate entity type for table 'dbo.VersionInfo'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            #warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            optionsBuilder.UseSqlServer(@"Server=94.249.192.145; Database=Must4uModule; Trusted_Connection=False; uid=sa; pwd=jpkvpsbnpsgtzmipm;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthDatas>(entity =>
            {
                entity.Property(e => e.ExternalId)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AuthDatas)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AuthDatas_Users");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.AuthDatas)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_AuthDatas_AuthTypes");
            });

            modelBuilder.Entity<AuthTypes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Discription)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Cities>(entity =>
            {
                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Cities)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Cities_Countries");
            });

            modelBuilder.Entity<Countries>(entity =>
            {
                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<MapMasterVisitPictures>(entity =>
            {
                entity.HasOne(d => d.Picuture)
                    .WithMany(p => p.MapMasterVisitPictures)
                    .HasForeignKey(d => d.PicutureId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MapMasterVisitPictures_Pictures");
            });

            modelBuilder.Entity<MapMasterVisitServices>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.MapMasterVisitServices)
                    .HasForeignKey<MapMasterVisitServices>(d => d.Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MapMasterVisitServices_MasterVisits");

                entity.HasOne(d => d.ServiceType)
                    .WithMany(p => p.MapMasterVisitServices)
                    .HasForeignKey(d => d.ServiceTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MapMasterVisitServices_ServicesType");
            });

            modelBuilder.Entity<MapMessagesGroupUsers>(entity =>
            {
                entity.HasOne(d => d.Group)
                    .WithMany(p => p.MapMessagesGroupUsers)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MapMessagesGroupUsers_MessageGroups");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MapMessagesGroupUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MapMessagesGroupUsers_Users");
            });

            modelBuilder.Entity<MasterSchedules>(entity =>
            {
                entity.Property(e => e.Nday).HasColumnName("NDay");

                entity.Property(e => e.Nmonth).HasColumnName("NMonth");

                entity.Property(e => e.Nyear).HasColumnName("NYear");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MasterSchedules)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MasterSchedule_Users");
            });

            modelBuilder.Entity<MasterServices>(entity =>
            {
                entity.HasOne(d => d.ServicesType)
                    .WithMany(p => p.MasterServices)
                    .HasForeignKey(d => d.ServicesTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MasterServices_ServicesType");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MasterServices)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MasterServices_Users");
            });

            modelBuilder.Entity<MasterVisits>(entity =>
            {
                entity.Property(e => e.Nickname).HasMaxLength(200);

                entity.HasOne(d => d.Master)
                    .WithMany(p => p.MasterVisitsMaster)
                    .HasForeignKey(d => d.MasterId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MasterVisits_Users1");

                entity.HasOne(d => d.MasterSchedule)
                    .WithMany(p => p.MasterVisits)
                    .HasForeignKey(d => d.MasterScheduleId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MasterVisits_MasterSchedules");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.MasterVisitsUser)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_MasterVisits_Users");
            });

            modelBuilder.Entity<MessageGroups>(entity =>
            {
                entity.Property(e => e.GroupId)
                    .IsRequired()
                    .HasColumnType("varchar(100)");

                entity.HasOne(d => d.Iniciator)
                    .WithMany(p => p.MessageGroups)
                    .HasForeignKey(d => d.IniciatorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_MessageGroups_Users");
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.Messages)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Messsages_MessageGroups");

                entity.HasOne(d => d.UserIdFromNavigation)
                    .WithMany(p => p.MessagesUserIdFromNavigation)
                    .HasForeignKey(d => d.UserIdFrom)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Messsages_Users");

                entity.HasOne(d => d.UserIdToNavigation)
                    .WithMany(p => p.MessagesUserIdToNavigation)
                    .HasForeignKey(d => d.UserIdTo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Messsages_Users1");
            });

            modelBuilder.Entity<Must4uTestTable>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Col1).IsRequired();
            });

            modelBuilder.Entity<PostComents>(entity =>
            {
                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.Creator)
                    .WithMany(p => p.PostComents)
                    .HasForeignKey(d => d.CreatorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostComents_Users");

                entity.HasOne(d => d.Post)
                    .WithMany(p => p.PostComents)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostComents_Posts");
            });

            modelBuilder.Entity<PostLikes>(entity =>
            {
                entity.HasOne(d => d.Post)
                    .WithMany(p => p.PostLikes)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostLikes_Posts");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PostLikes)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostLikes_Users");
            });

            modelBuilder.Entity<PostTags>(entity =>
            {
                entity.HasOne(d => d.Post)
                    .WithMany(p => p.PostTags)
                    .HasForeignKey(d => d.PostId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostTags_Posts");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PostTags)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_PostTags_Users");
            });

            modelBuilder.Entity<PostTypes>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<Posts>(entity =>
            {
                entity.Property(e => e.PictureUrl)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.HasOne(d => d.PostType)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.PostTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Posts_PostTypes");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Posts_Users");
            });

            modelBuilder.Entity<ServicesType>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Discription)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(500);
            });

            modelBuilder.Entity<StationRegions>(entity =>
            {
                entity.Property(e => e.StationName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.StationRegions)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StationRegions_Cities");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.StationRegions)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_StationRegions_Countries");
            });

            modelBuilder.Entity<UserFiends>(entity =>
            {
                entity.HasOne(d => d.Iniciator)
                    .WithMany(p => p.UserFiendsIniciator)
                    .HasForeignKey(d => d.IniciatorId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserFiends_Users2");

                entity.HasOne(d => d.User1)
                    .WithMany(p => p.UserFiendsUser1)
                    .HasForeignKey(d => d.User1Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserFiends_Users");

                entity.HasOne(d => d.User2)
                    .WithMany(p => p.UserFiendsUser2)
                    .HasForeignKey(d => d.User2Id)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserFiends_Users1");
            });

            modelBuilder.Entity<UserSocialNetworks>(entity =>
            {
                entity.Property(e => e.Facebook).HasMaxLength(1000);

                entity.Property(e => e.Instagram).HasMaxLength(1000);

                entity.Property(e => e.Vk).HasMaxLength(1000);

                entity.Property(e => e.Youtube).HasMaxLength(1000);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserSocialNetworks)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_UserSocialNetworks_Users");
            });

            modelBuilder.Entity<UserTypes>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.AboutText).HasMaxLength(1000);

                entity.Property(e => e.Address).HasMaxLength(500);

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Latitude).HasColumnType("decimal");

                entity.Property(e => e.Longitude).HasColumnType("decimal");

                entity.Property(e => e.Phone).HasMaxLength(500);

                entity.Property(e => e.PictureUrl)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Users_Cities");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Users_Countries");

                entity.HasOne(d => d.StationRegion)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.StationRegionId)
                    .HasConstraintName("FK_Users_StationRegions");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Users_UserTypes");
            });
        }
    }
}