﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class PostTags
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public long UserId { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsDeleted { get; set; }

        public virtual Posts Post { get; set; }
        public virtual Users User { get; set; }
    }
}
