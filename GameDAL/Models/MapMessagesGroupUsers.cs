﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MapMessagesGroupUsers
    {
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long UserId { get; set; }
        public DateTimeOffset LastTimeView { get; set; }

        public virtual MessageGroups Group { get; set; }
        public virtual Users User { get; set; }
    }
}
