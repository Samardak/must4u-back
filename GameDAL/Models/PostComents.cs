﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class PostComents
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public long CreatorId { get; set; }
        public string Text { get; set; }
        public DateTimeOffset DateCreation { get; set; }

        public virtual Users Creator { get; set; }
        public virtual Posts Post { get; set; }
    }
}
