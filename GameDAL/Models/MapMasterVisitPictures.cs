﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MapMasterVisitPictures
    {
        public long Id { get; set; }
        public long MasterVisitId { get; set; }
        public long PicutureId { get; set; }

        public virtual Pictures Picuture { get; set; }
    }
}
