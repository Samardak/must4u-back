﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class ServicesType
    {
        public ServicesType()
        {
            MapMasterVisitServices = new HashSet<MapMasterVisitServices>();
            MasterServices = new HashSet<MasterServices>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }

        public virtual ICollection<MapMasterVisitServices> MapMasterVisitServices { get; set; }
        public virtual ICollection<MasterServices> MasterServices { get; set; }
    }
}
