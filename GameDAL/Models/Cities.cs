﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Cities
    {
        public Cities()
        {
            StationRegions = new HashSet<StationRegions>();
            Users = new HashSet<Users>();
        }

        public long Id { get; set; }
        public long CountryId { get; set; }
        public string City { get; set; }

        public virtual ICollection<StationRegions> StationRegions { get; set; }
        public virtual ICollection<Users> Users { get; set; }
        public virtual Countries Country { get; set; }
    }
}
