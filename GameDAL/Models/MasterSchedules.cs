﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MasterSchedules
    {
        public MasterSchedules()
        {
            MasterVisits = new HashSet<MasterVisits>();
        }

        public long Id { get; set; }
        public long UserId { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public bool IsWeekly { get; set; }
        public long DayOfWeek { get; set; }
        public int Nyear { get; set; }
        public int Nmonth { get; set; }
        public int Nday { get; set; }
        public bool IsFreeDay { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<MasterVisits> MasterVisits { get; set; }
        public virtual Users User { get; set; }
    }
}
