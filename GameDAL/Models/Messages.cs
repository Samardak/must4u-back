﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Messages
    {
        public long Id { get; set; }
        public long GroupId { get; set; }
        public long UserIdFrom { get; set; }
        public long UserIdTo { get; set; }
        public string Text { get; set; }
        public bool IsRead { get; set; }
        public DateTimeOffset DateCreation { get; set; }

        public virtual MessageGroups Group { get; set; }
        public virtual Users UserIdFromNavigation { get; set; }
        public virtual Users UserIdToNavigation { get; set; }
    }
}
