﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class UserFiends
    {
        public long Id { get; set; }
        public long User1Id { get; set; }
        public long User2Id { get; set; }
        public long IniciatorId { get; set; }
        public bool IsConfirm { get; set; }
        public bool IsIgnore { get; set; }
        public bool IdDeleted { get; set; }
        public DateTimeOffset DateCreation { get; set; }

        public virtual Users Iniciator { get; set; }
        public virtual Users User1 { get; set; }
        public virtual Users User2 { get; set; }
    }
}
