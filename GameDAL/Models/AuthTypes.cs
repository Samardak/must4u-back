﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class AuthTypes
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
    }
}
