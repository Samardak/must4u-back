﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class AuthDatas
    {
        public long Id { get; set; }
        public int AuthTypeId { get; set; }
        public long UserId { get; set; }
        public int UserTypeId { get; set; }
        public string ExternalId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public virtual Users User { get; set; }
        public virtual UserTypes UserType { get; set; }
    }
}
