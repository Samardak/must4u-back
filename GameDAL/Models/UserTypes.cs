﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class UserTypes
    {
        public UserTypes()
        {
            AuthDatas = new HashSet<AuthDatas>();
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AuthDatas> AuthDatas { get; set; }
        public virtual ICollection<Users> Users { get; set; }
    }
}
