﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MasterVisits
    {
        public long Id { get; set; }
        public long? UserId { get; set; }
        public string Nickname { get; set; }
        public long MasterId { get; set; }
        public long MasterScheduleId { get; set; }
        public bool IsMasterConfirm { get; set; }
        public bool IsUserConfirm { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IdDeleted { get; set; }

        public virtual MapMasterVisitServices MapMasterVisitServices { get; set; }
        public virtual Users Master { get; set; }
        public virtual MasterSchedules MasterSchedule { get; set; }
        public virtual Users User { get; set; }
    }
}
