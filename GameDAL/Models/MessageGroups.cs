﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MessageGroups
    {
        public MessageGroups()
        {
            MapMessagesGroupUsers = new HashSet<MapMessagesGroupUsers>();
            Messages = new HashSet<Messages>();
        }

        public long Id { get; set; }
        public string GroupId { get; set; }
        public long IniciatorId { get; set; }

        public virtual ICollection<MapMessagesGroupUsers> MapMessagesGroupUsers { get; set; }
        public virtual ICollection<Messages> Messages { get; set; }
        public virtual Users Iniciator { get; set; }
    }
}
