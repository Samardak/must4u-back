﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class Pictures
    {
        public Pictures()
        {
            MapMasterVisitPictures = new HashSet<MapMasterVisitPictures>();
        }

        public long Id { get; set; }
        public string FullUrl { get; set; }
        public string Url { get; set; }

        public virtual ICollection<MapMasterVisitPictures> MapMasterVisitPictures { get; set; }
    }
}
