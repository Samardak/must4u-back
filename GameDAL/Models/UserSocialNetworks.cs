﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class UserSocialNetworks
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string Vk { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public string Youtube { get; set; }

        public virtual Users User { get; set; }
    }
}
