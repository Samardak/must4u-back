﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MasterServices
    {
        public long Id { get; set; }
        public int ServicesTypeId { get; set; }
        public long UserId { get; set; }
        public int DuringTimeMinutes { get; set; }
        public int Price { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset DateCreation { get; set; }

        public virtual ServicesType ServicesType { get; set; }
        public virtual Users User { get; set; }
    }
}
