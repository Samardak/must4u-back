﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class MapMasterVisitServices
    {
        public long Id { get; set; }
        public long MasterVisitId { get; set; }
        public int ServiceTypeId { get; set; }
        public bool IsDeleted { get; set; }

        public virtual MasterVisits IdNavigation { get; set; }
        public virtual ServicesType ServiceType { get; set; }
    }
}
