﻿using System;
using System.Collections.Generic;

namespace GameDAL.Models
{
    public partial class PostTypes
    {
        public PostTypes()
        {
            Posts = new HashSet<Posts>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Posts> Posts { get; set; }
    }
}
