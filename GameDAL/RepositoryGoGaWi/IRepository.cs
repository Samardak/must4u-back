﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.EntityFrameworkCore;

namespace GameDAL.RepositoryGoGaWi
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly Must4uModuleContext _context;
        protected DbSet<T> _db;

        public Repository(Must4uModuleContext context)
        {
            _context = context;
            _db = context.Set<T>();
        }

        public async Task<T> FirstOrDefault(Expression<Func<T, bool>> func)
        {
            return await _db.FirstOrDefaultAsync(func);
        }

        public async Task<List<T>> GetList(Expression<Func<T, bool>> func)
        {
            return await _db.Where(func).ToListAsync();
        }

        public async Task<List<T>> GetList()
        {
            return await _db.ToListAsync();
        }

        public async Task<T> Insert(T input)
        {
            _db.Add(input);
            await _context.SaveChangesAsync();
            return input;
        }

        public async Task<T> Update(T input)
        {
            _db.Update(input);
            await _context.SaveChangesAsync();
            return input;
        }

        public IQueryable<T> Queryable => _db.AsQueryable();
        public DbSet<T> GetDbSet()
        {
            return _db;
        }

        public async Task SaveShanges()
        {
            await _context.SaveChangesAsync();
        }
    }
}
