﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Game.Tests.BLL.Services;
using GameBLL.Service;
using GameBLL.Service.InternalService;
using GameBLL.Settings;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.Game.GameTokenService;
using PlatformCore.Common.Game.InputOutput.IRegistrationService;
using PlatformCore.Common.PlatformException;
using PlatformCore.Common.RequestHelper;
using PlatformCore.DAL.Common.CacheService;
using Shouldly;
using Xunit;
using PlatformCore.Application.Common.Extention;

namespace Game.Tests
{
    public class GameTestBase
    {
        private ServiceCollection Services { get; set; }

        private IAuthService _authService;
        public IServiceProvider ServiceProvider { get; set; }
        public GameTestBase()
        {
            Services = new ServiceCollection();
            #region service db repositoryWithDto
            //http://6figuredev.com/technology/generic-repositoryWithDto-dependency-injection-with-net-core/
            Services.AddDbContext<GameDAL.Models.Must4uModuleContext>();

            Services.AddScoped(typeof(IRepository<>), typeof(RepositoryMock<>));
            Services.AddScoped(typeof(IRepository<,>), typeof(RepositoryMock<,>));

            //Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            //Services.AddScoped(typeof(IRepository<,>), typeof(Repository<,>));






            var item = Options.Create<AppSettings>(
                new AppSettings
                {
                    FullPathToFileLog = "",
                    AccountingUrl = $"http://172.16.45.166:3331/api/",
                    ClientUrl = $"http://172.16.45.166:3332/api/",
                    ThirdPartyAuthWebApiUrl = $"http://172.16.45.166:3325/api/",
                    UrlConfirmation = "",
                    ProxyPlayerUrl = "http://172.16.45.166:3329/api/",
                    UrlForgotPassword = "UrlForgotPassword",

                });
            Services.AddSingleton(provider => item);

            

            

            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));

            //Services.AddScoped(typeof(IRepositoryTransactions), typeof(RepositoryTransactions));
            //Services.AddScoped(typeof(IRepositoryTransactionDocuments), typeof(RepositoryTransactionDocuments));
            //Services.AddScoped(typeof(IRepositoryFinancialTypeMappings), typeof(RepositoryFinancialTypeMappings));


            //Services.AddScoped(typeof(IRepositoryWithDto<>), typeof(Repository<>));
            #endregion service db repositoryWithDto


            ConfigureService();
            ConfigurateAutomapper();



            #region redis DI
            Services.AddSingleton<IСacheService>(serviceProvider => new СacheService(
                    //$"{ConnectionConfig.ServerAddress}:6379,allowAdmin=true",
                    $"localhost:6379,allowAdmin=true",
                    1)
                );
            #endregion


            ServiceProvider = Services.BuildServiceProvider();
            _authService = ServiceProvider.GetRequiredService<IAuthService>();




            var repos = ServiceProvider.GetRequiredService<IRepository<ServicesType>>();
            /*
            
            Id	Name	Discription
            1	Маникюр	Маникюр
            2	Снятие 	Снятие
            3	Френч	Френч
            4	Лунный	Лунный
            5	Покрытие однотон	Покрытие однотон
            6	Парафинотерапия	Парафинотерапия

             */
            #region insert service type
            repos.Insert(new ServicesType
            {
                Id = 1,
                Name = "Маникюр",
                Discription = "Маникюр",
            });
            repos.Insert(new ServicesType
            {
                Id = 2,
                Name = "Снятие",
                Discription = "Снятие",
            });
            repos.Insert(new ServicesType
            {
                Id = 3,
                Name = "Френч",
                Discription = "Френч",
            });
            repos.Insert(new ServicesType
            {
                Id = 4,
                Name = "Лунный",
                Discription = "Лунный",
            });
            repos.Insert(new ServicesType
            {
                Id = 5,
                Name = "Покрытие однотон",
                Discription = "Покрытие однотон",
            });
            repos.Insert(new ServicesType
            {
                Id = 6,
                Name = "Парафинотерапия",
                Discription = "Парафинотерапия",
            });
            #endregion insert service type
        }

        private void ConfigureService()
        {
            //Services.AddScoped(typeof (IAccountingServiceApp), typeof (AccountingServiceApp));
            //Services.AddScoped(typeof(ITransactionServiceApp), typeof(TransactionServiceApp));

            //Services.AddScoped(typeof(ITransactionService), typeof(TransactionService));


            Services.AddScoped(typeof(IRequestHelper), typeof(RequestHelper));
            Services.AddScoped(typeof(ITokenService), typeof(TokenService));
            Services.AddScoped(typeof(ISessionService), typeof(SessionService));
            Services.AddScoped(typeof(IEmailSenderService), typeof(EmailSenderServiceService));



            
            Services.AddScoped(typeof(IDateConvertorServiceInternal), typeof(DateConvertorServiceInternal));
            Services.AddScoped(typeof(IAuthService), typeof(AuthService));
            Services.AddScoped(typeof(IMasterScheduleService), typeof(MasterScheduleService));

            Services.AddScoped(typeof(IPostService), typeof(PostService));
            Services.AddScoped(typeof(PostService));

            Services.AddScoped(typeof(ISkipCountCheckService), typeof(SkipCountCheckService));
            Services.AddScoped(typeof(IFriendService), typeof(FriendService));

            
            Services.AddScoped(typeof(IProfileService),typeof(ProfileService));

            Services.AddScoped(typeof(INotificationService),typeof(NotificationService));

            Services.AddScoped(typeof(ProfileService));
            Services.AddScoped(typeof(MessagesService));
            Services.AddScoped(typeof(VisitMasterService));
            Services.AddScoped(typeof(PictureService));
            Services.AddScoped(typeof(IPictureService), typeof(PictureService));

            Services.AddScoped(typeof(IProfileMasterService), typeof(ProfileMasterService));


            Services.AddScoped(typeof(ISearchService), typeof(SearchService));
            Services.AddScoped(typeof(SearchService));
            Services.AddScoped(typeof(LocationService));





        }
        private void ConfigurateAutomapper()
        {
            Mapper.Initialize(config =>
            {

                //config.CreateMap<TournamentGames, TournamentGameDto>().ReverseMap();
                config.CreateMap<AuthDatas, AuthDatasDTO>().ReverseMap();
                config.CreateMap<AuthTypes, AuthTypesDTO>().ReverseMap();
                config.CreateMap<Cities, CitiesDTO>().ReverseMap();
                config.CreateMap<Countries, CountriesDTO>().ReverseMap();
                config.CreateMap<StationRegions, StationRegionsDTO>().ReverseMap();
                config.CreateMap<Users, UsersDTO>().ReverseMap();
                config.CreateMap<ServicesType, ServicesTypeDTO>().ReverseMap();
                config.CreateMap<MasterServices, MasterServicesDTO>().ReverseMap();
                config.CreateMap<UserSocialNetworks, UserSocialNetworksDTO>().ReverseMap();
                config.CreateMap<Posts, PostsDTO>().ReverseMap();
                config.CreateMap<PostComents, PostComentsDTO>().ReverseMap();
                config.CreateMap<PostLikes, PostLikesDTO>().ReverseMap();
                config.CreateMap<PostTags, PostTagsDTO>().ReverseMap();


                config.CreateMap<MapMessagesGroupUsers, MapMessagesGroupUsersDTO>().ReverseMap();
                config.CreateMap<MessageGroups, MessageGroupsDTO>().ReverseMap();
                config.CreateMap<Messages, MessagesDTO>().ReverseMap();

                config.CreateMap<MasterServices, MasterServicesDTO>().ReverseMap();

                config.CreateMap<PicturesDTO, Pictures>().ReverseMap();


            });
        }


        [Fact]
        public async Task Test()
        {
            var session = await this.RegisterAndLoginMaster();
        }

        protected async Task<LoginOutput>  RegisterAndLoginMaster()
        {
            return await _authService.RegisterAndLogin(new RegisterInput
            {
                Email = Guid.NewGuid().ToString(),
                FirstName = Guid.NewGuid().ToString(),
                LastName = Guid.NewGuid().ToString(),
                Password = Guid.NewGuid().ToString(),
                UserType = EnumUserTypes.Master
            });
        }

        protected async Task<LoginOutput> RegisterAndLoginClient()
        {
            return await _authService.RegisterAndLogin(new RegisterInput
            {
                Email = Guid.NewGuid().ToString(),
                FirstName = Guid.NewGuid().ToString(),
                LastName = Guid.NewGuid().ToString(),
                Password = Guid.NewGuid().ToString(),
                UserType = EnumUserTypes.Client
            });
        }
    }
    

    public class X
    {
        [Fact]
        public void Xr()
        {
            var r = "123 "
                .Then<string, string>(x => x.Trim)
                .Then(Convert.ToInt32);
            r.ShouldBe(123);

            if(string.IsNullOrEmpty("1")) throw new BaseException();

            
            "1".DoIf(string.IsNullOrEmpty, s => throw new BaseException());

            var check = true;
            if (check)
                "x".Trim();
            else
                "x".ToUpper();
                    
            true.IfDo(() =>  check = false, () => { });
        }
    }
}