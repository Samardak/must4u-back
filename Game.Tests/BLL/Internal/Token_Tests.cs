﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.Game.GameTokenService;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Internal
{
    public class Token_Tests : GameTestBase
    {
        private readonly ITokenService _tokenService;

        public Token_Tests()
        {
            _tokenService = base.ServiceProvider.GetRequiredService<ITokenService>();
        }

        [Fact]
        public void Test()
        {
            long customerId = 1;
            var str = _tokenService.GetNewToken(customerId);
            long custId;
            _tokenService.GetUserIddbyToken(str, out custId);
            customerId.ShouldBe(custId);
        }
    }
}
