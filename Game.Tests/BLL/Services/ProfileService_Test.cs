using System;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class ProfileService_Test : GameTestBase
    {
        private readonly IProfileService _profileService;

        public ProfileService_Test()
        {
            _profileService = base.ServiceProvider.GetRequiredService<IProfileService>();
        }

        [Fact]
        public async Task X()
        {
            var session = await base.RegisterAndLoginMaster();

            var resp = await _profileService.ChangePicture(new ChangePictureInput
            {
                Token = session.Token,
                PictureUrl = Guid.NewGuid().ToString()
            });
            resp.ShouldNotBeNull();
            resp.PictureOut.ShouldNotBeNull();
        }
    }
}