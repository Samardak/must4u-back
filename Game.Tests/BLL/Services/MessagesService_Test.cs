using System;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.BaseInputOutput;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class MessagesService_Test : GameTestBase
    {
        private readonly MessagesService _messagesService;

        public MessagesService_Test()
        {
            _messagesService = base.ServiceProvider.GetRequiredService<MessagesService>();
        }




        [Fact]
        public async Task SendMessage_Should_Success()
        {
            var client1 = await base.RegisterAndLoginClient();
            var client2 = await base.RegisterAndLoginClient();

            var result = await _messagesService.SendMessage(new SendMessageInput
            {
                Id = client2.UserId,
                Token = client1.Token,
                Text = "Success"
            });
            result.MessageId.ShouldBeGreaterThan(0);
            result.Text.ShouldNotBeNull();

            var messages = await _messagesService.GetMessagesWithUserId(new GetMessagesWithUserId
            {
                UserId = client1.UserId,
                Token = client2.Token,
            });
            messages.ShouldNotBeNull();
            messages.Count.ShouldBeGreaterThan(0);
            messages.FirstOrDefault().PictureUrl.ShouldNotBeNull();
            messages.FirstOrDefault().Text.ShouldNotBeNull();
        }


        [Fact]
        public async Task GetAllMessageGroup_Should_Pass()
        {
            var client1 = await base.RegisterAndLoginClient();
            var client2 = await base.RegisterAndLoginClient();
            var client3 = await base.RegisterAndLoginClient();


            #region create messages
            await _messagesService.SendMessage(new SendMessageInput
            {
                Id = client2.UserId,
                Token = client1.Token,
                Text = "Text1"
            });

            await _messagesService.SendMessage(new SendMessageInput
            {
                Id = client3.UserId,
                Token = client1.Token,
                Text = "Text2"
            });

            await _messagesService.SendMessage(new SendMessageInput
            {
                Id = client3.UserId,
                Token = client1.Token,
                Text = "Text3"
            });
            #endregion create messages

            var result = await _messagesService.GetAllMessageGroup(new BaseSkipCountInput
            {
                Token = client1.Token,
            });

            //result.Count.ShouldBe(2);
            //var first = result[0];
            //first.Text.ShouldBe("Text3");

            ////var second = result[1];
            ////second.Text.ShouldBe("Text1");

            //result = await _messagesService.GetAllMessageGroup(new BaseSkipCountInput
            //{
            //    Token = client1.Token,
            //    Count = 1,
            //    Skip = 1
            //});
            //result.Count.ShouldBe(1);
            //result.FirstOrDefault().Text.ShouldBe("Text1");

        }


        [Fact]
        public void GetGroupId_GetUsersId_Should_Success()
        {
            //arange
            long userId1 = 100;
            long userId2 = 12;
            //act

            var groupId = _messagesService.GetGroupId(userId1, userId2);
            groupId.ShouldBe(userId2.ToString() + userId1.ToString() + userId2.ToString().Length.ToString());
            //assert

            var usersId = _messagesService.GetUsersId(groupId);
            usersId.userId1.ShouldBe(userId2);
            usersId.userId2.ShouldBe(userId1);


            //arange
            long userId11 = 12;
            long userId22 = 100;
            //act

            var group1Id = _messagesService.GetGroupId(userId11, userId22);
            group1Id.ShouldBe(userId11.ToString() + userId22.ToString() + userId11.ToString().Length.ToString());
            //assert

            var users1Id = _messagesService.GetUsersId(group1Id);
            users1Id.userId1.ShouldBe(userId11);
            users1Id.userId2.ShouldBe(userId22);
        }
    }
}