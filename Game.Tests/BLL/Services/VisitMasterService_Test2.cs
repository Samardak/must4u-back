using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class VisitMasterService_Test : GameTestBase
    {
        private readonly VisitMasterService _visitMasterService;
        private readonly IMasterScheduleService _masterScheduleService;
        private readonly IDateConvertorServiceInternal _convertorServiceInternal;
        private readonly IProfileMasterService _profileMasterService;

        public VisitMasterService_Test()
        {
            _masterScheduleService = base.ServiceProvider.GetRequiredService<IMasterScheduleService>();
            _convertorServiceInternal = base.ServiceProvider.GetRequiredService<IDateConvertorServiceInternal>();
            _visitMasterService = base.ServiceProvider.GetRequiredService<VisitMasterService>();
            _profileMasterService= base.ServiceProvider.GetRequiredService<IProfileMasterService>();
        }


        [Fact]
        public async Task GetVisits_Test()
        {
            
            var master = await base.RegisterAndLoginMaster();
            var countVisits = await _visitMasterService.GetVisits(new GetVisitsInput
            {
                Id = master.UserId,
                Token = master.Token,
                DateTimeStart = _convertorServiceInternal.ConvertToMs(new DateTimeOffset(new DateTime(2017, 5, 1,1,1,1)))
            });

            countVisits.Services.Count.ShouldBe(0);
            countVisits.MasterTimes.FirstOrDefault().MasterTimes.Count.ShouldBe(0);
        }

        //[Fact]
        //public async Task X()
        //{
        //    var client = await base.RegisterAndLoginClient();
        //    var master = await base.RegisterAndLoginMaster();

        //    await _profileMasterService.SetProfileServices(new SetProfileService
        //    {
        //        Token = master.Token,
        //        MyServices = new List<MasterServicesOut>
        //        {
        //            new MasterServicesOut
        //            {
        //                DuringTimeMinutes = 10,
        //                Price = 10,
        //                ServicesTypeId = 1,
        //            }
        //        }
        //    });


        //    #region arrange
        //    await _masterScheduleService.SetDayScheduleCurrentDay(new SetDayScheduleInput
        //    {
        //        Token = master.Token,
        //        Year = 2017,
        //        Month = 5,
        //        Day = 1,
        //        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
        //        {
        //            new MasterScheduleDayOfWeeksInputOutput
        //            {
        //                StartTime = new TimeSpan(10, 0, 0),
        //                EndTime = new TimeSpan(11, 0, 0)
        //            }
        //        }
        //    });

        //    var dt = new DateTimeOffset(new DateTime(2017, 5, 1, 1, 1, 1));
        //    var timeStart = _convertorServiceInternal.ConvertToMs(dt);
        //    GetVisitsOutput visits = await _visitMasterService.GetVisits(new GetVisitsInput
        //    {
        //        Token = client.Token,
        //        Id = master.UserId,
        //        DateTimeStart = timeStart
        //    });
        //    visits.MasterTimes.Count.ShouldBeGreaterThan(0);
        //    MasterTimes firstVisits = visits.MasterTimes.FirstOrDefault().MasterTimes.FirstOrDefault();
        //    firstVisits.IsFree.ShouldBe(true);
        //    firstVisits.Id.ShouldBeGreaterThan(0);
        //    #endregion arrange



        //    #region act
        //    await _visitMasterService.ClientSetVisitToMaster(new SetVisitsInput
        //    {
        //        Token = client.Token,
        //        MasterScheduleId = firstVisits.Id,
        //        MasterServiceTypes = new List<int> { 1 },
        //        Pictures = new List<string> { "http://picture.com?id=123" }
        //    });
        //    #endregion act

        //    #region assert
        //    visits = await _visitMasterService.GetVisits(new GetVisitsInput
        //    {
        //        Token = client.Token,
        //        Id = master.UserId,
        //        DateTimeStart = timeStart
        //    });
        //    firstVisits = visits.MasterTimes.FirstOrDefault().MasterTimes.FirstOrDefault();
        //    firstVisits.IsFree.ShouldBe(false);
        //    #endregion assert
        //}
    }
}