using System;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.PlatformException;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class PostService_Test : GameTestBase
    {

        private readonly PostService _postService;
        private readonly IRepository<PostLikes, PostLikesDTO> _repoPostLike;
        private readonly IRepository<PostTags> _repoPostTag;

        public PostService_Test()
        {
            _repoPostTag = base.ServiceProvider.GetRequiredService<IRepository<PostTags>>(); ;
            _repoPostLike = base.ServiceProvider.GetRequiredService<IRepository<PostLikes, PostLikesDTO>>();
            _postService = base.ServiceProvider.GetRequiredService<PostService>();
        }

        [Fact]
        public async Task CreatePost_GetPostsByUserId()
        {
            var session = await base.RegisterAndLoginMaster();

            #region create posts
            var resp = await _postService.CreatePost(new CreatePostInput
            {
                PictureUrl = "123",
                Token = session.Token
            });
            resp.Id.ShouldBeGreaterThan(0);
            resp.DateCreation.ShouldBeGreaterThan(DateTimeOffset.Now.AddDays(-1));
            resp.PictureUrl.ShouldNotBeNull();
            var resp1 = await _postService.CreatePost(new CreatePostInput
            {
                PictureUrl = "1234",
                Token = session.Token
            });
            resp1.Id.ShouldBeGreaterThan(0);
            resp1.DateCreation.ShouldBeGreaterThan(DateTimeOffset.Now.AddDays(-1));
            resp1.PictureUrl.ShouldNotBeNull();
            #endregion create posts

            var myPosts = await _postService.GetPostsByUserId(new GetPostsByUserId
            {
                Token = session.Token,
                UserId = session.UserId,
                Count = 0,
                Skip = 0
            });
            myPosts.Count.ShouldBeGreaterThan(0);

            var postDto = await _postService.SetImpression(new SetImpressionInp
            {
                PostId = myPosts.FirstOrDefault().Id,
                IsLike = true,
                Token = session.Token
            });
            postDto.CountLikes.ShouldBe(1);
            var all = await _repoPostLike.FirstOrDefault(i=> i.UserId == session.UserId && i.PostId == postDto.Id);
            all.PostId.ShouldBeGreaterThan(0);
            all.Id.ShouldBeGreaterThan(0);
            all.UserId.ShouldBe(session.UserId);
            all.IsLike.ShouldBe(true);
            

            all = await _repoPostLike.FirstOrDefault(i => i.UserId == session.UserId && i.PostId == postDto.Id);
            var post = await _postService.SetImpression(new SetImpressionInp
            {
                PostId = myPosts.FirstOrDefault().Id,
                IsLike = false,
                Token = session.Token
            });
            post.CountLikes.ShouldBe(0);
            post.CountDislike.ShouldBe(1);
            all.IsLike.ShouldBe(false);
        }

        [Fact]
        public async Task CreatePost_Pass()
        {
            var sessionMaster = await base.RegisterAndLoginMaster();
            var sessionClient = await base.RegisterAndLoginClient();

            var postId = await _postService.CreatePost(new CreatePostInput
            {
                Token = sessionMaster.Token,
                IsAd = false,
                PictureUrl = Guid.NewGuid().ToString()
            });

            var respOut = await _postService.CreatePostComment(new CreatePostCommentInp
            {
                Token = sessionClient.Token,
                PostId = postId.Id,
                PostText = Guid.NewGuid().ToString()
            });
            respOut.ShouldNotBeNull();
            respOut.CommentId.ShouldBeGreaterThan(0);


            #region post teg some user
            await _postService.TagUser(new TagUserInput
            {
                UserId = sessionClient.UserId,
                Token = sessionMaster.Token,
                PostId = respOut.PostId
            });
            var postTag = await _repoPostTag.FirstOrDefault(i => !i.IsDeleted &&
                                                i.UserId == sessionClient.UserId &&
                                                i.PostId == respOut.PostId);
            postTag.ShouldNotBeNull();
            #endregion post teg some user

            #region post tag user incorrect user
            BaseException exeption = null;
            try
            {
                await _postService.TagUser(new TagUserInput
                {
                    UserId = -sessionClient.UserId,
                    Token = sessionMaster.Token,
                    PostId = respOut.PostId
                });
            }
            catch (Exception ex)
            {
                exeption = ex as BaseException;
            }
            exeption.ShouldNotBeNull();
            exeption.Code.ShouldBe(CodeError.DoesNotExistUser);
            #endregion post tag user incorrect user



        }



    }
}