using System;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.BaseInputOutput;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.PlatformException;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class FriendService_Test : GameTestBase
    {
        private readonly IFriendService _friendService;

        public FriendService_Test()
        {
            _friendService = base.ServiceProvider.GetRequiredService<IFriendService>();
        }

        [Fact]
        public async Task GetFriends()
        {
            var session = await base.RegisterAndLoginMaster();
            var myFriend = await _friendService.GetFriends(new GetFriensInp { Token = session.Token});
        }


        [Fact]
        public async Task InviteFriendAndGetInvite()
        {
            var session = await base.RegisterAndLoginMaster();
            var session2 = await base.RegisterAndLoginMaster();
            var session3 = await base.RegisterAndLoginMaster();

            #region invite friend
            var outFriend = await _friendService.InviteFriend(new BaseInputById
            {
                Token = session.Token,
                Id = session2.UserId,
            });
            #endregion invite friend
            
            #region get list who invite me
            var listInvites = await _friendService.GetInvites(new BaseSkipCountInput { Token = session2.Token });
            listInvites.Count.ShouldBe(1);
            listInvites.Items.FirstOrDefault().UserId.ShouldBe(session.UserId);
            listInvites.Items.FirstOrDefault().FirstName.ShouldBe(session.FirstName);
            #endregion get list who invite me

            #region success invite
            await _friendService.AcceptInvite(new BaseInputById
            {
                Token = session2.Token,
                Id = session.UserId
            });

            
            #region contains
            var countFriend = await _friendService.GetFriends(new GetFriensInp
            {
                Token = session2.Token,
            });
            countFriend.Count.ShouldBeGreaterThan(0); 
            #endregion

            #endregion success invite

            #region faild accept invite
            BaseException ex = null;
            try
            {
                await _friendService.AcceptInvite(new BaseInputById
                {
                    Token = session2.Token,
                    Id = session.UserId
                });
            }
            catch (System.Exception exception)
            {
                ex = exception as BaseException;
            }
            ex.ShouldNotBeNull();
            #endregion faild accept invite


            var containsResult = await _friendService.GetFriends(new GetFriensInp
            {
                Token = session2.Token,
                Contains = session.FirstName.ToUpper()
            });
            containsResult.Count.ShouldBeGreaterThan(0);
            containsResult = await _friendService.GetFriends(new GetFriensInp
            {
                Token = session2.Token,
                Contains = Guid.NewGuid().ToString()
            });
            containsResult.Count.ShouldBe(0);
        }







    }
}