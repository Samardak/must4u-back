using System;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.Game.Enum;
using PlatformCore.Common.Game.InputOutput.IRegistrationService;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class RegistrationService_Test : GameTestBase
    {
        private readonly IAuthService _authService;
        

        public RegistrationService_Test()
        {
            _authService = base.ServiceProvider.GetRequiredService<IAuthService>();
        }

        [Fact]
        public async void Register()
        {
            var strGuid = Guid.NewGuid().ToString();
            
            var input = new RegisterInput
            {
                UserType = EnumUserTypes.Client,
                Email = strGuid,
                FirstName = strGuid,
                LastName = strGuid,
                Password = strGuid,
            };
            var resp = await _authService.Register(input);
            resp.ShouldNotBeNull();


            var respLogin = await _authService.Login(new LoginInput
            {
                Email = input.Email,
                Password = input.Password,
                UserType = input.UserType
            });
            respLogin.Token.ShouldNotBeNull();
        }


       
    }
}

