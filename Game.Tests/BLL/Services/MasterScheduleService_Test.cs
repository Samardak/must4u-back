using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.BaseInputOutput;
using PlatformCore.Common.CodeError;
using PlatformCore.Common.PlatformException;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class MasterScheduleService_Test : GameTestBase
    {
        private readonly IMasterScheduleService _masterScheduleService;

        public MasterScheduleService_Test()
        {
            _masterScheduleService = base.ServiceProvider.GetRequiredService<IMasterScheduleService>();
        }

        [Fact]
        public async Task GetWeekSchedule_SetWeekSchedule_Test()
        {
            var session = await base.RegisterAndLoginMaster();
            session.ShouldNotBeNull();
            session.Token.ShouldNotBeNull();


            var inputEmpty = new GetWeekScheduleInput{Token = session.Token,MyDays = new List<GetWeeokScheduleList>()};
            await _masterScheduleService.SetWeekSchedule(inputEmpty);

            #region dublicate day off week
            var inputDublicateDay = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>(),
                        DayOfWeek = DayOfWeek.Friday
                    },
                    new GetWeeokScheduleList
                    {
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>(),
                        DayOfWeek = DayOfWeek.Friday
                    }
                }
            };
            BaseException ex = null;
            try
            {
                await _masterScheduleService.SetWeekSchedule(inputDublicateDay);
            }
            catch (Exception e)
            {
                ex = e as BaseException;
            }
            ex.Code.ShouldBe(CodeError.YouPassDubliacteDayOffWeek);
            #endregion dublicate day off week

            #region dublicate schedule id
            var dublicateSchedulerId = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 1,
                            },
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 1,
                            }
                        }
                    }
                }
            };
            try
            {
                await _masterScheduleService.SetWeekSchedule(dublicateSchedulerId);
            }
            catch (Exception e)
            {
                ex = e as BaseException;
            }
            ex.Code.ShouldBe(CodeError.YouPassDubliacteScheduleId);
            #endregion dublicate schedule id
            
            #region does not exist schedule id
            var inputNotExistId = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {

                    new GetWeeokScheduleList
                    {
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 1,
                            },
                        },
                        DayOfWeek = DayOfWeek.Friday
                    }
                }
            };
            try
            {
                await _masterScheduleService.SetWeekSchedule(inputNotExistId);
            }
            catch (Exception e)
            {
                ex = e as BaseException;
            }
            ex.ShouldNotBeNull();
            ex.Code.ShouldBe(CodeError.BadRequeast);
            #endregion does not exist schedule id
            
            #region pass incorrect time start time and end time
            ex = null;
            try
            {
                var inputIncorrectTime = new GetWeekScheduleInput
                {
                    Token = session.Token,
                    MyDays = new List<GetWeeokScheduleList>
                    {
                        new GetWeeokScheduleList
                        {
                            DayOfWeek = DayOfWeek.Friday,
                            MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                            {
                                new MasterScheduleDayOfWeeksInputOutput
                                {
                                    StartTime = new TimeSpan(0,6,3),
                                    EndTime = new TimeSpan(0,4,3),
                                }
                            }
                        },
                    }
                };
                await _masterScheduleService.SetWeekSchedule(inputIncorrectTime);
            }
            catch (Exception e)
            {
                ex = e as BaseException;
            }
            ex.ShouldNotBeNull();
            ex.Code.ShouldBe(CodeError.BadRequeast);
            #endregion pass incorrect time start time and end time


            #region create new scheduleId 
            var inputNewSchedule = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 0,
                                StartTime = new TimeSpan(3,3,3),
                                EndTime = new TimeSpan(4,3,3)
                            },
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                StartTime = new TimeSpan(4,3,3),
                                EndTime = new TimeSpan(5,3,3)
                            }
                        }
                    },
                }
            };
            await _masterScheduleService.SetWeekSchedule(inputNewSchedule);
            #endregion create new scheduleId 

            var mySchedule = await _masterScheduleService.GetWeekSchedule(new BaseInput {Token = session.Token});

            #region create only new schedule
            mySchedule.MyDays.Count.ShouldBe(1);
            var myDay = mySchedule.MyDays.FirstOrDefault();
            myDay.ShouldNotBeNull();
            myDay.DayOfWeek.ShouldBe(DayOfWeek.Friday);
            myDay.MyTimes.Count.ShouldBe(2);
            myDay.MyTimes.FirstOrDefault().Id.ShouldBeGreaterThan(0);
            myDay.MyTimes.LastOrDefault().Id.ShouldBeGreaterThan(1);
            #endregion create only new schedule
            
            #region update and delete 
            myDay.MyTimes.FirstOrDefault().DayOfWeek = DayOfWeek.Sunday;
            myDay.MyTimes = new List<MasterScheduleDayOfWeeksInputOutput> { myDay.MyTimes.FirstOrDefault() };
            var updateMySchedule = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    myDay
                }
            };
            await _masterScheduleService.SetWeekSchedule(updateMySchedule);
            mySchedule = await _masterScheduleService.GetWeekSchedule(new BaseInput { Token = session.Token });
            mySchedule.MyDays.Count.ShouldBe(1);
            mySchedule.MyDays.FirstOrDefault().MyTimes.Count.ShouldBeGreaterThan(0);
            mySchedule.MyDays.FirstOrDefault().MyTimes.FirstOrDefault().DayOfWeek.ShouldBe(DayOfWeek.Sunday);
            #endregion update and delete 

        }

        [Fact]
        public async Task SetDayScheduleEveryWeek_Test()
        {
            var session = await base.RegisterAndLoginMaster();
            BaseException exception = null;
            var inputNewSchedule = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 0,
                                StartTime = new TimeSpan(3,3,3),
                                EndTime = new TimeSpan(4,3,3)
                            },
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                StartTime = new TimeSpan(4,3,3),
                                EndTime = new TimeSpan(5,3,3)
                            }
                        }
                    },
                }
            };
            await _masterScheduleService.SetWeekSchedule(inputNewSchedule);

            var firstFriday = this.FindDateOfWeek(DayOfWeek.Friday);
            firstFriday.DayOfWeek.ShouldBe(DayOfWeek.Friday);

            #region dublicate schedule id
            try
            {
                var dublicateScheduleId = new SetDayScheduleInput
                {
                    Token = session.Token,
                    Year = firstFriday.Year,
                    Month = firstFriday.Month,
                    Day = firstFriday.Day,
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                    {
                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            Id = 100,
                        },
                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            Id = 100,
                        }
                    }
                };
                await _masterScheduleService.SetDayScheduleEveryWeek(dublicateScheduleId);
            }
            catch (Exception e)
            {
                exception = e as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.YouPassDubliacteScheduleId);
            #endregion dublicate schedule id

            #region check time data
            exception = null;
            try
            {
                var invalidTime = new SetDayScheduleInput
                {
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                    {

                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            StartTime = new TimeSpan(3,3,3),
                            EndTime = new TimeSpan(2,3,3),
                        },
                    },
                    Token = session.Token,
                    Year = 2017,
                    Month = 3,
                    Day = 3
                };
                await _masterScheduleService.SetDayScheduleEveryWeek(invalidTime);
            }
            catch (Exception ex)
            {
                exception = ex as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.BadRequeast);
            #endregion check time data
            
            #region check not exist schedure id
            exception = null;
            try
            {
                var notExistScheduleId = new SetDayScheduleInput
                {
                    Token = session.Token,
                    Year = 2017,
                    Month = 3,
                    Day = 3,
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        Id = 3
                    }
                }
                };
                await _masterScheduleService.SetDayScheduleEveryWeek(notExistScheduleId);
            }
            catch (Exception ex)
            {
                exception = ex as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.BadRequeast);
            #endregion check not exist schedure id

            #region create schedule
            var inputCreate = new SetDayScheduleInput
            {
                Year = 2017,
                Day = 3,
                Month = 3,
                Token = session.Token,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(10,10,10),
                        EndTime = new TimeSpan(16,10,10),
                    },
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(16,10,10),
                        EndTime = new TimeSpan(18,10,10),
                    }
                }
            };
            await _masterScheduleService.SetDayScheduleEveryWeek(inputCreate);
            var mySchedule = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3
            });
            var myTime = mySchedule.MyTimes;
            myTime.Count.ShouldBe(2);
            myTime.FirstOrDefault().Id.ShouldBeGreaterThan(0);
            #endregion create schedule

            #region update schedule
            var updateTimes = mySchedule.MyTimes;
            var myFTime = updateTimes.FirstOrDefault();
            myFTime.StartTime = new TimeSpan(1, 1, 1);
            var inputUpdate = new SetDayScheduleInput
            {
                Year = 2017,
                Day = 3,
                Month = 3,
                Token = session.Token,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    myFTime
                }
            };

            await _masterScheduleService.SetDayScheduleEveryWeek(inputUpdate);
            var updateOut = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3
            });
            updateOut.MyTimes.Count.ShouldBeGreaterThan(0);
            updateOut.MyTimes.FirstOrDefault().StartTime.ShouldBe(new TimeSpan(1, 1, 1));
            #endregion update schedule


        }

        [Fact]
        public async Task SetDayScheduleCurrentDay_Test()
        {
            var session = await base.RegisterAndLoginMaster();

            BaseException exception = null;
            var inputNewSchedule = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 0,
                                StartTime = new TimeSpan(3,3,3),
                                EndTime = new TimeSpan(4,3,3)
                            },
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                StartTime = new TimeSpan(4,3,3),
                                EndTime = new TimeSpan(5,3,3)
                            }
                        }
                    },
                }
            };
            await _masterScheduleService.SetWeekSchedule(inputNewSchedule);

            var firstFriday = this.FindDateOfWeek(DayOfWeek.Friday);
            firstFriday.DayOfWeek.ShouldBe(DayOfWeek.Friday);

            #region dublicate schedule id
            try
            {
                var dublicateScheduleId = new SetDayScheduleInput
                {
                    Token = session.Token,
                    Year = firstFriday.Year,
                    Month = firstFriday.Month,
                    Day = firstFriday.Day,
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                    {
                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            Id = 100,
                        },
                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            Id = 100,
                        }
                    }
                };
                await _masterScheduleService.SetDayScheduleCurrentDay(dublicateScheduleId);
            }
            catch (Exception e)
            {
                exception = e as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.YouPassDubliacteScheduleId);
            #endregion dublicate schedule id

            #region check time data
            exception = null;
            try
            {
                var invalidTime = new SetDayScheduleInput
                {
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                    {

                        new MasterScheduleDayOfWeeksInputOutput
                        {
                            StartTime = new TimeSpan(3,3,3),
                            EndTime = new TimeSpan(2,3,3),
                        },
                    },
                    Token = session.Token,
                    Year = 2017,
                    Month = 3,
                    Day = 3
                };
                await _masterScheduleService.SetDayScheduleCurrentDay(invalidTime);
            }
            catch (Exception ex)
            {
                exception = ex as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.BadRequeast);
            #endregion check time data

            #region validate not exist schedule id
            exception = null;
            try
            {
                var inputNotExistId = new SetDayScheduleInput
                {
                    Token = session.Token,
                    Year = 2017,
                    Day = 3,
                    Month = 3,
                    MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        Id = 100000
                    }
                }
                };
                await _masterScheduleService.SetDayScheduleCurrentDay(inputNotExistId);
            }
            catch (Exception ex)
            {
                exception = ex as BaseException;
            }
            exception.ShouldNotBeNull();
            exception.Code.ShouldBe(CodeError.BadRequeast);
            #endregion validate not exist schedule id

            #region create schedule weaklyDay
            var inputCreateWeek = new SetDayScheduleInput
            {
                Year = 2017,
                Day = 3,
                Month = 3,
                Token = session.Token,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(10,10,10),
                        EndTime = new TimeSpan(16,10,10),
                    },
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(16,10,10),
                        EndTime = new TimeSpan(18,10,10),
                    }
                }
            };
            await _masterScheduleService.SetDayScheduleEveryWeek(inputCreateWeek);
            var mySchedule = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3
            });
            var myTime = mySchedule.MyTimes;
            myTime.Count.ShouldBe(2);
            myTime.FirstOrDefault().Id.ShouldBeGreaterThan(0);
            mySchedule.ScheduleDayMode.ShouldBe(EnumSetDaySchedule.AllWeeks);
            #endregion create schedule weaklyDay

            #region create schedule day 
            var inputCreate = new SetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(3,3,3),
                        EndTime = new TimeSpan(4,3,3)
                    },
                    new MasterScheduleDayOfWeeksInputOutput
                    {
                        StartTime = new TimeSpan(4,3,3),
                        EndTime = new TimeSpan(5,3,3)
                    }
                }
            };
            await _masterScheduleService.SetDayScheduleCurrentDay(inputCreate);
            var createOut = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Day = 3,
                Month = 3,
                Year = 2017
            });
            createOut.MyTimes.Count.ShouldBe(2);
            createOut.ScheduleDayMode.ShouldBe(EnumSetDaySchedule.OnlyThisDay);
            #endregion create schedule day 


            #region rupdate schedule
            var inputUpdate = new SetDayScheduleInput
            {
                Token = session.Token,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                {
                    createOut.MyTimes.FirstOrDefault()
                },
                Year = 2017,
                Month = 3,
                Day = 3,
            };
            await _masterScheduleService.SetDayScheduleCurrentDay(inputUpdate);
            var nowSche = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3
            });
            nowSche.ScheduleDayMode.ShouldBe(EnumSetDaySchedule.OnlyThisDay);
            nowSche.MyTimes.Count.ShouldBe(1);
            #endregion rupdate schedule





            #region times other day
            var createOutOtherDay = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Month = 3,
                Day = 3,
                Year = 2018
            });
            createOutOtherDay.MyTimes.Count.ShouldBe(0);
            #endregion times other day




        }

        [Fact]
        public async Task SetDayScheduleFreeDay_Test()
        {
            var session = await base.RegisterAndLoginMaster();

            var input = new SetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3,
                MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>()
            };
            var retFree = await _masterScheduleService.SetDayScheduleFreeDay(input);
            var retMSch = await _masterScheduleService.GetDaySchedule(new GetDayScheduleInput
            {
                Token = session.Token,
                Year = 2017,
                Month = 3,
                Day = 3
            });
            retMSch.ScheduleDayMode.ShouldBe(EnumSetDaySchedule.FreeDay);
            retMSch.MyTimes.Count.ShouldBe(0);

        }

        [Fact]
        public async Task GetWeekScheduleForMaster()
        {
            var session = await base.RegisterAndLoginMaster();

            #region create new scheduleId 
            var inputNewSchedule = new GetWeekScheduleInput
            {
                Token = session.Token,
                MyDays = new List<GetWeeokScheduleList>
                {
                    new GetWeeokScheduleList
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        MyTimes = new List<MasterScheduleDayOfWeeksInputOutput>
                        {
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                Id = 0,
                                StartTime = new TimeSpan(3,3,3),
                                EndTime = new TimeSpan(4,3,3)
                            },
                            new MasterScheduleDayOfWeeksInputOutput
                            {
                                StartTime = new TimeSpan(4,3,3),
                                EndTime = new TimeSpan(5,3,3)
                            }
                        }
                    },
                }
            };
            await _masterScheduleService.SetWeekSchedule(inputNewSchedule);
            #endregion create new scheduleId 

            var client = await base.RegisterAndLoginClient();
            var schedule = await _masterScheduleService.GetDayScheduleForMaster(
                new GetDayScheduleForMasterInput
            {
                Id = session.UserId,
                Token = client.Token,
                Year = 2017,
                Month = 5,
                Day = 1
            });
            schedule.MyTimes.Count.ShouldBeGreaterThan(0);
        }


        private DateTimeOffset FindDateOfWeek(DayOfWeek dayOfWeek)
        {
            var dtNow = DateTimeOffset.Now;
            if (dtNow.DayOfWeek == dayOfWeek)
                return dtNow;
            for (int i = 0; i < 10; i++)
            {
                dtNow = dtNow.AddDays(1);
                if (dtNow.DayOfWeek == dayOfWeek)
                    return dtNow;
            }
            return dtNow;
        }

        

    }
}