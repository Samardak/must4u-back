﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Application.Common.CommonService.DateConvertor;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL
{
    public class TimeStamp_Test
    {
        [Fact]
        public void X()
        {
            var  d = new DateConvertorServiceInternal();
            var input = 1489132800;
            DateTimeOffset result = d.ConvertToDate(input);
            var second = d.ConvertToSecond(result);
            
            second.ShouldBe(input);

        }

    }
}
