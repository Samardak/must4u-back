using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using GameDAL.IRepositoryGoGaWi;
using Microsoft.EntityFrameworkCore;

namespace Game.Tests
{
    public static class ResourseRepo<T>
    {
        public static List<T> List { get; set; } = new List<T>();
    }
    public class RepositoryMock<T, TDto> : IRepository<T,TDto> where T : class
    {
        public RepositoryMock()
        {
            this.list = ResourseRepo<T>.List;
        }
        private List<T> list;
        private int _maxId = 0;


        public async Task<T> FirstOrDefault(Expression<Func<T, bool>> func)
        {
            return list.FirstOrDefault(func.Compile());
        }

        public async Task<TDto> FirstOrDefaultDto(Expression<Func<T, bool>> func)
        {
            var ret = list.FirstOrDefault(func.Compile());
            return Mapper.Map<TDto>(ret);
        }

        public async Task<List<T>> GetList(Expression<Func<T, bool>> func)
        {
            return list.Where(func.Compile()).ToList();
        }

        public async Task<List<TDto>> GetListDto(Expression<Func<T, bool>> func)
        {
            var ret = await this.GetList(func);
            return ret.Select(i=> Mapper.Map<TDto>(i)).ToList();
        }

        public async Task<List<T>> GetList()
        {
            var ret = list.ToList();
            return ret;
        }

        public async Task<List<TDto>> GetListDto()
        {
            var ret = await this.GetList();
            return ret.Select(i => Mapper.Map<TDto>(i)).ToList();
        }

        public async Task<T> Insert(T input)
        {
            dynamic d = input;
            _maxId = _maxId + 1;
            d.Id = _maxId;
            list.Add(d);
            dynamic dynamicInp = input;
            return d;
        }

        public async Task<TDto> Insert(TDto input)
        {
            var t = Mapper.Map<T>(input);
            return Mapper.Map<TDto>(await Insert(t));
        }

        public async Task<T> Update(T input)
        {
            dynamic d = input;
            var ret = this.list.FirstOrDefault(i => (i as dynamic).Id == d.Id);
            list.Remove(ret);
            list.Add(input);
            return input;
        }

        public DbSet<T> GetDbSet()
        {
            throw new NotImplementedException();
        }

        public Task SaveShanges()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Queryable => this.list.AsQueryable();
    }

    public class RepositoryMock<T> : IRepository<T> where T : class
    {
        public RepositoryMock()
        {
            this.list = ResourseRepo<T>.List;
        }
        private List<T> list;
        private int _maxId = 0;
        public async Task<T> FirstOrDefault(Expression<Func<T, bool>> func)
        {
            return Enumerable.FirstOrDefault(list, func.Compile());
        }

        public async Task<List<T>> GetList(Expression<Func<T, bool>> func)
        {
            return Enumerable.Where(list, func.Compile()).ToList();
        }

        public async Task<List<T>> GetList()
        {
            return Enumerable.ToList(list);
        }

        public async Task<T> Insert(T input)
        {
            dynamic d = input;
            _maxId = _maxId + 1;
            d.Id = _maxId;
            list.Add(d);
            return d;
        }

        public async Task<T> Update(T input)
        {
            dynamic d = input;
            var ret = Enumerable.FirstOrDefault(this.list, i => (i as dynamic).Id == d.Id);
            list.Remove(ret);
            list.Add(input);
            return input;
        }

        public DbSet<T> GetDbSet()
        {
            throw new NotImplementedException();
        }

        public Task SaveShanges()
        {
            throw new NotImplementedException();
        }

        public IQueryable<T> Queryable => this.list.AsQueryable();
    }

    

}